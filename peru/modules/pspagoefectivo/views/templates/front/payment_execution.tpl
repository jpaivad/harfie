<!-- PagoEfectivo Estilos -->
    <link rel="stylesheet" type="text/css" href="{$this_path_pe}lib_pagoefectivo/js/fancybox/jquery.fancybox.css?v=2.0.6" media="screen" />
    <link href="{$this_path_pe}lib_pagoefectivo/css/estilos{if $this_version == "15"}15{else}16{/if}.css?" type="text/css" rel="stylesheet" media="screen" />
<!-- Add jQuery library -->
    <script type="text/javascript" src="../../modules/pspagoefectivo/lib_pagoefectivo/js/jquery.min.js"></script>
<!-- PagoEfectivo Librerías -->
	<script type="text/javascript" src="../../modules/pspagoefectivo/lib_pagoefectivo/js/fancybox/jquery.fancybox.js"></script>
	<script type="text/javascript" src="../../modules/pspagoefectivo/lib_pagoefectivo/js/swfobject/swfobject.js"></script>
    <script type="text/javascript" src="../../modules/pspagoefectivo/lib_pagoefectivo/js/JScript.js"></script>
{capture name=path}
    {l s='Pago por Pago Efectivo.' mod='pspagoefectivo'}
{/capture}

<h1 class="page-heading">
    {l s='Resumen del pedido' mod='pspagoefectivo'}
</h1>

{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}

{if $nbProducts <= 0}
	<p class="alert alert-warning warning">
        {l s='Su carro de compras está vacío.' mod='pspagoefectivo'}
    </p>
{else}
    <form id="form_pagoefectivo_execution" action="{$link->getModuleLink('pspagoefectivo', 'validation', [], true)|escape:'html':'UTF-8'}" method="post">
        <div class="box cheque-box">
            <h3 class="page-subheading">
                {l s='Pago por PagoEfectivo' mod='pspagoefectivo'}
            </h3>
            <p class="cheque-indent">
                    <img src="{$this_path_pe}img_pagoefectivo/logo_pagoefectivo_91x42.png" alt="Transferencia bancaria" width="91" height="42" style="float:left; margin: 0px 10px 5px 0px;">
                    <span style="text-align: justify;">
                        Compra con PagoEfectivo y paga a trav&eacute;s de internet o en cualquier oficina del BCP, BBVA, Interbank, Scotiabank, en Agencias de Pago de Servicios Western Union y en establecimientos autorizados que tengan el logo de PagoEfectivo y/o FullCarga. <br />
                            <span style="font-size:10px;">
                			    <a id="fancy" title="&iquest;Qu&eacute; es PagoEfectivo?"  style="color:#006999; text-decoration:none;" href="{$this_popup_pe}">&iquest;Qu&eacute; es PagoEfectivo?</a>
                            </span>
                    </span>
                    <div class="clear"></div> <br />
            </p>

            <p class="cheque-indent">
                <strong class="dark">
                    {l s='Aquí hay un pequeño resumen de su pedido:' mod='pspagoefectivo'}
                </strong>
            </p>
            <p>
                - {l s='El importe total de su pedido es' mod='pspagoefectivo'}
                <span id="amount" class="price">{displayPrice price=$total}</span>
                {if $use_taxes == 1}
                    {l s='(tasas incluídas)' mod='pspagoefectivo'}
                {/if}
            </p>
            <p>
                -
                {if $currencies|@count > 1}
                    {l s='Aceptamos las siguientes monedas para las transferencias bancarias.' mod='pspagoefectivo'}
                    <div class="form-group">
                        <label>{l s='Seleccione una de las siguientes monedas:' mod='pspagoefectivo'}</label>
                        <select id="currency_payement" class="form-control" name="currency_payement">
                            {foreach from=$currencies item=currency}
                                <option value="{$currency.id_currency}" {if $currency.id_currency == $cust_currency}selected="selected"{/if}>
                                    {$currency.name}
                                </option>
                            {/foreach}
                        </select>
                    </div>
                {else}
                    {l s='Aceptamos las siguientes monedas para las transferencias bancarias:' mod='pspagoefectivo'}&nbsp;<b>{$currencies.0.name}</b>
                    <input type="hidden" name="currency_payement" value="{$currencies.0.id_currency}" />
                {/if}
            </p>
            <p>
                - {l s='La información para realizar la transferencia bancaria aparecerá en la página siguiente.' mod='pspagoefectivo'}
            </p>
            <p>
                - {l s='Por favor, confirme su pedido haciendo clic en "confirmo mi pedido."' mod='pspagoefectivo'}.
            </p>
        </div><!-- .cheque-box -->

        <p class="cart_navigation" id="cart_navigation">
            {if $this_version == "15"}
                <input type="submit" value="{l s='Confirmo mi pedido' mod='pspagoefectivo'}" class="exclusive_large" />
            	<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html'}" class="button_large">{l s='Otros modos de pago' mod='pspagoefectivo'}</a>
            {else}
                 <a class="button-exclusive btn btn-default" href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html':'UTF-8'}">
                    <i class="icon-chevron-left"></i>{l s='Otros modos de pago' mod='pspagoefectivo'}
                </a>
                <button class="button btn btn-default button-medium" type="submit">
                    <span>{l s='Confirmo mi pedido' mod='pspagoefectivo'}<i class="icon-chevron-right right"></i></span>
                </button>
            {/if}
        </p>
    </form>
{/if}
