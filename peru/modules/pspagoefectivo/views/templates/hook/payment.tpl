<!--href="{$link->getModuleLink('pspagoefectivo', 'payment')|escape:'html':'UTF-8'}"-->
<link href="{$this_path_pe}lib_pagoefectivo/css/estilos{if $this_version == "15"}15{else}16{/if}.css?" type="text/css" rel="stylesheet" media="screen" />
<div class="row">
	<div class="col-xs-12 col-md-6-pagoefectivo">
        <p class="payment_module">
            <a
            class="pagoefectivo"
            href="{$link->getModuleLink('pspagoefectivo', 'validation', [], true)|escape:'html':'UTF-8'}"
            title="{l s='Pagar por PagoEfectivo' mod='pspagoefectivo'}">

	
			<u><b>Banca por Internet</b></u>	<br />
			Paga en BBVA, BCP, INTERBANK, SCOTIABANK Y BANBIF. Deb&iacute;talo de tu cuenta o c&aacute;rgalo a tu tarjeta de credito asociada.<br />
			<u><b>Agentes y Agencias</b></u><br />
                        Ac&eacute;rcate a cualquier punto de BBVA, BCP, INTERBANK, SCOTIABANK y BANBIF. Agentes corresponsales KASNET, WESTERN UNION - Pago de Servicios y FULLCARGA. <br />
            </a>
        </p>
    </div>
</div>