﻿<?php
    ini_set('display_errors',1);
class pspagoefectivoValidationModuleFrontController extends ModuleFrontController
{
	/**
	 * @see FrontController::postProcess()
	 */
	public function postProcess()
	{
		$cart = $this->context->cart;
		if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active)
			Tools::redirect('index.php?controller=order&step=1');

		// Check that this payment option is still available in case the customer changed his address just before the end of the checkout process
		$authorized = false;
		foreach (Module::getPaymentModules() as $module)
			if ($module['name'] == 'pspagoefectivo')
			{
				$authorized = true;
				break;
			}
		if (!$authorized)
			die($this->module->l('Este método de pago no está disponible.', 'validation'));

		$customer = new Customer($cart->id_customer);
		if (!Validate::isLoadedObject($customer))
            Tools::redirect('index.php?controller=order&step=1');

		$currency = $this->context->currency;
		$total = (float)$cart->getOrderTotal(true, Cart::BOTH);
        require_once(dirname(__FILE__) . "/../../lib_pagoefectivo/code/configpe.php");
		$mailVars = array(
			'{pe_customer_email}' => $customer->email,
			'{pe_order_to}' => $customer->firstname . " " . $customer->lastname,
			'{pe_shop_adress}' => EMAIL_PORTAL,
    		'{pe_details}' => 'El código de pago es: ' . $cart->id,
    		'{pe_tipo_pago}' => 'Pago Efectivo',
		);

        // Registra la orden en base de datos, tabla de órdenes
  		$this->module->validateOrder($cart->id, Configuration::get('PS_OS_BANKWIRE'), $total, $this->module->displayName, NULL, $mailVars, (int)$currency->id, false, $customer->secure_key);

        /***************************************************************************************************************************/
        /******************************************* GENERAR EL CÓDIGO CIP**********************************************************/
        require_once(dirname(__FILE__) . "/../../lib_pagoefectivo/code/PagoEfectivo.php");
        require_once(dirname(__FILE__) . "/../../lib_pagoefectivo/code/be/be_solicitud.php");
        
        /*****************************************DATOS DE PRUEBA*******************************************************************/
        date_default_timezone_set('America/Lima');
        $be_solicitud = new BE_PagoEfectivo_Solicitud();
        $be_solicitud->_moneda            = 1;//(int)$currency->id;
        $be_solicitud->_monto             = number_format($total, 2, '.', '');
        $be_solicitud->_medio_pago        = MEDIO_PAGO;
        $be_solicitud->_cod_servicio      = MERCHAND_ID;
        $be_solicitud->_numero_orden      = $this->module->currentOrder;
        $be_solicitud->_concepto_pago     = COMERCIO_CONCEPTO_PAGO . ": Orden " .$be_solicitud->_numero_orden;
        $be_solicitud->_email_comercio    = EMAIL_PORTAL;
        $be_solicitud->_fecha_expirar     = date('d/m/Y H:i:s', time() + ((int)TIEMPO_EXPIRACION * 60 * 60));
        $be_solicitud->_usuario_nombre    = $customer->firstname;
        $be_solicitud->_usuario_apellidos = $customer->lastname;
        $be_solicitud->_usuario_email     = $customer->email;
        /************************************************************************************************************/

        /*****************************************ENVIAR VENTA A PAGO EFECTIVO********************************************************/
    	$pagoefectivo = new App_Service_PagoEfectivo();
        $paymentRequest = $pagoefectivo->enviarPago($be_solicitud);
        /************************************************************************************************************/

        /***************************************************************************************************************************/

	    $params['cliente'] = $customer->firstname . " " . $customer->lastname;
	    $params['monto'] = $total;
	    $params['moneda'] = (int)$currency->id;
	    $params['orden'] = str_pad($cart->id, 2, "0", STR_PAD_LEFT);
	    $params['cip'] = $paymentRequest->CIP->NumeroOrdenPago;
	    $params['estado'] = $paymentRequest->Estado;
	    $params['token'] = $paymentRequest->Token;
	    $params['fecha_pago'] = $be_solicitud->_fecha_expirar;

        // Registra la orden en base de datos, tabla pagoefectivo`
        Db::getInstance()->autoExecute(_DB_PREFIX_ . 'pagoefectivo', $params, 'INSERT');
        $token = ($paymentRequest->Token?$paymentRequest->Token:0);
//        $token = 123;

echo '<script type="text/javascript">window.location="http://www.harfie.com/harfietest/shop/index.php?controller=order-confirmation&id_cart=' .$cart->id.'&id_module='.$this->module->id.'&id_order='.$this->module->currentOrder.'&TOKEN=' . $token . '&key='.$customer->secure_key . '"</script>';



        
Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$this->module->id.'&id_order='.$this->module->currentOrder.'&TOKEN=' . $token . '&key='.$customer->secure_key);

    }
}
?>