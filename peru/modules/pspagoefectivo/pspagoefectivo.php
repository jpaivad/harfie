<?php
  //@001 LAG 06/08/2014 - Se ocultó la etiqueta Server Ip
    ini_set('display_errors',1);
  if (!defined('_PS_VERSION_'))
     exit;
  class pspagoefectivo extends PaymentModule
  {
      private $_html = '';
      private $_postErrors = array();
      /******************************************************* INSTALACIÓN ******************************************************************/
      public function __construct()
      {
        $this->name = 'pspagoefectivo';
        $this->tab = 'payments_gateways';
        $this->version = '3.0';
        $this->author = 'El Comercio';
		$this->controllers = array('payment', 'validation');
        $this->need_instance = 1;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.6');
        $this->bootstrap = true;

        $this->currencies = true;
	    $this->currencies_mode = 'checkbox';

        parent::__construct();

        $this->displayName = $this->l('PagoEfectivo');
        $this->description = $this->l('Transacciones seguras en internet en el Perú.');

        $this->confirmUninstall = $this->l('¿Seguro de desintalar el módulo de PagoEfectivo?');

        if (!Configuration::get('PAGOEFECTIVO_CONFIG'))
          $this->warning = $this->l('El módulo de PagoEfectivo aún no ha sido configurado.');
        $configpe = dirname(__FILE__) . DIRECTORY_SEPARATOR . "lib_pagoefectivo/code/configpe.php";
        require_once($configpe);
        if (!version_compare(PHP_VERSION, '5.2.0', '>=')) {
            $this->warning = $this->l("PagoEfectivo es compatible con PHP 5.2 o superior, usted tiene la versión " . PHP_VERSION);
        }
        if (!extension_loaded('openssl')) {
             $this->warning = $this->l("Debe habilitar la extensión openssl en su archivo php.ini.");
        }
        if (!extension_loaded('soap')) {
             $this->warning = $this->l("Debe habilitar la extensión soap en su archivo php.ini.");
        }

/*        if(!file_get_contents(WSCIP)){
             $this->warning = $this->l("Debe configurar su IP pública (" . $_SERVER['REMOTE_ADDR'] . ") para tener acceso a los Web Services de Pago Efectivo.");
        }*/
      }

      public function install()   // Debe crearse la tabla SQL y copiarse los archivos a la ubicacion correcta
      {
         if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);
         return (parent::install() && $this->installDB() && $this->createDir() &&
            $this->registerHook('payment') &&
            $this->registerHook('paymentReturn'));
      }

      public function createDir()
      {
          if($this->copy_directory(_PS_MODULE_DIR_ . "/pspagoefectivo/urlpe", _PS_MODULE_DIR_ . "/../urlpe") &&
             $this->delete_directory(_PS_MODULE_DIR_ . "/pspagoefectivo/urlpe"))
             return true;
          else return false;
      }

      function copy_directory($src,$dst) {
          try{
              mkdir($dst);
              $files = array_diff(scandir($src), array('.','..'));
              foreach ($files as $file) {
                 (is_dir("$src/$file") && !is_link($src)) ? copy_directory("$src/$file","$dst/$file") : copy("$src/$file","$dst/$file");
              }
              return true;
          }
          catch(Exception $e)
          {
              return false;
          }
      }

      function delete_directory($dir) {
          try{
              $files = array_diff(scandir($dir), array('.','..'));
              foreach ($files as $file) {
                (is_dir("$dir/$file") && !is_link($dir)) ? delete_directory("$dir/$file") : unlink("$dir/$file");
              }
              rmdir($dir);
              return true;
          }
          catch(Exception $e)
          {
              return false;
          }
      }
    	public function installDB()
    	{
    		$return = true;
    		$return &= Db::getInstance()->execute('
                      CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'pagoefectivo (
                      id_pasarelapago int(10) NOT NULL AUTO_INCREMENT,
                      cliente varchar(200) NOT NULL,
                      monto decimal(17,2) DEFAULT NULL,
                      moneda int(11) NOT NULL,
                      orden varchar(18) NOT NULL,
                      cip varchar(18) DEFAULT NULL,
                      estado tinyint(1) DEFAULT NULL,
                      token varchar(256) DEFAULT NULL,
                      fecha_pago timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      PRIMARY KEY (id_pasarelapago)
                    ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;'
    		);

    		return $return;
    	}

      public function uninstall() // Deben eliminarse los archivos de pago efectivo
      {
         return (parent::uninstall() && $this->uninstallPE() && Configuration::deleteByName('PAGOEFECTIVO_CONFIG'));
      }

      public function uninstallPE()
      {
          return $this->deleteSecurityPath();
      }
      public function deleteSecurityPath()
      {
          $configpe = dirname(__FILE__) . DIRECTORY_SEPARATOR . "lib_pagoefectivo/code/configpe.php";
          require_once($configpe);
          if (file_exists(SECURITY_PRIMARY_PATH)) {
              return $this->deleteDir(SECURITY_PRIMARY_PATH);
          }
          return true;
      }
      function deleteDir($dir)
      {
         if (substr($dir, strlen($dir)-1, 1) != '/')
             $dir .= '/';

         if ($handle = opendir($dir))
         {
             while ($obj = readdir($handle))
             {
                 if ($obj != '.' && $obj != '..')
                 {
                     if (is_dir($dir.$obj))
                     {
                         if (!$this->deleteDir($dir.$obj))
                             return false;
                     }
                     elseif (is_file($dir.$obj))
                     {
                         if (!unlink($dir.$obj))
                             return false;
                     }
                 }
             }

             closedir($handle);

             if (!@rmdir($dir))
                 return false;
             return true;
         }
         return false;
      }
      public function getKeyPath()
      {

          $configpe = dirname(__FILE__) . DIRECTORY_SEPARATOR . "lib_pagoefectivo/code/configpe.php";
          require_once($configpe);
          return SECURITY_PATH;
      }
      public function makeSecurityPath($MERCHAND_ID,$CCLAVE)
      {
                    
          $configpe = dirname(__FILE__) . DIRECTORY_SEPARATOR . "lib_pagoefectivo/code/configpe.php";
          require_once($configpe);
          return $MERCHAND_ID . "/" . str_replace("-","/",$CCLAVE);
      }
      public function setKeyPath($ROOT,$MERCHAND_ID,$CCLAVE)
      {
          return ($ROOT . $this->makeSecurityPath($MERCHAND_ID,$CCLAVE) . "/keys/");
      }
      public function setNotificationPath($ROOT,$MERCHAND_ID,$CCLAVE)
      {
          return ($ROOT . $this->makeSecurityPath($MERCHAND_ID,$CCLAVE) . "/notificacion/");
      }
      /************************************************* CONFIGURACIÓN *********************************************************************/
      public function getContent()
      {
          
          $output = null;
          if (Tools::isSubmit('submit'.$this->name))
          {
              
              $MERCHAND_ID = strval(Tools::getValue('MERCHAND_ID'));
              $CAPI = strval(Tools::getValue('CAPI'));
              $CCLAVE = strval(Tools::getValue('CCLAVE'));
              $SERVER = strval(Tools::getValue('SERVER'));
              //I001
              //$SERVER_IP = strval(Tools::getValue('SERVER_IP'));
              //F001
              $MOD_INTEGRACION = strval(Tools::getValue('MOD_INTEGRACION'));
              $COMERCIO_CONCEPTO_PAGO = strval(Tools::getValue('COMERCIO_CONCEPTO_PAGO'));
              $EMAIL_PORTAL = strval(Tools::getValue('EMAIL_PORTAL'));
              $TIEMPO_EXPIRACION = strval(Tools::getValue('TIEMPO_EXPIRACION'));

              $msj_error="";$count_error=0;
              if (!$MERCHAND_ID || empty($MERCHAND_ID)){ //                || !Validate::isGenericName($my_module_name))
                  ++$count_error; $msj_error .="<li>Indique el código del comercio</li>";
              }
              if (!$CAPI || empty($CAPI)){
                  ++$count_error; $msj_error .="<li>Indique el código público del comercio</li>";
              }
              if (!$CCLAVE || empty($CCLAVE)){
                  ++$count_error; $msj_error .="<li>Indique el código privado del comercio</li>";
              }
              if (!$SERVER || empty($SERVER)){
                  ++$count_error; $msj_error .="<li>Indique la url de PagoEfectivo</li>";
              }
              if (!isset($_FILES['LLAVE_PUBLICA']) || !isset($_FILES['LLAVE_PUBLICA']['tmp_name']) || empty($_FILES['LLAVE_PUBLICA']['tmp_name'])){
                  ++$count_error; $msj_error .="<li>Cargue la llave pública</li>";
              }
              if (!isset($_FILES['LLAVE_PRIVADA']) || !isset($_FILES['LLAVE_PRIVADA']['tmp_name']) || empty($_FILES['LLAVE_PRIVADA']['tmp_name'])){
                  ++$count_error; $msj_error .="<li>Cargue la llave privada</li>";
              }
              if (!$MOD_INTEGRACION || empty($MOD_INTEGRACION)){
                  ++$count_error; $msj_error .="<li>Indique el modo de integración</li>";
              }
              if (!$COMERCIO_CONCEPTO_PAGO || empty($COMERCIO_CONCEPTO_PAGO)){
                  ++$count_error; $msj_error .="<li>Indique el nombre del comercio</li>";
              }
              if (!$EMAIL_PORTAL || empty($EMAIL_PORTAL)){
                  ++$count_error; $msj_error .="<li>Indique el email del comercio</li>";
              }
              if (!$TIEMPO_EXPIRACION || empty($TIEMPO_EXPIRACION) || !is_numeric($TIEMPO_EXPIRACION)){
                  ++$count_error; $msj_error .="<li>Indique el tiempo de expiración del pago en horas.</li>";
              }
              
              if($count_error==0)
              {


                  $configpe = dirname(__FILE__) . DIRECTORY_SEPARATOR . "lib_pagoefectivo/code/configpe.php";
                  require_once($configpe);
                  if (file_exists(SECURITY_PATH)) { $this->deleteDir(SECURITY_PATH); }
                  $SECURITY_PATH = $this->setKeyPath(SECURITY_PRIMARY_PATH,$MERCHAND_ID,$CCLAVE);

                  if (!file_exists($SECURITY_PATH)) {
                      if (!mkdir($SECURITY_PATH, 0777, true)) {
                          die('Failed to create folders...');
                      }
                  }
                  if ($error = $this->validateKeyUpload($_FILES['LLAVE_PUBLICA'], 4000))
  				        return $this->displayError($this->l($error));
        		  else
        		  {
      				$ext = substr($_FILES['LLAVE_PUBLICA']['name'], strrpos($_FILES['LLAVE_PUBLICA']['name'], '.') + 1);
      				$file_public = md5($_FILES['LLAVE_PUBLICA']['name']).'.'.$ext;

      				if (!move_uploaded_file($_FILES['LLAVE_PUBLICA']['tmp_name'], $SECURITY_PATH.$file_public))
      					return $this->displayError($this->l('Ocurrió un error mientras se cargaba el archivo: ' . $_FILES['LLAVE_PUBLICA']['name']));
        		  }
                  if ($error = $this->validateKeyUpload($_FILES['LLAVE_PRIVADA'], 4000))
    				    return $this->displayError($this->l($error));
        		  else
        		  {
      				$ext = substr($_FILES['LLAVE_PRIVADA']['name'], strrpos($_FILES['LLAVE_PRIVADA']['name'], '.') + 1);
      				$file_private = md5($_FILES['LLAVE_PRIVADA']['name']).'.'.$ext;

      				if (!move_uploaded_file($_FILES['LLAVE_PRIVADA']['tmp_name'], $SECURITY_PATH.$file_private))
      					return $this->displayError($this->l('Ocurrió un error mientras se cargaba el archivo: ' . $_FILES['LLAVE_PRIVADA']['name']));
      		      }

                  //Escribe los datos en el archivo de configuración
/*                  $URL_NOTIFICACION = $this->setNotificationPath(DOMINIO_COMERCIO, $MERCHAND_ID,$CCLAVE) . "notificacion.php";
                  $PATH_NOTIFICACION = $this->setNotificationPath(SECURITY_PRIMARY_PATH,$MERCHAND_ID,$CCLAVE);
                  if (!file_exists($PATH_NOTIFICACION)) {
                      if (!mkdir($PATH_NOTIFICACION, 0777, true)) {
                          die('Failed to create folders...');
                      }
                  }
    			  if (!rename(PATH_NOTIFICACION . "notificacion.php", $PATH_NOTIFICACION . "notificacion.php"))
  				     	return $this->displayError($this->l('Ocurrió un error mientras se cargaba el archivo: notificacion.php'));
*/
                  $sLineas = file($configpe);$index=0;
                  foreach ($sLineas as $sLinea)
                  {
                      if(strrpos($sLinea,"efine('MERCHAND_ID'")>0){ $sLineas[$index] = "define('MERCHAND_ID', '" . $MERCHAND_ID . "');\n"; }
                      else if(strrpos($sLinea,"efine('CAPI'")>0){ $sLineas[$index] = "define('CAPI', '" . $CAPI . "');\n"; }
                      else if(strrpos($sLinea,"efine('CCLAVE'")>0){ $sLineas[$index] = "define('CCLAVE', '" . $CCLAVE . "');\n"; }
                      else if(strrpos($sLinea,"efine('SERVER'")>0){ $sLineas[$index] = "define('SERVER', '" . $SERVER . "');\n"; }
                      //I001
                      //else if(strrpos($sLinea,"efine('SERVER_IP'")>0){ $sLineas[$index] = "define('SERVER_IP', '" . $SERVER_IP . "');\n"; }
                      //F001
                      else if(strrpos($sLinea,"efine('PUBLICKEY'")>0){ $sLineas[$index] = "define('PUBLICKEY', '" . $file_public . "');\n"; }
                      else if(strrpos($sLinea,"efine('PRIVATEKEY'")>0){ $sLineas[$index] = "define('PRIVATEKEY', '" . $file_private . "');\n"; }
                      else if(strrpos($sLinea,"efine('MOD_INTEGRACION'")>0){ $sLineas[$index] = "define('MOD_INTEGRACION', '" . $MOD_INTEGRACION . "');\n"; }
                      else if(strrpos($sLinea,"efine('COMERCIO_CONCEPTO_PAGO'")>0){ $sLineas[$index] = "define('COMERCIO_CONCEPTO_PAGO', '" . $COMERCIO_CONCEPTO_PAGO . "');\n"; }
                      else if(strrpos($sLinea,"efine('EMAIL_PORTAL'")>0){ $sLineas[$index] = "define('EMAIL_PORTAL', '" . $EMAIL_PORTAL . "');\n"; }
                      else if(strrpos($sLinea,"efine('TIEMPO_EXPIRACION'")>0){ $sLineas[$index] = "define('TIEMPO_EXPIRACION', '" . $TIEMPO_EXPIRACION . "');\n"; }
                      else if(strrpos($sLinea,"efine('SECURITY_PATH'")>0){ $sLineas[$index] = "define('SECURITY_PATH', '" . $SECURITY_PATH . "');\n"; }
//                      else if(strrpos($sLinea,"efine('URL_NOTIFICACION'")>0){ $sLineas[$index] = "define('URL_NOTIFICACION', '" . $URL_NOTIFICACION . "');\n"; }
//                      else if(strrpos($sLinea,"efine('PATH_NOTIFICACION'")>0){ $sLineas[$index] = "define('PATH_NOTIFICACION', '" . $PATH_NOTIFICACION . "');\n"; }
                      ++$index;
                  }
                  file_put_contents($configpe, $sLineas);
                  Configuration::updateValue('PAGOEFECTIVO_CONFIG', '1');
                  $output .= $this->displayConfirmation($this->l('Configuración actualizada'));
              }
              else
              {
                 $msj_error = $count_error . " errores<br/><ol>" . $msj_error . "</ol>";
                 $output .= $this->displayError($msj_error);
              }
          }
          
          return $output.$this->displayForm();
      }

      public function displayForm()
      {
          // Init Fields form array
          $fields_form[0]['form'] = array(
              'legend' => array(
                  'title' => $this->l('CONFIGURACIÓN: PAGOEFECTIVO'),
   			      'icon' => 'icon-cogs'
              ),
              'input' => array(
                  array(
                      'type' => 'text',
                      'label' => $this->l('Merchand ID'),
                      'name' => 'MERCHAND_ID',
                      'size' => 3,
                      'maxlength' => 3,
                      'required' => true,
					  'desc' => $this->l('Código de 3 caracteres que identifica al comercio.')
                  ),
                  array(
                      'type' => 'text',
                      'label' => $this->l('CAPI'),
                      'name' => 'CAPI',
                      'size' => 50,
                      'required' => true,
					  'desc' => $this->l('Código público del comercio.')
                  ),
                  array(
                      'type' => 'text',
                      'label' => $this->l('CCLAVE'),
                      'name' => 'CCLAVE',
                      'size' => 50,
                      'required' => true,
					  'desc' => $this->l('Código privado del comercio.')
                  ),
                  array(
                      'type' => 'text',
                      'label' => $this->l('Servidor PagoEfectivo'),
                      'name' => 'SERVER',
                      'size' => 50,
                      'required' => true,
					  'desc' => $this->l('Url del servidor de PagoEfectivo (testing / producción).')
                  ),
                 //I001
/*                  array(
                      'type' => 'text',
                      'label' => $this->l('Ip Servidor PagoEfectivo'),
                      'name' => 'SERVER_IP',
                      'size' => 50,
                      'maxlength' => 16,
                      'required' => true,
					  'desc' => $this->l('Ip del servidor de PagoEfectivo (notificaciones).')
                  ),*/
                  //F001
                  array(
                      'type' => 'file',
                      'label' => $this->l('Llave Pública'),
                      'name' => 'LLAVE_PUBLICA',
                      'required' => true,
					  'desc' => $this->l('Carga la llave pública provista por PagoEfectivo.'),
                  ),
                  array(
                      'type' => 'file',
                      'label' => $this->l('Llave Privada'),
                      'name' => 'LLAVE_PRIVADA',
                      'required' => true,
					  'desc' => $this->l('Carga la llave privada provista por PagoEfectivo.'),
                  ),
                  array(
                      'type' => 'select',
                      'label' => $this->l('Modo Integración'),
                      'name' => 'MOD_INTEGRACION',
                      'required' => true,
					  'desc' => $this->l('Selecciona la modalidad de integración.'),
                      'options' => array(
                          'query' => array(
							array('key' => 1, 'name' => 'Iframe ('.$this->l('Embebido en el portal del comercio').')'),
							array('key' => 2, 'name' => 'Redirección ('.$this->l('Abre una nueva ventana').')')
    						),
    						'name' => 'name',
    						'id' => 'key'
                        )
                  ),
                  array(
                      'type' => 'text',
                      'label' => $this->l('Nombre Comercio'),
                      'name' => 'COMERCIO_CONCEPTO_PAGO',
                      'size' => 50,
                      'required' => true,
					  'desc' => $this->l('Nombre del comercio para el concepto de Pago que acompaña al numero de pedido en el banco.')
                  ),
                  array(
                      'type' => 'text',
                      'label' => $this->l('Email Comercio'),
                      'name' => 'EMAIL_PORTAL',
                      'size' => 50,
                      'required' => true,
					  'desc' => $this->l('Email del comercio para envío de comunicaciones.')
                  ),
                  array(
                      'type' => 'text',
                      'label' => $this->l('Tiempo Expiración'),
                      'name' => 'TIEMPO_EXPIRACION',
                      'size' => 50,
                      'maxlength' => 4,
                      'required' => true,
					  'desc' => $this->l('Tiempo de expiración del pago (horas).')
                  ),
              ),
              'submit' => array(
                  'title' => $this->l('Grabar'),
                  'class' => (version_compare(_PS_VERSION_, '1.6.0.0', '<')?"button":NULL)
              ),
              'buttons' => array(
				  array(
					'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
					'title' => $this->l('Regresar'),
					'icon' => 'process-icon-back'
			      )
			   )
          );

          $helper = new HelperForm();

          // Module, token and currentIndex
          $helper->module = $this;
          $helper->name_controller = $this->name;
          $helper->token = Tools::getAdminTokenLite('AdminModules');
          $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

          // Language
          $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
          $helper->default_form_language = $default_lang;
          $helper->allow_employee_form_lang = $default_lang;

          // Title and toolbar
          $helper->title = $this->displayName;
          $helper->show_toolbar = false;        // false -> remove toolbar
   		  $helper->table =  $this->table;
          $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
          $helper->submit_action = 'submit'.$this->name;
          $helper->toolbar_btn = array(
              'save' =>
              array(
                  'desc' => $this->l('Grabar'),
                  'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                  '&token='.Tools::getAdminTokenLite('AdminModules'),
              ),
              'back' => array(
                  'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                  'desc' => $this->l('Regresar')
              )
          );

          $helper->tpl_vars = array(
			'uri' => $this->getPathUri(),
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
	      );

          return $helper->generateForm($fields_form);
      }
      public function getConfigFieldsValues()
   	  {
             require_once(dirname(__FILE__) . "/lib_pagoefectivo/code/configpe.php");
   	     // require_once("/lib_pagoefectivo/code/configpe.php");
//echo "<script language='javascript'>alert('thanks!');</script>"; 
          return array(
//    	  	'PRODUCTS_VIEWED_NBR' => Tools::getValue('PRODUCTS_VIEWED_NBR', Configuration::get('PRODUCTS_VIEWED_NBR')),
            "MERCHAND_ID"=>Tools::getValue('MERCHAND_ID',MERCHAND_ID),
            "CAPI"=>Tools::getValue('CAPI',CAPI),
            "CCLAVE"=>Tools::getValue('CCLAVE',CCLAVE),
            "SERVER"=>Tools::getValue('SERVER',SERVER),
            //I001
            //"SERVER_IP"=>Tools::getValue('SERVER_IP',SERVER_IP),
            //F001
            "LLAVE_PUBLICA"=>Tools::getValue('LLAVE_PUBLICA',PUBLICKEY),
            "LLAVE_PRIVADA"=>Tools::getValue('LLAVE_PRIVADA',PRIVATEKEY),
            "MOD_INTEGRACION"=>Tools::getValue('MOD_INTEGRACION',MOD_INTEGRACION),
            "COMERCIO_CONCEPTO_PAGO"=>Tools::getValue('COMERCIO_CONCEPTO_PAGO',COMERCIO_CONCEPTO_PAGO),
            "EMAIL_PORTAL"=>Tools::getValue('EMAIL_PORTAL',EMAIL_PORTAL),
            "TIEMPO_EXPIRACION"=>Tools::getValue('TIEMPO_EXPIRACION',TIEMPO_EXPIRACION),
      	  );
      }

      /*********************************************** HOOKS ***************************************************************/
      /**
      * hookPayment($params)
      * Called in Front Office at Payment Screen - displays user this module as payment option
      */
      function hookPayment($params)
      {
            if (!$this->active)
    			return;
    		if (!$this->checkCurrency($params['cart']))
    			return;
          global $smarty;

          $smarty->assign(array(
                      'this_path' => $this->_path,
                      'this_path_pe' => $this->_path,
                      'this_version' => (version_compare(_PS_VERSION_, '1.6.0.0', '<')?"15":"16"),
                      'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/'));

          return $this->display(__FILE__, 'payment.tpl');
      }
    	public function checkCurrency($cart)
    	{
    		$currency_order = new Currency($cart->id_currency);
    		$currencies_module = $this->getCurrency($cart->id_currency);

    		if (is_array($currencies_module))
    			foreach ($currencies_module as $currency_module)
    				if ($currency_order->id == $currency_module['id_currency'])
    					return true;
    		return false;
    	}


      /*
      * This function will check display the card details form payment_execution.tpl
      * It will check if the submit button on the form has been pressed and submit the card details to the database
      */

      public function execPayment($cart)
      {
          if (!$this->active)
            return ;

          global $cookie, $smarty;

          $smarty->assign(array(
      	    'nbProducts' => $cart->nbProducts(),
      	    'cust_currency' => $cart->id_currency,
      	    'currencies' => $this->getCurrency((int) $cart->id_currency),
      	    'total' => $cart->getOrderTotal(true, Cart::BOTH),
      	    'isoCode' => Language::getIsoById((int) ($cookie->id_lang)),
      	    'moduleName' => $this->moduleName,
      	    'moduleAddress' => nl2br2($this->address),
      	    'this_path' => $this->_path,
      	    'this_path_ssl' => Tools::getShopDomainSsl(true, true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/',
      	    'id_country' => $cookie->id_pais,
            'this_version' => (version_compare(_PS_VERSION_, '1.6.0.0', '<')?"15":"16"),
          ));

          return $this->display(__FILE__, 'payment_execution.tpl');
      }

      	public function hookPaymentReturn($params)
    	{
    		if (!$this->active)
    			return;

   		    require_once(dirname(__FILE__) . "/lib_pagoefectivo/code/configpe.php");
    		$state = $params['objOrder']->getCurrentState();
    		if ($state == Configuration::get('PS_OS_BANKWIRE') )//|| $state == Configuration::get('PS_OS_OUTOFSTOCK'))
    		{
      		    require_once(dirname(__FILE__) . "/lib_pagoefectivo/code/be/be_solicitud.php");
    			$this->smarty->assign(array(
    				'total_to_pay' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
                    'TOKEN' => Tools::getValue("TOKEN"),
                    'IFRAME' => WSGENPAGOIFRAME . '?Token=' . Tools::getValue("TOKEN"),
                    'CONCEPTO_PAGO' => 'Orden '.$params['objOrder']->id.' en '. COMERCIO_CONCEPTO_PAGO,
                    'FECHA_EXPIRAR' => date('d/m/Y H:i:s', time() + ((int)TIEMPO_EXPIRACION * 60 * 60)),
    				'status' => 'ok',
    				'id_order' => $params['objOrder']->id
    			));
    			if (isset($params['objOrder']->reference) && !empty($params['objOrder']->reference))
    				$this->smarty->assign('reference', $params['objOrder']->reference);
    		}
    		else
    			$this->smarty->assign('status', 'failed');

            if(MOD_INTEGRACION=="1"){ //IFRAME
        		return $this->display(__FILE__, 'payment_return.tpl');
            }
            else{
              $redirect = WSGENPAGO . '?Token=' . Tools::getValue("TOKEN");
              Tools::redirectLink($redirect);
            }
    	}


      /********************************************** HERRAMIENTAS ************************************************************/
      public static function validateKeyUpload($file, $max_file_size = 0)
	  {
    		if ((int)$max_file_size > 0 && $file['size'] > $max_file_size)
    			return sprintf(
    				Tools::displayError('El archivo pesa demasiado (%1$d kB). Máximo permitido: %2$d kB'),
    				$file['size'] / 1000,
    				$max_file_size / 1000
    			);
    		if (substr($file['name'], -4) != '.1pz')
    			return Tools::displayError('Formato del archivo no reconocido, formato permitido: .1pz');
    		if ($file['error'])
    			return Tools::displayError('Error mientras se cargaba el archivo, por favor revise la configuración de su servidor.');
    		return false;
	  }
  }
?>