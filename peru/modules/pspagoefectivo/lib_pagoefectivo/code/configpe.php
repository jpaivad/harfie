<?php
//Configuracion de entorno
define('SERVER', 'https://pagoefectivo.pe/');
define('SERVER_IP', '');

//Rutas de Webservices
$wsCrypta= SERVER.'PagoEfectivoWSCrypto/WSCrypto.asmx';
define('WSCRYPTA', $wsCrypta.'?wsdl');
$wsCIP= SERVER.'PagoEfectivoWSGeneral/WSCIP.asmx';
define('WSCIP', $wsCIP.'?wsdl');
$wsGenPago = SERVER.'GenPago.aspx';
define('WSGENPAGO', $wsGenPago);
$wsGenPagoIframe = SERVER.'GenPagoIF.aspx';
define('WSGENPAGOIFRAME', $wsGenPagoIframe);

//Para las modalidades
$wsGenCIP = SERVER.'PagoEfectivoWSGeneralv2/service.asmx';
define('WSGENCIP', $wsGenCIP.'?wsdl');
$wsImgCIP = SERVER.'CNT/GenImgCIP.aspx';
define('WSCIPIMG', $wsImgCIP);
$wsCryptab= SERVER.'pasarela/pasarela/crypta.asmx';
define('WSCRYPTAB', $wsCryptab.'?wsdl');

//Configuracion de cuenta
//Aqui iran las claves que les proporcionemos - estan son de prueba
define('CAPI', '49849c9f-84a1-405b-858d-310848019a8b');
define('CCLAVE', '51de69ce-08df-43ce-ade5-9187646efb1e');

//Mail de la persona a la que le llegara el mail en la prueba de generacion de cip
//Este mail es de prueba, al final en vez de esta constante - se reemplazará con el mail del cliente
define('EMAIL_PORTAL', 'info@harfie.com');
define('EMAIL_CONTACTO','alex.castillo@perucomsultores.pe');
define('TIEMPO_EXPIRACION', '16');

//Este dato es unico por servicio - nosotros se lo proporcionaremos
define('MERCHAND_ID', 'HAR');
//Nombre del portal para el concepto de Pago que acompaña al numero de pedido en el banco
define('COMERCIO_CONCEPTO_PAGO', 'HARFIE');
//El dominio de pruebas o produccion al que solicitaron permisos por IP
define('DOMINIO_COMERCIO','http://'.$_SERVER['SERVER_NAME'].'/');

define('PATH',dirname(__FILE__) . DIRECTORY_SEPARATOR);

//Colocar la url de notificacion, luego enviarnoslas para configurarlas
define('URL_NOTIFICACION', 'http://127.0.0.1/PRU/2075c03d/86f4/4e0a/ae72/434fb25ab941/notificacion/notificacion.php');
define('PATH_NOTIFICACION', 'C:\Program Files (x86)\EasyPHP VC9\data\localweb\projects\prestashop\modules\pspagoefectivo\lib_pagoefectivo\code\../../../../tools/pagoefectivo/PRU/2075c03d/86f4/4e0a/ae72/434fb25ab941/notificacion/');

//Colocar la url de información
define('URL_POPUP',SERVER . 'CNT/QueEsPagoEfectivo.aspx');

//ubicacion y nombre de los archivos a usar
define('SECURITY_PRIMARY_PATH',PATH  ."../../../../urlpe/");
define('SECURITY_PATH', '/var/www/html/peru/modules/pspagoefectivo/lib_pagoefectivo/code/../../../../urlpe/HAR/51de69ce/08df/43ce/ade5/9187646efb1e/keys/');
//Estos archivos se los enviara PagoEfectivo
//nombre del archivo clave publica de PagoEfectivo
define('PUBLICKEY', '3b150ddbdfbef327b894d4152e7f8d41.1pz');
//nombre del archivo clave privada del comercio - cambiar con el prefijo de la llave - por el valor de MERCHAN_ID indicado
define('PRIVATEKEY', '1389c6aad5b8ecfbea64dc9c641eedcd.1pz');

define('MEDIO_PAGO','1,2');
define('MOD_INTEGRACION', '1');
define('MONEDA',1);
?>