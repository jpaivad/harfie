<?php

class VisaNETPeruNotifierModuleFrontController extends ModuleFrontController
{
    
    private $motivos = array(101 => "(101) Operación Denegada. Tarjeta Vencida. Verifique los datos en su tarjeta e ingréselos correctamente.",
                                102 => "(102) Operación Denegada. Contactar con entidad emisora de su tarjeta. ",
                                104 => "(104) Operación Denegada. Operación no permitida para esta tarjeta. Contactar con la entidad emisora de su tarjeta. ",
                                106 => "(106) Operación Denegada. Intentos de clave secreta excedidos. Contactar con la entidad emisora de su tarjeta. ",
                                107 => "(107) Operación Denegada. Contactar con la entidad emisora de su tarjeta. ",
                                108 => "(108) Operación Denegada. Contactar con la entidad emisora de su tarjeta. ",
                                109 => "(109) Operación Denegada. Contactar con el comercio. ",
                                110 => "(110) Operación Denegada. Operación no permitida para esta tarjeta. Contactar con la entidad emisora de su tarjeta. ",
                                111 => "(111) Operación Denegada. Contactar con el comercio. ",
                                112 => "(112) Operación Denegada. Se requiere clave secreta. ",
                                116 => "(116) Operación Denegada. Fondos insuficientes. Contactar con entidad emisora de su tarjeta ",
                                117 => "(117) Operación Denegada. Clave secreta incorrecta. ",
                                118 => "(118) Operación Denegada. Tarjeta Inválida. Contactar con entidad emisora de su tarjeta. ",
                                119 => "(119) Operación Denegada. Intentos de clave secreta excedidos. Contactar con entidad emisora de su tarjeta. ",
                                121 => "(121) Operación Denegada. ",
                                126 => "(126) Operación Denegada. Clave secreta inválida. ",
                                129 => "(129) Operación Denegada. Código de seguridad invalido. Contactar con entidad emisora de su tarjeta ",
                                180 => "(180) Operación Denegada. Tarjeta Inválida. Contactar con entidad emisora de su tarjeta. ",
                                181 => "(181) Operación Denegada. Tarjeta con restricciones de débito. Contactar con entidad emisora de su tarjeta. ",
                                182 => "(182) Operación Denegada. Tarjeta con restricciones de crédito. Contactar con entidad emisora de su tarjeta. ",
                                183 => "(183) Operación Denegada. Problemas de comunicación. Intente más tarde. ",
                                190 => "(190) Operación Denegada. Contactar con entidad emisora de su tarjeta. ",
                                191 => "(191) Operación Denegada. Contactar con entidad emisora de su tarjeta. ",
                                192 => "(192) Operación Denegada. Contactar con entidad emisora de su tarjeta. ",
                                199 => "(199) Operación Denegada. ",
                                201 => "(201) Operación Denegada. Tarjeta vencida. Contactar con entidad emisora de su tarjeta. ",
                                202 => "(202) Operación Denegada. Contactar con entidad emisora de su tarjeta ",
                                204 => "(204) Operación Denegada. Operación no permitida para esta tarjeta. Contactar con entidad emisora de su tarjeta. ",
                                206 => "(206) Operación Denegada. Intentos de clave secreta excedidos. Contactar con la entidad emisora de su tarjeta. ",
                                207 => "(207) Operación Denegada. Contactar con entidad emisora de su tarjeta.. ",
                                208 => "(208) Operación Denegada. Contactar con entidad emisora de su tarjeta. ",
                                209 => "(209) Operación Denegada. Contactar con entidad emisora de su tarjeta ",
                                263 => "(263) Operación Denegada. Contactar con el comercio. ",
                                264 => "(264) Operación Denegada. Entidad emisora de la tarjeta no está disponible para realizar la autenticación. ",
                                265 => "(265) Operación Denegada. Clave secreta del tarjetahabiente incorrecta. Contactar con entidad emisora de su tarjeta. ",
                                266 => "(266) Operación Denegada. Tarjeta Vencida. Contactar con entidad emisora de su tarjeta. ",
                                280 => "(280) Operación Denegada. Clave secreta errónea. Contactar con entidad emisora de su tarjeta. ",
                                290 => "(290) Operación Denegada. Contactar con entidad emisora de su tarjeta. ",
                                300 => "(300) Operación Denegada. Número de pedido del comercio duplicado. Favor no atender. ",
                                306 => "(306) Operación Denegada. Contactar con entidad emisora de su tarjeta. ",
                                401 => "(401) Operación Denegada. Contactar con el comercio. ",
                                402 => "(402) Operación Denegada. ",
                                403 => "(403) Operación Denegada. Tarjeta no autenticada. ",
                                404 => "(404) Operación Denegada. Contactar con el comercio. ",
                                405 => "(405) Operación Denegada. Contactar con el comercio. ",
                                406 => "(406) Operación Denegada. Contactar con el comercio. ",
                                407 => "(407) Operación Denegada. Contactar con el comercio. ",
                                408 => "(408) Operación Denegada. Código de seguridad no coincide. Contactar con entidad emisora de su tarjeta ",
                                409 => "(409) Operación Denegada. Código de seguridad no procesado por la entidad emisora de la tarjeta ",
                                410 => "(410) Operación Denegada. Código de seguridad no ingresado. ",
                                411 => "(411) Operación Denegada. Código de seguridad no procesado por la entidad emisora de la tarjeta  ",
                                412 => "(412) Operación Denegada. Código de seguridad no reconocido por la entidad emisora de la tarjeta ",
                                413 => "(413) Operación Denegada. Contactar con entidad emisora de su tarjeta. ",
                                414 => "(414) Operación Denegada. ",
                                415 => "(415) Operación Denegada. ",
                                416 => "(416) Operación Denegada. ",
                                417 => "(417) Operación Denegada. ",
                                418 => "(418) Operación Denegada. ",
                                419 => "(419) Operación Denegada. ",
                                420 => "(420) Operación Denegada. Tarjeta no es VISA. ",
                                421 => "(421) Operación Denegada. Contactar con entidad emisora de su tarjeta. ",
                                422 => "(422) Operación Denegada. El comercio no está configurado para usar este medio de pago. Contactar con el comercio. ",
                                423 => "(423) Operación Denegada. Se canceló el proceso de pago. ",
                                424 => "(424) Operación Denegada. ",
                                666 => "(666) Operación Denegada. Problemas de comunicación. Intente más tarde. ",
                                667 => "(667) Operación Denegada. Transacción sin respuesta de Verified by Visa. ",
                                668 => "(668) Operación Denegada. Contactar con el comercio. ",
                                669 => "(669) Operación Denegada. Contactar con el comercio. ",
                                670 => "(670) Operación Denegada. Contactar con el comercio. ",
                                672 => "(672) Operación Denegada. Módulo antifraude. ",
                                673 => "(673) Operación Denegada. Contactar con el comercio. ",
                                674 => "(674) Operación Denegada. Contactar con el comercio. ",
                                676 => "(676) Operación Denegada. Contactar con el comercio. ",
                                677 => "(677) Operación Denegada. Contactar con el comercio. ",
                                678 => "(678) Operación Denegada. Contactar con el comercio. ",
                                904 => "(904) Operación Denegada. ",
                                909 => "(909) Operación Denegada. Problemas de comunicación. Intente más tarde. ",
                                910 => "(910) Operación Denegada. ",
                                912 => "(912) Operación Denegada. Entidad emisora de la tarjeta no disponible ",
                                913 => "(913) Operación Denegada. ",
                                916 => "(916) Operación Denegada. ",
                                928 => "(928) Operación Denegada. ",
                                940 => "(940) Operación Denegada. ",
                                941 => "(941) Operación Denegada. ",
                                942 => "(942) Operación Denegada. ",
                                943 => "(943) Operación Denegada. ",
                                945 => "(945) Operación Denegada. ",
                                946 => "(946) Operación Denegada. Operación de anulación en proceso. ",
                                947 => "(947) Operación Denegada. Problemas de comunicación. Intente más tarde. ",
                                948 => "(948) Operación Denegada. ",
                                949 => "(949) Operación Denegada. ",
                                965 => "(965) Operación Denegada. ");
                             



    public function postProcess(){
        

        $eticket = $_GET['eticket'];
        

        
        $cart = $this->context->cart;
        $customer = new Customer($cart->id_customer);
        $currency = $this->context->currency;
        $total = (float)$cart->getOrderTotal(true, Cart::BOTH);
        
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
        $xml.= '<consulta_eticket>';
        $xml.= '<parametros>';
        $xml.= '	<parametro id="CODTIENDA">'.$this->module->codTienda.'</parametro>';
        $xml.= '	<parametro id="ETICKET">'.$eticket.'</parametro>';
        $xml.= '</parametros>';
        $xml.= '</consulta_eticket>';
        $client = new SoapClient($this->module->connections[$this->module->enviroment]["ws_consulta"]);
        //~ $rt=$client->__doRequest($request, $location, $action, $version);
        
        #Consulta el estado
        $parametros = array("xmlIn" => $xml);
        $result = $client->ConsultaEticket($parametros);
        $xmlDocument = new DOMDocument();
        if ($xmlDocument->loadXML($result->ConsultaEticketResult)) {
            //~ $orden_id = $this->module->isAprobada($xmlDocument);
            if ($this->module->isAprobada($xmlDocument)){
                $_SESSION['visanetperu_response'] = $this->module->getData($xmlDocument);
                $_SESSION['visanetperu_response']['products'] = $cart->getProducts();
                $this->module->validateOrder((int)$cart->id, Configuration::get('PS_OS_PAYMENT'), $total, $this->module->displayName, NULL, NULL, (int)$currency->id, false, $customer->secure_key);
                Tools::redirect('index.php?controller=order-confirmation&id_cart='.(int)$cart->id.'&id_module='.(int)$this->module->id.'&id_order='.$this->module->currentOrder.'&key='.$customer->secure_key);
            }else{
                $this->module->validateOrder((int)$cart->id, Configuration::get('PS_OS_ERROR'), $total, $this->module->displayName, NULL, NULL, (int)$currency->id, false, $customer->secure_key);
                $data = $this->module->getData($xmlDocument);
                $this->context->smarty->assign(array(
                    'pedido' => $data['nordent'],
                    'fecha' => $data['fechayhora_tx'],
                    'motivo' => $this->motivos[$data['cod_accion']]
                ));
                
                return $this->setTemplate('payment_error.tpl');
            }
            
        }
        return $this->setTemplate('payment_error.tpl');
    }
}
?>
