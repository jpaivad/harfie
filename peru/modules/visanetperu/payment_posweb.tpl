<div class="row">
    <div class="col-xs-12 col-md-6">
        <p class="payment_module">
            <a href="javascript:void(0)" onclick="$('#visanet_peru_form').submit();" id="visanetperu_process_payment" title="{l s='Pagar con tarjeta de crédito' mod='visanetperu'}">
                <img src="{$this_path}/logo.png" alt="{l s='Pay with credit card' mod='visanetperu'}" width="86" height="49" />
                {l s='Pagar con tarjeta de crédito.' mod='visanetperu'}
            </a>
        </p>

        <form style="display: none" id="visanet_peru_form" method="post" target="_blank" action="{$action_url}">
            <input type="hidden" name="codtienda" value="{$cod_tienda}" />
            <input type="hidden" name="numcompra" value="{$num_compra}" />
            <input type="hidden" name="mount" value="{$mount}" />
        </form>
    </div>
</div>
