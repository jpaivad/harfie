<h2>{l s='Error en el pago'}</h2>

<div style="width:100%; height:120px; margin: 20px 0 20px 15px;">
	<div style="float:left; width:60%; text-align:left;">
		<p>{l s='Existe un error en su pago. Puede intentar realizar el pedido eligiendo otro sistema de pago o contactar con nosotros.' mod='visanetperu'}</p>
        <br />{l s='Número de pedido:' mod='visanetperu'} <b>{$pedido}</b>
        <br />{l s='Fecha y hora:' mod='visanetperu'} <b>{$fecha}</b>
        <br />{l s='Motivo:' mod='visanetperu'} <b>{$motivo}</b>
	</div>
</div>
<div class="clear"></div>
<br /><br />
<div></div>{l s='Para resolver cualquier no due en ponerse en contacto con nosotros' mod='visanetperu'}
	<a href="{$link->getPageLink('contact', true)}" data-ajax="false" target="_blank">{l s='Servicio al cliente' mod='visanetperu'}</a>.
</div>
