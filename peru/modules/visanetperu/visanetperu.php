<?php
session_start();
/*
*
*/
ini_set('display_errors','1');
ini_set("error_reporting",E_ALL);
if (!defined('_PS_VERSION_'))
    exit;

class VisaNetPeru extends PaymentModule
{

    public $enviroment = "development"; #"production"; # "development";
    public $codTienda = "123456789";
    public $posWeb = false;
    
    public $connections = array("development"=>array("ws_genera"=>"http://qas.multimerchantvisanet.com/WSGenerarEticket/WSEticket.asmx?wsdl",
                                                   "form_pago"=>"http://qas.multimerchantvisanet.com/formularioweb/formulariopago.aspx",
                                                   "ws_consulta"=>"http://qas.multimerchantvisanet.com/WSConsulta/WSConsultaEticket.asmx?wsdl"),
                               "production"=>array("ws_genera"=>"https://www.multimerchantvisanet.com/WSGenerarEticket/WSEticket.asmx?wsdl",
                                                   "form_pago"=>"https://www.multimerchantvisanet.com/formularioweb/formulariopago.asp",
                                                   "ws_consulta"=>"https://www.multimerchantvisanet.com/WSConsulta/WSConsultaEticket.asmx?wsdl"));



                                                   
    public function __construct()
    {
        $this->name = 'visanetperu';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.0';

        parent::__construct();

        $this->displayName = $this->l('VisaNet Peru');
        $this->description = $this->l('Accepts payments by credit visaNet Peru.');
        $this->confirmUninstall = $this->l('Are you sure you want to delete your details?');
        Configuration::updateValue("PS_OS_PAYMENT", 2);
        
        
        //cod Tienda
        $codTienda = Configuration::get('VISANET_PERU_COD_TIENDA');
        if ($codTienda)  
			$this->codTienda = $codTienda;
        
        //cod Tienda
        $posWeb = Configuration::get('VISANET_PERU_POS_WEB');
        if ($posWeb)  
			$this->posWeb = $posWeb;
        
        //enviroment
        $enviroment = Configuration::get('VISANET_PERU_ENVIROMENT');
        if ($enviroment)  
			$this->enviroment = $enviroment;
			
			
		//development ws genera
		$devWsGenera = Configuration::get('VISANET_PERU_DEV_WS_GENERA');
        if ($devWsGenera)  
			$this->connections['development']['ws_genera'] = $devWsGenera;
			
		//development form pago
		$devFormPago = Configuration::get('VISANET_PERU_DEV_FORM_PAGO');
        if ($devFormPago)  
			$this->connections['development']['form_pago'] = $devFormPago;
        
        //development ws consulta
		$devWsConsulta = Configuration::get('VISANET_PERU_DEV_WS_CONSULTA');
        if ($devWsConsulta)  
			$this->connections['development']['ws_consulta'] = $devWsConsulta;
			
		//production ws genera
		$prodWsGenera = Configuration::get('VISANET_PERU_PROD_WS_GENERA');
        if ($prodWsGenera)  
			$this->connections['production']['ws_genera'] = $prodWsGenera;
			
		//production form pago
		$prodFormPago = Configuration::get('VISANET_PERU_PROD_FORM_PAGO');
        if ($prodFormPago)  
			$this->connections['production']['form_pago'] = $prodFormPago;
        
        //production ws consulta
		$prodWsConsulta = Configuration::get('VISANET_PERU_PROD_WS_CONSULTA');
        if ($prodWsConsulta)  
			$this->connections['production']['ws_consulta'] = $prodWsConsulta;
		
			
		$context = Context::getContext();
		$this->notifier = "http://".$_SERVER['SERVER_NAME'].$context->shop->getBaseURI()."modules/visanetperu/notifier.php";
		
        
        

    }

    public function install()
    {
        if (!parent::install() OR !$this->registerHook('payment') OR !$this->registerHook('paymentReturn') OR !$this->registerHook('footer')){
            return false;
        }

        return true;
    }

    public function uninstall()
    {
        return parent::uninstall();
    }
    
    public function getContent()
    {
        $output = '<h2>'.$this->displayName.'</h2>';
        if (Tools::isSubmit('submitVisanetPeru'))
        {
            $this->codTienda = Tools::getValue('cod_tienda');
            Configuration::updateValue('VISANET_PERU_COD_TIENDA', $this->codTienda);
            
            $this->posWeb = Tools::getValue('pos_web');
            Configuration::updateValue('VISANET_PERU_POS_WEB', $this->posWeb);
            
            $this->enviroment = Tools::getValue('enviroment');
            Configuration::updateValue('VISANET_PERU_ENVIROMENT', $this->enviroment);
            
            $this->connections['development']['ws_genera'] = Tools::getValue('dev_ws_genera');
            Configuration::updateValue('VISANET_PERU_DEV_WS_GENERA', $this->connections['development']['ws_genera']);
            
            $this->connections['development']['form_pago'] = Tools::getValue('dev_form_pago');
            Configuration::updateValue('VISANET_PERU_DEV_FORM_PAGO', $this->connections['development']['form_pago']);
            
            $this->connections['development']['ws_consulta'] = Tools::getValue('dev_ws_consulta');
            Configuration::updateValue('VISANET_PERU_DEV_WS_CONSULTA', $this->connections['development']['ws_consulta']);
            
            
            $this->connections['production']['ws_genera'] = Tools::getValue('prod_ws_genera');
            Configuration::updateValue('VISANET_PERU_PROD_WS_GENERA', $this->connections['production']['ws_genera']);
            
            $this->connections['production']['form_pago'] = Tools::getValue('prod_form_pago');
            Configuration::updateValue('VISANET_PERU_PROD_FORM_PAGO', $this->connections['production']['form_pago']);
            
            $this->connections['production']['ws_consulta'] = Tools::getValue('prod_ws_consulta');
            Configuration::updateValue('VISANET_PERU_PROD_WS_CONSULTA', $this->connections['production']['ws_consulta']);
            
            
            
        }
        return $this->displayForm();
    }

    public function displayForm()
    {
        return '
        <form action="'.$_SERVER['REQUEST_URI'].'" method="post">
            <fieldset>
                <legend><img src="'.$this->_path.'/logo.png" alt="" title="" />'.$this->l('Settings').'</legend>
                <label>'.$this->l('Cód. Tienda').'</label>
                <div class="margin-form">
                    <input type="text" name="cod_tienda" value="'.Configuration::get('VISANET_PERU_COD_TIENDA').'" />
                </div>
                <label>'.$this->l('Modo POS WEB').'</label>
                <div class="margin-form">
                    <input type="checkbox" name="pos_web" '.($this->posWeb?"checked=\"checked\"":"").' />
                </div>
                <div>
					<label>'.$this->l('Ambiente').'</label>
					<div class="margin-form">
						<select name="enviroment">
							<option value="production" '.($this->enviroment=="production"?"selected=\"selected\"":"").'>'.$this->l('Producción').'</option>
							<option value="development" '.($this->enviroment=="development"?"selected=\"selected\"":"").'>'.$this->l('Pruebas').'</option>
						</select>
					</div>
				</div>
                <div style="margin-top:20px;">
					<label>'.$this->l('url ws genera pruebas').'</label>
					<div class="margin-form">
						<input type="text" name="dev_ws_genera" size="80" value="'.$this->connections['development']['ws_genera'].'" />
					</div>
					<label>'.$this->l('url form pago pruebas').'</label>
					<div class="margin-form">
						<input type="text" name="dev_form_pago" size="80" value="'.$this->connections['development']['form_pago'].'" />
					</div>
					<label>'.$this->l('url ws consulta pruebas').'</label>
					<div class="margin-form">
						<input type="text" name="dev_ws_consulta" size="80" value="'.$this->connections['development']['ws_consulta'].'" />
					</div>
				</div>
                <div style="margin-top:20px;">
					<label>'.$this->l('url ws genera producción').'</label>
					<div class="margin-form">
						<input type="text" name="prod_ws_genera" size="80" value="'.$this->connections['production']['ws_genera'].'" />
					</div>
					<label>'.$this->l('url form pago producción').'</label>
					<div class="margin-form">
						<input type="text" name="prod_form_pago" size="80" value="'.$this->connections['production']['form_pago'].'" />
					</div>
					<label>'.$this->l('url ws consulta producción"').'</label>
					<div class="margin-form">
						<input type="text" name="prod_ws_consulta" size="80" value="'.$this->connections['production']['ws_consulta'].'" />
					</div>
				</div>
				<div style="margin-top:20px;">
					<label>'.$this->l('Url respuesta visaNet').'</label>
					<div class="margin-form">
						<input type="text" size="80" readonly="readonly" value="'.$this->notifier.'" />
						
					</div>
				</div>
                
                <center><input type="submit" name="submitVisanetPeru" value="'.$this->l('Save').'" class="button" /></center>
            </fieldset>
        </form>';
    }
    
    function eTicketOK($xmlDoc){
        if ($xmlDoc == null){
            return false;
        }
        
        $cantMensajes= 0;
        $xpath = new DOMXPath($xmlDoc);
        $nodeList = $xpath->query('//mensajes', $xmlDoc);
        
        $XmlNode= $nodeList->item(0);
        
        
        
        if($XmlNode==null){
            $cantMensajes= 0;
        }else{
            $cantMensajes= $XmlNode->childNodes->length;
        }
        //~ var_dump($cantMensajes);
        //~ echo "<br>";
        return $cantMensajes==0; 
    }
    
    private function recuperaEticket($xmlDoc){
        $strReturn = "";
        
        $xpath = new DOMXPath($xmlDoc);
        $nodeList = $xpath->query("//registro/campo[@id='ETICKET']");
        //~ var_dump($xpath);
        //~ var_dump($nodeList);
        
        $XmlNode= $nodeList->item(0);
        
        if($XmlNode==null){
            $strReturn = "";
        }else{
            $strReturn = $XmlNode->nodeValue;
        }
        return $strReturn;
	}
    
    public function isAprobada($xmlDoc){
        #Si está aprobada devuelve el número de orden
        $xpath = new DOMXPath($xmlDoc);
        
        $nodeList = $xpath->query("//operacion[@id='1']/campo[@id='respuesta']");
        
        $XmlNode= $nodeList->item(0);
        
        if($XmlNode!=null){
            if ($XmlNode->nodeValue == 1){
                $nodeList = $xpath->query("//operacion[@id='1']/campo[@id='nordent']");
                return $nodeList->item(0)->nodeValue;
            }
        }
        return false;
	}
    
    public function getData($xmlDoc){
        #Si está aprobada devuelve el número de orden

        $xpath = new DOMXPath($xmlDoc);
        
        
        $data = array();
        $keys = array("nordent", "cod_accion", "pan", "nombre_th", "imp_autorizado", "fechayhora_tx"); 
        foreach($keys as $key){
            $nodeList = $xpath->query("//operacion[@id='1']/campo[@id='".$key."']");
            $data[$key] = $XmlNode= $nodeList->item(0)->nodeValue;
        }
        
        return $data;
        
	}
    public function hookPayment($params){
        
        $xmlDocument = Null;
        
        $numOrden = $params['cart']->id;
        
        $intentos = 50;
        
        $cart_details = $params['cart']->getSummaryDetails(null, true);
        
        #factor de conversión a soles que es la única moneda que acepta el WS
        $conversion_rate = Currency::getCurrencyInstance(Currency::getIdByIsoCode('PEN'))->conversion_rate / @Currency::getCurrent()->conversion_rate;
        if(!$conversion_rate)
                $conversion_rate = 1;

        
        
        if(!$this->posWeb){
            
            while (!$this->eTicketOK($xmlDocument) && $intentos >0){
                $intentos--;
                #Realiza la petición de pago
                //~ echo $numOrden."<br>";
                
            
                $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
                $xml .= '<nuevo_eticket>';
                $xml .= '    <parametros>';
                $xml .= '        <parametro id="CANAL">3</parametro>';
                $xml .= '        <parametro id="PRODUCTO">1</parametro>';
                $xml .= '        <parametro id="CODTIENDA">'.$this->codTienda.'</parametro>';
                $xml .= '        <parametro id="NUMORDEN">'.$numOrden.'</parametro>';
                $xml .= '        <parametro id="MOUNT">'.number_format((float)round(($cart_details['total_price'] * $conversion_rate),2), 2, '.', '').'</parametro>';
                $xml .= '        <parametro id="DATO_COMERCIO">DATOS</parametro>';
                $xml .= '    </parametros>';
                $xml .= '</nuevo_eticket>';
                
                
                //~ var_dump($xml);
                
                //~ $xmlIn = $xmlIn . "		<parametro id=\"NOMBRE\">" . $nombre . "</parametro>";
                //~ $xmlIn = $xmlIn . "		<parametro id=\"APELLIDO\">" . $apellido . "</parametro>";
                //~ $xmlIn = $xmlIn . "		<parametro id=\"CIUDAD\">" . $ciudad . "</parametro>";
                //~ $xmlIn = $xmlIn . "		<parametro id=\"DIRECCION\">" . $direccion . "</parametro>";
                //~ $xmlIn = $xmlIn . "		<parametro id=\"CORREO\">" . $correo . "</parametro>";
                //~ 
                //~ $xmlIn = $xmlIn . "		<parametro id=\"DATO_COMERCIO\">" . $datoComercio . "</parametro>";
                //~ $xmlIn = $xmlIn . "	</parametros>";
                //~ $xmlIn = $xmlIn . "</nuevo_eticket>";
                
                
                $client = new SoapClient($this->connections[$this->enviroment]["ws_genera"]);
                #Solicita el ticket
                $parametros = array("xmlIn" => $xml);
                $result = $client->GeneraEticket($parametros);
                
                
                
                //~ die;
                
                $xmlDocument = new DOMDocument();
                
                if ($xmlDocument->loadXML($result->GeneraEticketResult)){
                    if ($this->eTicketOK($xmlDocument)){
                        $eticket = $this->recuperaEticket($xmlDocument);
                    }else{
                        $numOrden = $numOrden + floor($numOrden/10);
                    }
                }else{
                    //~ var_dump($result);
                    die("Respuesta no valida");
                }
            }
            if (!isset($eticket)){
                var_dump($result);
                die("Error obtienendo eticket visanet");
            }
            
            #Redirecciona al formulario de pago
            $this->context->smarty->assign(array(
                'action_url' => $this->connections[$this->enviroment]["form_pago"],
                'eticket' => $eticket,
                'this_path' => $this->_path
                
            ));
            return $this->display(__FILE__, 'payment.tpl');
        }else{
            #Redirecciona al formulario de pago
            $this->context->smarty->assign(array(
                'action_url' => $this->connections[$this->enviroment]["form_pago"],
                'this_path' => $this->_path,
                'cod_tienda' => $this->codTienda,
                'num_compra' => $numOrden,
                'mount' => number_format((float)round(($cart_details['total_price'] * $conversion_rate),2), 2, '.', '')
            ));
            
            return $this->display(__FILE__, 'payment_posweb.tpl');
        }

        

    }
    
    public function hookPaymentReturn()
	{
        //"nordent", "cod_accion", "pan", "nombre_th", "imp_autorizado", "fechayhora_tx"
        
        if (count($_SESSION['visanetperu_response']['products'])==1){
            $producto = $_SESSION['visanetperu_response']['products'][0]['name'];
        }else{
            $producto = "Varios productos: ";
            $count = count($_SESSION['visanetperu_response']['products']);
            $idx = 1;
            
            foreach ($_SESSION['visanetperu_response']['products'] as $product){
                if ($idx > 1 && $idx < $count)
                    $producto .= ", ";
                if ($idx == $count)
                    $producto .= " y ";
                $producto .= "<i>".$product['name']."</i>";
                $idx++;
            }
        }

        $this->context->smarty->assign(array(
            'pedido' => $_SESSION['visanetperu_response']['nordent'],
            'nombre' => $_SESSION['visanetperu_response']['nombre_th'],
            'tarjeta' => $_SESSION['visanetperu_response']['pan'],
            'fecha' => $_SESSION['visanetperu_response']['fechayhora_tx'],
            'moneda' => Currency::getCurrencyInstance(Currency::getIdByIsoCode('PEN'))->name,
            'importe' => $_SESSION['visanetperu_response']['imp_autorizado'],
            'producto' => $producto
            
        ));
		return $this->display(__FILE__, 'confirmation.tpl');
	}
    
    public function hookFooter(){
        $this->context->smarty->assign(array(
            'this_path' => $this->_path
        ));
        return $this->display(__FILE__, 'footer.tpl');
    }

}
