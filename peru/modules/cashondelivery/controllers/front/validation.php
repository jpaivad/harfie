<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @since 1.5.0
 */
 	session_start(); 
	include('lib.inc');

	error_reporting(E_ALL);
	ini_set("display_errors", 1);
	ini_set('date.timezone', 'America/Lima'); 
	header( 'Content-Type: text/html;charset=utf-8' );
	
	define('URL_FORMULARIO_VISA','http://qas.multimerchantvisanet.com/formularioweb/formulariopago.aspx');
	define('URL_WSGENERAETICKET_VISA','http://qas.multimerchantvisanet.com/WSGenerarEticket/WSEticket.asmx?wsdl');
	define('URL_WSCONSULTAETICKET_VISA','http://qas.multimerchantvisanet.com/WSConsulta/WSConsultaEticket.asmx?wsdl');

	
class CashondeliveryValidationModuleFrontController extends ModuleFrontController
{
	public $ssl = true;
	public $display_column_left = false;
	
		//Funcion que genera Numero de Pedido
	public function NumPedido(){
		$archivo = "NumPedido.txt"; 
		$numPedido = 0; 
		
		$fp = fopen($archivo,"r"); 
		$numPedido = fgets($fp, 26); 
		fclose($fp); 
		
		++$numPedido; 
		
		$fp = fopen($archivo,"w+"); 
		fwrite($fp, $numPedido, 26); 
		fclose($fp); 
		
		return $numPedido;
	}
	function TestMsg(){
		return 333666;
	}

	public function postProcess()
	{
	
	
		
		//echo '<script type="text/javascript">alert(" '.NumPedido().'"); </script>';
		
		//if (isset($_POST["btPagar_x"])) {
		//Se asigna el código de comercio y Nro. de pedido
		$numPedido= 1370;//$_POST["numPedido"];
		$codTienda= 544651601;
		$mount= $_POST["montoTotal"];
		$nombre= $_POST["nombre"];
		$apellido= $_POST["apellido"];
		$ciudad= $_POST["ciudad"];
		$direccion= $_POST["direccion"];
		$correo= $_POST["correo"];
		
		$datoComercio= "EJEMPLO VISANET";
		
		//Se arma el XML de requerimiento
		$xmlIn = "";
		$xmlIn = $xmlIn . "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
		$xmlIn = $xmlIn . "<nuevo_eticket>";
		$xmlIn = $xmlIn . "	<parametros>";
		$xmlIn = $xmlIn . "		<parametro id=\"CANAL\">3</parametro>";
		$xmlIn = $xmlIn . "		<parametro id=\"PRODUCTO\">1</parametro>";
		$xmlIn = $xmlIn . "		<parametro id=\"CODTIENDA\">" . $codTienda . "</parametro>";
		$xmlIn = $xmlIn . "		<parametro id=\"NUMORDEN\">" . $numPedido . "</parametro>";
		$xmlIn = $xmlIn . "		<parametro id=\"MOUNT\">" . $mount . "</parametro>";
		
		$xmlIn = $xmlIn . "		<parametro id=\"NOMBRE\">" . $nombre . "</parametro>";
		$xmlIn = $xmlIn . "		<parametro id=\"APELLIDO\">" . $apellido . "</parametro>";
		$xmlIn = $xmlIn . "		<parametro id=\"CIUDAD\">" . $ciudad . "</parametro>";
		$xmlIn = $xmlIn . "		<parametro id=\"DIRECCION\">" . $direccion . "</parametro>";
		$xmlIn = $xmlIn . "		<parametro id=\"CORREO\">" . $correo . "</parametro>";
		
		$xmlIn = $xmlIn . "		<parametro id=\"DATO_COMERCIO\">" . $datoComercio . "</parametro>";
		$xmlIn = $xmlIn . "	</parametros>";
		$xmlIn = $xmlIn . "</nuevo_eticket>";
		
		//Se asigna la url del servicio
		$servicio= URL_WSGENERAETICKET_VISA;
		
		//Invocación del web service
		$conf=array();
		//Se habilita el parametro PROXY_ON en el archivo "lib.inc" si se maneja algun proxy para realizar conexiones externas.
		if(PROXY_ON == true){
			$conf=array('proxy_host'     => PROXY_HOST,
		                    'proxy_port'     => PROXY_PORT,
		                    'proxy_login'    => PROXY_LOGIN,
		                    'proxy_password' => PROXY_PASSWORD);
		}
		
		$client = new SoapClient($servicio, $conf);
    
    //parametros de la llamada
		$parametros=array(); 
		$parametros['xmlIn']= $xmlIn;
		
		//Aqui captura la cadena de resultado
		$result = $client->GeneraEticket($parametros);
		//Muestra la cadena recibida
		//echo 'Cadena de respuesta: ' . $result->GeneraEticketResult . '<br>' . '<br>';
		
		//Aqui carga la cadena resultado en un XMLDocument (DOMDocument)
		$xmlDocument = new DOMDocument();
		
		if ($xmlDocument->loadXML($result->GeneraEticketResult)){
			/////////////////////////[MENSAJES]////////////////////////
			//Ejemplo para determinar la cantidad de mensajes en el XML
			$iCantMensajes= CantidadMensajes($xmlDocument);
			//echo 'Cantidad de Mensajes: ' . $iCantMensajes . '<br>';
			
			//Ejemplo para mostrar los mensajes del XML 
			for($iNumMensaje=0;$iNumMensaje < $iCantMensajes; $iNumMensaje++){
				echo 'Mensaje #' . ($iNumMensaje +1) . ': ';
				echo RecuperaMensaje($xmlDocument, $iNumMensaje+1);
				echo '<BR>';
				echo "Numero de pedido: " . $numPedido;
			}
			/////////////////////////[MENSAJES]////////////////////////
			
			if ($iCantMensajes == 0){
				$Eticket= RecuperaEticket($xmlDocument);
				//echo 'Eticket: ' . $Eticket;
				
				$html= htmlRedirecFormEticket($Eticket);
				echo $html;
				
				exit;
			}
					
		}else{
			echo "Error cargando XML";
		}	
	//	}
	}

	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent();

		$this->context->smarty->assign(array(
			'total' => $this->context->cart->getOrderTotal(true, Cart::BOTH),
			'this_path' => $this->module->getPathUri(),//keep for retro compat
			'this_path_cod' => $this->module->getPathUri(),
			'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->module->name.'/'
		));

		$this->setTemplate('validation.tpl');
	}
	//Funcion de ejemplo que obtiene la cantidad de mensajes
	public function CantidadMensajes($xmlDoc){
		$cantMensajes= 0;
		$xpath = new DOMXPath($xmlDoc);
		$nodeList = $xpath->query('//mensajes', $xmlDoc);
		
		$XmlNode= $nodeList->item(0);
		
		if($XmlNode==null){
			$cantMensajes= 0;
		}else{
			$cantMensajes= $XmlNode->childNodes->length;
		}
		return $cantMensajes; 
	}
	//Funcion que recupera el valor de uno de los mensajes XML de respuesta
	public  function RecuperaMensaje($xmlDoc,$iNumMensaje){
		$strReturn = "";
			
			$xpath = new DOMXPath($xmlDoc);
			$nodeList = $xpath->query("//mensajes/mensaje[@id='" . $iNumMensaje . "']");
			
			$XmlNode= $nodeList->item(0);
			
			if($XmlNode==null){
				$strReturn = "";
			}else{
				$strReturn = $XmlNode->nodeValue;
			}
			return $strReturn;
	}
	//Funcion que recupera el valor del Eticket
	public  function RecuperaEticket($xmlDoc){
		$strReturn = "";
			
			$xpath = new DOMXPath($xmlDoc);
			$nodeList = $xpath->query("//registro/campo[@id='ETICKET']");
			
			$XmlNode= $nodeList->item(0);
			
			if($XmlNode==null){
				$strReturn = "";
			}else{
				$strReturn = $XmlNode->nodeValue;
			}
			return $strReturn;
	}
}