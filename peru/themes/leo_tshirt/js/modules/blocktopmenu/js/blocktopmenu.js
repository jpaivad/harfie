/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

var responsiveflagMenu = false;
var categoryMenu = $('ul.sf-menu');
var mCategoryGrover = $('#menu-wrap .cat-title');

$(document).ready(function(){
	
	categoryMenu = $('ul.sf-menu');
	mCategoryGrover = $('#menu-wrap .cat-title');
	responsiveMenu();
	$(window).resize(responsiveMenu);

});

// check resolution
function responsiveMenu()
{
   if ($(document).width() <= 991 && responsiveflagMenu == false)
	{
			menuChange('enable');
		responsiveflagMenu = true;
	}
	else if ($(document).width() >= 992)
	{
		menuChange('disable');
		responsiveflagMenu = false;
	}
}

// init Super Fish Menu for 991px+ resolution
function desktopInit()
{
	$('#block_top_menu > ul >li.mnudel').remove();

	$('#divcarrritotest2').hide();
	$('.cartminionlye').hide();
	$('.buscarbarrasear').show();


	mCategoryGrover.off();
	mCategoryGrover.removeClass('active');
	$('.sf-menu > li > ul').removeClass('menu-mobile').parent().find('.menu-mobile-grover').remove();
	$('.sf-menu').removeAttr('style');
	categoryMenu.superfish('init');
	//add class for width define
	$('.sf-menu > li > ul').addClass('submenu-container clearfix'); 
	 // loop through each sublist under each top list item
    $('.sf-menu > li > ul').each(function(){
        i = 0;
        //add classes for clearing
        $(this).each(function(){ 
            if ($(this).attr('id') != "category-thumbnail"){
                i++;
                if(i % 2 == 1)
                    $(this).addClass('first-in-line-xs');
                else if (i % 5 == 1)
                    $(this).addClass('first-in-line-lg');
            }
        });
    });
}

function mobileInit()
{


	categoryMenu.superfish('destroy');
	$('.sf-menu').removeAttr('style');

	mCategoryGrover.on('click', function(e){
		$('ul.menu-content').stop().slideToggle('medium');
		return false;
	});

	$('.sf-menu > li > ul').addClass('menu-mobile clearfix').parent().prepend('<span class="menu-mobile-grover"></span>');

	$(".sf-menu .menu-mobile-grover").on('click touchstart', function(e){
		var catSubUl = $(this).next().next('.menu-mobile');
		if (catSubUl.is(':hidden'))
		{
			catSubUl.slideDown();
			$(this).addClass('active');
		}
		else
		{
			catSubUl.slideUp();
			$(this).removeClass('active');
		}
		return false;
	});

	$('#block_top_menu > ul').append('<li class="mnudel"><a class="scroll" href="http://harfie.com/peru/index.php?fc=module&amp;module=blockwishlist&amp;controller=mywishlist&amp;id_lang=6">WISH LIST  </a></li>');
	$('#block_top_menu > ul').append('<li class="mnudel"><a class="scroll" href="http://desarrollo.harfie.com/peru/index.php?controller=my-account" rel="nofollow" title="Login to your customer account">										Sign in										</a></li>');
	$('#block_top_menu > ul').prepend('<li style=" 	height: 33px;" class="mnudel"><div class="sf-search noBack" style="margin-top: 12px;width:100%"><form id="searchbox" action="http://desarrollo.harfie.com/peru/index.php?controller=search" style="float: initial"  method="get"><p><input type="hidden" name="controller" value="search"><input type="hidden" value="position" name="orderby"><input type="hidden" value="desc" name="orderway"><input type="text" name="search_query" placeholder="Buscar" value="" style="border: 1px solid;" ></p><span><button class="btn btn-outline-inverse" name="submit_search" type="submit"><span class="button-search fa fa-search"></span></button></span></form></div></li>');
	$('#divcarrritotest2').addClass('hide');
	$('.cartminionlye').show();
	$('.buscarbarrasear').hide();
	

	
	$('#btncarshowmini').on('click touchstart', function(e){
		if($('#divcarrritotest2').hasClass('hide'))		
		{
			$('#divcarrritotest2').hide();
			$('#divcarrritotest2').removeClass('hide')	
			$('#divcarrritotest2').addClass('show')	
		}
		else
		{
			$('#divcarrritotest2').show();
			$('#divcarrritotest2').removeClass('show')	
			$('#divcarrritotest2').addClass('hide')	
		}
	});
	$('#block_top_menu > ul:first > li > a').on('click touchstart', function(e){
		var parentOffset = $(this).prev().offset(); 
	   	var relX = parentOffset.left - e.pageX;
		if ($(this).parent('li').find('ul').length && relX >= 0 && relX <= 20)
		{
			e.preventDefault();
			var mobCatSubUl = $(this).next('.menu-mobile');
			var mobMenuGrover = $(this).prev();
			if (mobCatSubUl.is(':hidden'))
			{
				mobCatSubUl.slideDown();
				mobMenuGrover.addClass('active');
			}
			else
			{
				mobCatSubUl.slideUp();
				mobMenuGrover.removeClass('active');
			}
		}
	});
	
}

// change the menu display at different resolutions
function menuChange(status)
{
	status == 'enable' ? mobileInit(): desktopInit();
}