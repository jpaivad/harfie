{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='Shipping' mod='cashondelivery'}{/capture}

<h1 class="page-heading">{l s='Order summation' mod='cashondelivery'}</h1>

{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}

<form action="{$link->getModuleLink('cashondelivery', 'validation', [], true)|escape:'html'}" method="post">
	<div class="box aua aua...">
        <input type="hidden" name="confirm" value="1" />
        <h3 class="page-subheading">{l s='Cash on delivery (COD) payment' mod='cashondelivery'}</h3>
        <p>
            - {l s='You have chosen the cash on delivery method.' mod='cashondelivery'}
            <br/>
            - {l s='The total amount of your order is' mod='cashondelivery'}
            <span id="amount_{$currencies.0.id_currency}" class="price">{convertPrice price=$total}</span>
            {if $use_taxes == 1}
                {l s='(tax incl.)' mod='cashondelivery'}
            {/if}
        </p>
        <p>
            <b>{l s='Please confirm your order by clicking \'I confirm my order\'' mod='cashondelivery'}.</b>
        </p>        
    </div>






</form>

<SCRIPT>
<!--
function ActualizaPrecio(campo) { 
	var precioUnidad= 0.95;
	var cantidad= campo.value;
	var precioTotal= precioUnidad*cantidad;
	var numero = new Number(precioTotal);
	precioTotal = numero.toFixed(2);
	
	var precio= "Precio: S/. " + precioTotal;
	document.getElementById("precio").innerHTML=precio;
	document.frmPago.montoTotal.value= precioTotal
}
function ValidaForm(){

		return true;
	
}
//--> 
</SCRIPT>
	<form name="frmPago" method="post" action="{$link->getModuleLink('cashondelivery', 'validation', [], true)|escape:'html'}" onSubmit="return ValidaForm();">
					
						<table width="600" align="center" border="0">
					
							<tr>
								<td colspan="3">
									<div align="left">
										<span class="texto">Nombre:</span>
										<input name="nombre" type="text" size="26" maxlength="100" value="Nombre del Cliente">
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<div align="left">
										<span class="texto">Apellido:</span>
										<input name="apellido" type="text" size="26" maxlength="100" value="Apellido del Cliente">
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<div align="left">
										<span class="texto">Ciudad:</span>
										<input name="ciudad" type="text" size="26" maxlength="100" value="Ciudad">
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<div align="left">
										<span class="texto">Direccion:</span>
										<input name="direccion" type="text" size="26" maxlength="100" value="Direccion del Cliente">
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<div align="left">
										<span class="texto">Correo:</span>
										<input name="correo" type="text" size="26" maxlength="100" value="Correo del Cliente">
									</div>
								</td>
							</tr>
							<tr>
								<td valign="top">&nbsp;</td>
								<td>
									 
								</td>
								<td valign="top">&nbsp;</td>
							</tr>
							
						</table>
						<input name="montoTotal" type="hidden" size="10" maxlength="9" value="{convertPrice price=$total}">



    <p class="cart_navigation" id="cart_navigation">
        <a href="{$link->getPageLink('order', true)}?step=3" class="button-exclusive btn btn-outline">{l s='Other payment methods' mod='cashondelivery'}</a>
<input name="btPagar" type="image" src="images/go.gif" class="Estilo2" value="Pagar"></td>
    </p>

					</form>
