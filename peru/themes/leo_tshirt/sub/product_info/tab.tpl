<script type="text/javascript">
$(document).ready(function(){
		$('.more_info_block .page-product-heading li:first, .more_info_block .tab-content section:first').addClass('active');
	});
</script>
<ul class="nav nav-tabs tab-info page-product-heading">
			<li class="active"><a href="#tab5" data-toggle="tab">Descripción</a></li>
			{if (isset($quantity_discounts) && count($quantity_discounts) > 0)}
				<li><a href="#quantityDiscount" data-toggle="tab">{l s='Volume discounts'}</a></li>
			{/if}
			{if $product->description}
				<li ><a href="#tab2" data-toggle="tab">{l s='More info'}</a></li>
			{/if}
			{if isset($features) && $features}
				<li><a href="#tab3" data-toggle="tab">Detalles</a></li>
			{/if}
                        {if isset($HOOK_PRODUCT_TAB_CONTENT) && $HOOK_PRODUCT_TAB_CONTENT}
				{$HOOK_PRODUCT_TAB}
			{/if}
			{if isset($accessories) && $accessories}
				<li><a href="#tab4" data-toggle="tab">{l s='Accessories'}</a></li>
			{/if}			
                        {if (isset($product) && $product->description) || (isset($features) && $features) || (isset($accessories) && $accessories) || (isset($HOOK_PRODUCT_TAB) && $HOOK_PRODUCT_TAB) || (isset($attachments) && $attachments) || isset($product) && $product->customizable}
                            {if isset($attachments) && $attachments}
                                <li ><a href="#tab5" data-toggle="tab">{l s='Download'}</a></li>
                            {/if}
                            {if isset($product) && $product->customizable}
                                    <li ><a href="#tab6" data-toggle="tab">{l s='Product customization'}</a></li>
                            {/if}
                        {/if}
                        {if isset($packItems) && $packItems|@count > 0}
                            <li ><a href="#blockpack" data-toggle="tab">{l s='Pack'}</a></li>
                        {/if}
		</ul>
		<div class="tab-content">

			<section id="tab5" class="tab-pane page-product-box active">
				
				


			{if $product->online_only}
				<p class="online_only">{l s='Online only'}</p>
			{/if}
	
			<h1 itemprop="name">{$product->name|escape:'html':'UTF-8'}</h1>
			
			<p id="product_reference"{if empty($product->reference) || !$product->reference} style="display: none;"{/if}>
				<label>{l s='Model'} </label>
				<span class="editable" itemprop="sku">{if !isset($groups)}{$product->reference|escape:'html':'UTF-8'}{/if}</span>
			</p>
			
			<!-- pb-right-column-->
		<div class="pb-right-column col-xs-12 col-sm-7 col-md-7">
			{if ($product->show_price && !isset($restricted_country_mode)) || isset($groups) || $product->reference || (isset($HOOK_PRODUCT_ACTIONS) && $HOOK_PRODUCT_ACTIONS)}
			<!-- add to cart form-->
			<form id="buy_block"{if $PS_CATALOG_MODE && !isset($groups) && $product->quantity > 0} class="hidden"{/if} action="{$link->getPageLink('cart')|escape:'html':'UTF-8'}" method="post">
				<!-- hidden datas -->
				<p class="hidden">
					<input type="hidden" name="token" value="{$static_token}" />
					<input type="hidden" name="id_product" value="{$product->id|intval}" id="product_page_product_id" />
					<input type="hidden" name="add" value="1" />
					<input type="hidden" name="id_product_attribute" id="idCombination" value="" />
				</p>
				<div class="box-info-product">
					<div class="content_prices clearfix">
						{if $product->show_price && !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
							<!-- prices -->
							<div class="price">
								<p class="our_price_display" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
									{if $product->quantity > 0}<link itemprop="availability" href="http://schema.org/InStock"/>{/if}
									{if $priceDisplay >= 0 && $priceDisplay <= 2}
										<span id="our_price_display" itemprop="price">{convertPrice price=$productPrice}</span>
										<!--{if $tax_enabled  && ((isset($display_tax_label) && $display_tax_label == 1) || !isset($display_tax_label))}
											{if $priceDisplay == 1}{l s='tax excl.'}{else}{l s='tax incl.'}{/if}
										{/if}-->
										<meta itemprop="priceCurrency" content="{$currency->iso_code}" />
										{hook h="displayProductPriceBlock" product=$product type="price"}
									{/if}
								</p>
								<p id="reduction_percent" {if !$product->specificPrice || $product->specificPrice.reduction_type != 'percentage'} style="display:none;"{/if}>
									<span id="reduction_percent_display">
										{if $product->specificPrice && $product->specificPrice.reduction_type == 'percentage'}-{$product->specificPrice.reduction*100}%{/if}
									</span>
								</p>
								<p id="old_price"{if (!$product->specificPrice || !$product->specificPrice.reduction) && $group_reduction == 0} class="hidden"{/if}>
									{if $priceDisplay >= 0 && $priceDisplay <= 2}
										{hook h="displayProductPriceBlock" product=$product type="old_price"}
										<span id="old_price_display">{if $productPriceWithoutReduction > $productPrice}{convertPrice price=$productPriceWithoutReduction}{/if}</span>
										<!-- {if $tax_enabled && $display_tax_label == 1}{if $priceDisplay == 1}{l s='tax excl.'}{else}{l s='tax incl.'}{/if}{/if} -->
									{/if}
								</p>
								{if $priceDisplay == 2}
									<br />
									<span id="pretaxe_price">
										<span id="pretaxe_price_display">{convertPrice price=$product->getPrice(false, $smarty.const.NULL)}</span>
										{l s='tax excl.'}
									</span>
								{/if}
							</div> <!-- end prices -->
							<p id="reduction_amount" {if !$product->specificPrice || $product->specificPrice.reduction_type != 'amount' || $product->specificPrice.reduction|floatval ==0} style="display:none"{/if}>
								<span id="reduction_amount_display">
								{if $product->specificPrice && $product->specificPrice.reduction_type == 'amount' && $product->specificPrice.reduction|intval !=0}
									-{convertPrice price=$productPriceWithoutReduction-$productPrice|floatval}
								{/if}
								</span>
							</p>
							{if $packItems|@count && $productPrice < $product->getNoPackPrice()}
								<p class="pack_price">{l s='Instead of'} <span style="text-decoration: line-through;">{convertPrice price=$product->getNoPackPrice()}</span></p>
							{/if}
							{if $product->ecotax != 0}
								<p class="price-ecotax">{l s='Including'} <span id="ecotax_price_display">{if $priceDisplay == 2}{$ecotax_tax_exc|convertAndFormatPrice}{else}{$ecotax_tax_inc|convertAndFormatPrice}{/if}</span> {l s='for ecotax'}
									{if $product->specificPrice && $product->specificPrice.reduction}
									<br />{l s='(not impacted by the discount)'}
									{/if}
								</p>
							{/if}
							{if !empty($product->unity) && $product->unit_price_ratio > 0.000000}
								{math equation="pprice / punit_price"  pprice=$productPrice  punit_price=$product->unit_price_ratio assign=unit_price}
								<p class="unit-price"><span id="unit_price_display">{convertPrice price=$unit_price}</span> {l s='per'} {$product->unity|escape:'html':'UTF-8'}</p>
								{hook h="displayProductPriceBlock" product=$product type="unit_price"}
							{/if}
						{/if} {*close if for show price*}
						{hook h="displayProductPriceBlock" product=$product type="weight"}
						<div class="clear"></div>
					</div> <!-- end content_prices -->
					<div class="product_attributes clearfix">
												{if isset($groups)}
							<!-- attributes -->
							<div id="attributes">
								<div class="clearfix"></div>
								{foreach from=$groups key=id_attribute_group item=group}
									{if $group.attributes|@count}
										<fieldset class="attribute_fieldset">
											<label class="attribute_label" {if $group.group_type != 'color' && $group.group_type != 'radio'}for="group_{$id_attribute_group|intval}"{/if}>{$group.name|escape:'html':'UTF-8'}&nbsp;</label>
											{assign var="groupName" value="group_$id_attribute_group"}
											<div class="attribute_list">
												{if ($group.group_type == 'select')}
													<select class="form-control attribute_select no-print" name="{$groupName}" id="group_{$id_attribute_group|intval}">
														{foreach from=$group.attributes key=id_attribute item=group_attribute}
															<option value="{$id_attribute|intval}"{if (isset($smarty.get.$groupName) && $smarty.get.$groupName|intval == $id_attribute) || $group.default == $id_attribute} selected="selected"{/if} title="{$group_attribute|escape:'html':'UTF-8'}">{$group_attribute|escape:'html':'UTF-8'}</option>
														{/foreach}
													</select>
												{elseif ($group.group_type == 'color')}
													<ul id="color_to_pick_list" class="clearfix">
														{assign var="default_colorpicker" value=""}
														{foreach from=$group.attributes key=id_attribute item=group_attribute}
															{assign var='img_color_exists' value=file_exists($col_img_dir|cat:$id_attribute|cat:'.jpg')}
															<li{if $group.default == $id_attribute} class="selected"{/if}>
																<a href="{$link->getProductLink($product)|escape:'html':'UTF-8'}" id="color_{$id_attribute|intval}" name="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" class="color_pick{if ($group.default == $id_attribute)} selected{/if}"{if !$img_color_exists && isset($colors.$id_attribute.value) && $colors.$id_attribute.value} style="background:{$colors.$id_attribute.value|escape:'html':'UTF-8'};"{/if} title="{$colors.$id_attribute.name|escape:'html':'UTF-8'}">
																	{if $img_color_exists}
																		<img src="{$img_col_dir}{$id_attribute|intval}.jpg" alt="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" title="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" width="20" height="20" />
																	{/if}
																</a>
															</li>
															{if ($group.default == $id_attribute)}
																{$default_colorpicker = $id_attribute}
															{/if}
														{/foreach}
													</ul>
													<input type="hidden" class="color_pick_hidden" name="{$groupName|escape:'html':'UTF-8'}" value="{$default_colorpicker|intval}" />
												{elseif ($group.group_type == 'radio')}
													<ul>
														{foreach from=$group.attributes key=id_attribute item=group_attribute}
															<li>
																<input type="radio" class="attribute_radio" name="{$groupName|escape:'html':'UTF-8'}" value="{$id_attribute}" {if ($group.default == $id_attribute)} checked="checked"{/if} />
																<span>{$group_attribute|escape:'html':'UTF-8'}</span>
															</li>
														{/foreach}
													</ul>
												{/if}
											</div> <!-- end attribute_list -->
										</fieldset>
									{/if}
								{/foreach}
							</div> <!-- end attributes -->
						{/if}
						<a href="#myModalTallas" id='Sizecharcart'>Cuadro de Tallas</a><br><br>
						<label id='idcanttitile'>Cant</label>

						{if !$PS_CATALOG_MODE}
						<p id="quantity_wanted_p"{if (!$allow_oosp && $product->quantity <= 0) || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}>
							<input type="text" name="qty" id="quantity_wanted" class="text form-control" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}{if $product->minimal_quantity > 1}{$product->minimal_quantity}{else}1{/if}{/if}" />
							<a href="#" data-field-qty="qty" class="btn btn-outline-inverse status-enable button-minus btn-sm product_quantity_down">
								<span><i class="fa fa-minus"></i></span>
							</a>
							<a href="#" data-field-qty="qty" class="btn btn-outline-inverse status-enable button-plus btn-sm product_quantity_up ">
								<span><i class="fa fa-plus"></i></span>
							</a>
							<span class="clearfix"></span>
						</p>
						{/if}
						<!-- minimal quantity wanted -->
						<p id="minimal_quantity_wanted_p"{if $product->minimal_quantity <= 1 || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}>
							{l s='This product is not sold individually. You must select at least'} <b id="minimal_quantity_label">{$product->minimal_quantity}</b> {l s='quantity for this product.'}
						</p>
						<div class="box-cart-bottom">
							

							<div {if (!$allow_oosp && $product->quantity <= 0) || !$product->available_for_order || (isset($restricted_country_mode) && $restricted_country_mode) || $PS_CATALOG_MODE} class="unvisible"{/if}>
								
								<!-- quantity wanted -->
								<p id="add_to_cart" class="buttons_bottom_block no-print">
									<button type="submit" name="Submit" class="exclusive btn btn-outline-inverse status-enable">
										 
										<span>{if $content_only && (isset($product->customization_required) && $product->customization_required)}{l s='Customize'}{else}{l s='Add to cart'}{/if}</span>
									</button>
								</p>

							</div>
						</div> <!-- end box-cart-bottom -->

					</div> <!-- end product_attributes -->

				<!-- ACa es -->
					<div class="box-cart-bottom">
						{if isset($HOOK_PRODUCT_ACTIONS) && $HOOK_PRODUCT_ACTIONS}{$HOOK_PRODUCT_ACTIONS}{/if}<strong></strong>
					</div> <!-- end box-cart-bottom -->

					{if !$content_only}
						{if $product->id|intval<220}
							<a href="http://harfie.com/peru/home/custom.php?idProduct={$product->id|intval}" id="Customizedit">
								<img src="/peru/home/images/cuadros.png" alt="" class="img-responsive" style="display: inline-block;vertical-align: baseline;margin-right:2px">
							 Perzonalizar</a>
						{/if}
					 {/if}  
					{if isset($HOOK_EXTRA_RIGHT) && $HOOK_EXTRA_RIGHT}{$HOOK_EXTRA_RIGHT}{/if}
				</div> <!-- end box-info-product -->
			</form>
			{/if}
		</div> <!-- end pb-right-column-->
			
			
	
			<p id="availability_date"{if ($product->quantity > 0) || !$product->available_for_order || $PS_CATALOG_MODE || !isset($product->available_date) || $product->available_date < $smarty.now|date_format:'%Y-%m-%d'} style="display: none;"{/if}>
				<span id="availability_date_label">{l s='Availability date:'}</span>
				<span id="availability_date_value">{dateFormat date=$product->available_date full=false}</span>
			</p>
			<!-- Out of stock hook -->
			<div id="oosHook"{if $product->quantity > 0} style="display: none;"{/if}>
				{$HOOK_PRODUCT_OOS}
			</div>

			</section>


			{if (isset($quantity_discounts) && count($quantity_discounts) > 0)}
			<!-- quantity discount -->
			<section id="tabquantityDiscount" class="tab-pane page-product-box">
				
				<div id="quantityDiscount" class="tab-pane">
					<table class="std table-product-discounts">
						<thead>
							<tr>
								<th>{l s='Quantity'}</th>
								<th>{if $display_discount_price}{l s='Price'}{else}{l s='Discount'}{/if}</th>
								<th>{l s='You Save'}</th>
							</tr>
						</thead>
						<tbody>
							{foreach from=$quantity_discounts item='quantity_discount' name='quantity_discounts'}
							<tr id="quantityDiscount_{$quantity_discount.id_product_attribute}" class="quantityDiscount_{$quantity_discount.id_product_attribute}" data-discount-type="{$quantity_discount.reduction_type}" data-discount="{$quantity_discount.real_value|floatval}" data-discount-quantity="{$quantity_discount.quantity|intval}">
								<td>
									{$quantity_discount.quantity|intval}
								</td>
								<td>
									{if $quantity_discount.price >= 0 || $quantity_discount.reduction_type == 'amount'}
										{if $display_discount_price}
											{convertPrice price=$productPrice-$quantity_discount.real_value|floatval}
										{else}
											{convertPrice price=$quantity_discount.real_value|floatval}
										{/if}
									{else}
										{if $display_discount_price}
											{convertPrice price = $productPrice-($productPrice*$quantity_discount.reduction)|floatval}
										{else}
											{$quantity_discount.real_value|floatval}%
										{/if}
									{/if}
								</td>
								<td>
									<span>{l s='Up to'}</span>
									{if $quantity_discount.price >= 0 || $quantity_discount.reduction_type == 'amount'}
										{$discountPrice=$productPrice-$quantity_discount.real_value|floatval}
									{else}
										{$discountPrice=$productPrice-($productPrice*$quantity_discount.reduction)|floatval}
									{/if}
									{$discountPrice=$discountPrice*$quantity_discount.quantity}
									{$qtyProductPrice = $productPrice*$quantity_discount.quantity}
									{convertPrice price=$qtyProductPrice-$discountPrice}
								</td>
							</tr>
							{/foreach}
						</tbody>
					</table>
				</div>
			</section>
		{/if}
		
		{if $product->description}
			<!-- More info -->
			<section id="tab2" class="tab-pane page-product-box ">
				
				{if isset($product) && $product->description}
					<!-- full description -->
					<div  class="rte">{$product->description}</div>
				{/if}
			</section>
			<!--end  More info -->
		{/if}
		{if isset($features) && $features}
			<!-- Data sheet -->
			<section id="tab3" class="tab-pane page-product-box">
				
				<table class="table-data-sheet">			
					{foreach from=$features item=feature}
					<tr class="{cycle values="odd,even"}">
						{if isset($feature.value)}			    
						<td>{$feature.name|escape:'html':'UTF-8'}</td>
						<td>{$feature.value|escape:'html':'UTF-8'}</td>
						{/if}
					</tr>
					{/foreach}
				</table>
			</section>
			<!--end Data sheet -->
		{/if}
		<!--HOOK_PRODUCT_TAB -->		
                    {if isset($HOOK_PRODUCT_TAB_CONTENT) && $HOOK_PRODUCT_TAB_CONTENT}{$HOOK_PRODUCT_TAB_CONTENT}{/if}
		<!--end HOOK_PRODUCT_TAB -->
		{if isset($accessories) && $accessories}
			<!--Accessories -->
			<section id="tab4" class="tab-pane page-product-box">

				<div class="block products_block accessories-block clearfix">
					<div class="block_content">
						<div class="product_list grid row">
							{foreach from=$accessories item=accessory name=accessories_list}
								{if ($accessory.allow_oosp || $accessory.quantity_all_versions > 0 || $accessory.quantity > 0) && $accessory.available_for_order && !isset($restricted_country_mode)}
									{assign var='accessoryLink' value=$link->getProductLink($accessory.id_product, $accessory.link_rewrite, $accessory.category)}
									<div class="product_block col-xs-6 col-sm-6 col-md-4 col-lg-4 item ajax_block_product{if $smarty.foreach.accessories_list.first} first_item{elseif $smarty.foreach.accessories_list.last} last_item{else} item{/if} product_accessories_description">
										<div class="product-container text-center product-block" itemscope itemtype="http://schema.org/Product">
											<div class="left-block">
												<div class="product-image-container image">
												   <div class="leo-more-info" data-idproduct="{$accessory.id_product}"></div>
													<a class="product_img_link"	href="{$accessory.link|escape:'html':'UTF-8'}" title="{$accessory.name|escape:'html':'UTF-8'}" itemprop="url">
														<img class="replace-2x img-responsive" src="{$link->getImageLink($accessory.link_rewrite, $accessory.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{if !empty($accessory.legend)}{$accessory.legend|escape:'html':'UTF-8'}{else}{$accessory.name|escape:'html':'UTF-8'}{/if}" title="{if !empty($accessory.legend)}{$accessory.legend|escape:'html':'UTF-8'}{else}{$accessory.name|escape:'html':'UTF-8'}{/if}" itemprop="image" />
														<span class="product-additional" data-idproduct="{$accessory.id_product}"></span>
													</a>
													{if isset($quick_view) && $quick_view}
														<a class="quick-view btn-outline-inverse btn" href="{$accessory.link|escape:'html':'UTF-8'}" rel="{$accessory.link|escape:'html':'UTF-8'}" title="{l s='Quick view'}" >
															<i class="fa fa-eye"></i>
															{l s='Quick view'}
														</a>
													{/if}
													{if isset($accessory.new) && $accessory.new == 1}
														<span class="new-box">
															<span class="new-label product-label">{l s='New'}</span>
														</span>
													{/if}
													{if isset($accessory.on_sale) && $accessory.on_sale && isset($accessory.show_price) && $accessory.show_price && !$PS_CATALOG_MODE}
														<span class="sale-box">
															<span class="sale-label product-label">{l s='Sale!'}</span>
														</span>
													{/if}
												</div>		
											</div>
											<!-- End of left-block -->

											<div class="right-block">
												<div class="product-meta">
													{hook h='displayProductListReviews' product=$accessory}
													<h5 itemprop="name" class="name">
														{if isset($accessory.pack_quantity) && $accessory.pack_quantity}{$accessory.pack_quantity|intval|cat:' x '}{/if}
														<a class="product-name" href="{$accessory.link|escape:'html':'UTF-8'}" title="{$accessory.name|escape:'html':'UTF-8'}" itemprop="url" >
															{$accessory.name|truncate:45:'...'|escape:'html':'UTF-8'}
														</a>
													</h5>
													<p class="product-desc" itemprop="description">
														{$accessory.description_short|strip_tags:'UTF-8'|truncate:360:'...'}
													</p>
													{if (!$PS_CATALOG_MODE AND ((isset($accessory.show_price) && $accessory.show_price) || (isset($accessory.available_for_order) && $accessory.available_for_order)))}
														<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
															{if isset($accessory.show_price) && $accessory.show_price && !isset($restricted_country_mode)}
																<span itemprop="price" class="price product-price">
																	{if !$priceDisplay}{convertPrice price=$accessory.price}{else}{convertPrice price=$accessory.price_tax_exc}{/if}
																</span>
																<meta itemprop="priceCurrency" content="{$currency->iso_code}" />
																{if isset($accessory.specific_prices) && $accessory.specific_prices && isset($accessory.specific_prices.reduction) && $accessory.specific_prices.reduction > 0}
																	{hook h="displayProductPriceBlock" product=$accessory type="old_price"}
																	<span class="old-price product-price">
																		{displayWtPrice p=$accessory.price_without_reduction}
																	</span>
																	{if $accessory.specific_prices.reduction_type == 'percentage'}
																		<span class="price-percent-reduction">-{$accessory.specific_prices.reduction * 100}%</span>
																	{/if}
																{/if}
																{hook h="displayProductPriceBlock" product=$accessory type="price"}
																{hook h="displayProductPriceBlock" product=$accessory type="unit_price"}
															{/if}
														</div>
													{/if}
													{if isset($accessory.color_list) && $ENABLE_COLOR}
														<div class="color-list-container">{$accessory.color_list} </div>
													{/if}
													<div class="product-flags">
														{if (!$PS_CATALOG_MODE AND ((isset($accessory.show_price) && $accessory.show_price) || (isset($accessory.available_for_order) && $accessory.available_for_order)))}
															{if isset($accessory.online_only) && $accessory.online_only}
																<span class="online_only label label-warning">{l s='Online only'}</span>
															{/if}
														{/if}
														{if isset($accessory.on_sale) && $accessory.on_sale && isset($accessory.show_price) && $accessory.show_price && !$PS_CATALOG_MODE}
															{elseif isset($accessory.reduction) && $accessory.reduction && isset($accessory.show_price) && $accessory.show_price && !$PS_CATALOG_MODE}
																<span class="discount label label-danger">{l s='Reduced price!'}</span>
															{/if}
													</div>
												

													{if (!$PS_CATALOG_MODE && $PS_STOCK_MANAGEMENT && ((isset($accessory.show_price) && $accessory.show_price) || (isset($accessory.available_for_order) && $accessory.available_for_order)))}
														{if isset($accessory.available_for_order) && $accessory.available_for_order && !isset($restricted_country_mode)}
															<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="availability">
																{if ($accessory.allow_oosp || $accessory.quantity > 0)}
																	<span class="{if $accessory.quantity <= 0 && !$accessory.allow_oosp}out-of-stock{else}available-now{/if}">
																		<link itemprop="availability" href="http://schema.org/InStock" />{if $accessory.quantity <= 0}{if $accessory.allow_oosp}{if isset($accessory.available_later) && $accessory.available_later}{$accessory.available_later}{else}{l s='In Stock'}{/if}{else}{l s='Out of stock'}{/if}{else}{if isset($accessory.available_now) && $accessory.available_now}{$accessory.available_now}{else}{l s='In Stock'}{/if}{/if}
																	</span>
																{elseif (isset($accessory.quantity_all_versions) && $accessory.quantity_all_versions > 0)}
																	<span class="available-dif">
																		<link itemprop="availability" href="http://schema.org/LimitedAvailability" />{l s='Product available with different options'}
																	</span>
																{else}
																	<span class="out-of-stock">
																		<link itemprop="availability" href="http://schema.org/OutOfStock" />{l s='Out of stock'}
																	</span>
																{/if}
															</div>
														{/if}
													{/if}
												
													{if $page_name ='product'}
														<div class="functional-buttons clearfix">			
															
															
															<div class="wishlist">
																{hook h='displayProductListFunctionalButtons' product=$accessory}
															</div>
															
															<div class="cart">
																{if ($accessory.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $accessory.available_for_order && !isset($restricted_country_mode) && $accessory.minimal_quantity <= 1 && $accessory.customizable != 2 && !$PS_CATALOG_MODE}
																	{if (!isset($accessory.customization_required) || !$accessory.customization_required) && ($accessory.allow_oosp || $accessory.quantity > 0)}
																		{if isset($static_token)}
																			<a class="button ajax_add_to_cart_button btn btn-outline-inverse" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$accessory.id_product|intval}&amp;token={$static_token}", false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart'}" data-id-product="{$accessory.id_product|intval}">
																				<i class="fa fa-shopping-cart"></i>
																				<span>{l s='Add to cart'}</span>
																			</a>
																		{else}
																			<a class="button ajax_add_to_cart_button btn btn-outline-inverse" href="{$link->getPageLink('cart',false, NULL, 'add=1&amp;id_product={$accessory.id_product|intval}', false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart'}" data-id-product="{$accessory.id_product|intval}">
																				<i class="fa fa-shopping-cart"></i>
																				<span>{l s='Add to cart'}</span>
																			</a>
																		{/if}
																	{else}
																		<div class="ajax_add_to_cart_button btn disabled btn-outline-inverse" title="{l s='Out of stock'}" >
																			<i class="fa fa-shopping-cart"></i>
																			<span>{l s='Out of stock'}</span>
																		</div>
																	{/if}
																{/if}
															</div>
															{if isset($comparator_max_item) && $comparator_max_item}
															<div class="compare">
																<a class="add_to_compare compare btn btn-outline-inverse" href="{$accessory.link|escape:'html':'UTF-8'}" data-id-product="{$accessory.id_product}" title="{l s='Add to compare'}" >
																	<i class="fa fa-files-o"></i>
																	<span>{l s='Add to compare'}</span>
																</a>						
															</div>
																
															{/if}
														</div>
													{/if}
												</div>
											</div>
											<!-- End right-block -->
											
										</div>
									</div>
								{/if}
							{/foreach}
						</div>
					</div>
				</div>	
			</section>
			<!--end Accessories -->
		{/if}
		
		<!-- description & features -->
		{if (isset($product) && $product->description) || (isset($features) && $features) || (isset($accessories) && $accessories) || (isset($HOOK_PRODUCT_TAB) && $HOOK_PRODUCT_TAB) || (isset($attachments) && $attachments) || isset($product) && $product->customizable}
			{if isset($attachments) && $attachments}
			<!--Download -->
			<section id="tab5" class="tab-pane page-product-box">
				
				{foreach from=$attachments item=attachment name=attachements}
					{if $smarty.foreach.attachements.iteration %3 == 1}<div class="row">{/if}
						<div class="col-lg-4 col-md-4 col-xs-12">
							<h4><a href="{$link->getPageLink('attachment', true, NULL, "id_attachment={$attachment.id_attachment}")|escape:'html':'UTF-8'}">{$attachment.name|escape:'html':'UTF-8'}</a></h4>
							<p class="text-muted">{$attachment.description|escape:'html':'UTF-8'}</p>
							<a class="btn btn-default btn-block" href="{$link->getPageLink('attachment', true, NULL, "id_attachment={$attachment.id_attachment}")|escape:'html':'UTF-8'}">
								<i class="icon-download"></i>
								{l s="Download"} ({Tools::formatBytes($attachment.file_size, 2)})
							</a>
							<hr>
						</div>
					{if $smarty.foreach.attachements.iteration %3 == 0 || $smarty.foreach.attachements.last}</div>{/if}
				{/foreach}
			</section>
			<!--end Download -->
			{/if}
			{if isset($product) && $product->customizable}
			<!--Customization -->
			<section id="tab6" class="tab-pane page-product-box">
				
				<!-- Customizable products -->
				<form method="post" action="{$customizationFormTarget}" enctype="multipart/form-data" id="customizationForm" class="clearfix">
					<p class="infoCustomizable">
						{l s='After saving your customized product, remember to add it to your cart.'}
						{if $product->uploadable_files}
						<br />
						{l s='Allowed file formats are: GIF, JPG, PNG'}{/if}
					</p>
					{if $product->uploadable_files|intval}
						<div class="customizableProductsFile">
							<h5 class="product-heading-h5">{l s='Pictures'}</h5>
							<ul id="uploadable_files" class="clearfix">
								{counter start=0 assign='customizationField'}
								{foreach from=$customizationFields item='field' name='customizationFields'}
									{if $field.type == 0}
										<li class="customizationUploadLine{if $field.required} required{/if}">{assign var='key' value='pictures_'|cat:$product->id|cat:'_'|cat:$field.id_customization_field}
											{if isset($pictures.$key)}
												<div class="customizationUploadBrowse">
													<img src="{$pic_dir}{$pictures.$key}_small" alt="" />
														<a href="{$link->getProductDeletePictureLink($product, $field.id_customization_field)|escape:'html':'UTF-8'}" title="{l s='Delete'}" >
															<img src="{$img_dir}icon/delete.gif" alt="{l s='Delete'}" class="customization_delete_icon" width="11" height="13" />
														</a>
												</div>
											{/if}
											<div class="customizationUploadBrowse form-group">
												<label class="customizationUploadBrowseDescription">
													{if !empty($field.name)}
														{$field.name}
													{else}
														{l s='Please select an image file from your computer'}
													{/if}
													{if $field.required}<sup>*</sup>{/if}
												</label>
												<input type="file" name="file{$field.id_customization_field}" id="img{$customizationField}" class="form-control customization_block_input {if isset($pictures.$key)}filled{/if}" />
											</div>
										</li>
										{counter}
									{/if}
								{/foreach}
							</ul>
						</div>
					{/if}
					{if $product->text_fields|intval}
						<div class="customizableProductsText">
							<h5 class="product-heading-h5">{l s='Text'}</h5>
							<ul id="text_fields">
							{counter start=0 assign='customizationField'}
							{foreach from=$customizationFields item='field' name='customizationFields'}
								{if $field.type == 1}
									<li class="customizationUploadLine{if $field.required} required{/if}">
										<label for ="textField{$customizationField}">
											{assign var='key' value='textFields_'|cat:$product->id|cat:'_'|cat:$field.id_customization_field}
											{if !empty($field.name)}
												{$field.name}
											{/if}
											{if $field.required}<sup>*</sup>{/if}
										</label>
										<textarea name="textField{$field.id_customization_field}" class="form-control customization_block_input" id="textField{$customizationField}" rows="3" cols="20">{strip}
											{if isset($textFields.$key)}
												{$textFields.$key|stripslashes}
											{/if}
										{/strip}</textarea>
									</li>
									{counter}
								{/if}
							{/foreach}
							</ul>
						</div>
					{/if}
					<p id="customizedDatas">
						<input type="hidden" name="quantityBackup" id="quantityBackup" value="" />
						<input type="hidden" name="submitCustomizedDatas" value="1" />
						<button class="button btn btn-default button button-small" name="saveCustomization">
							<span>{l s='Save'}</span>
						</button>
						<span id="ajax-loader" class="unvisible">
							<img src="{$img_ps_dir}loader.gif" alt="loader" />
						</span>
					</p>
				</form>
				<p class="clear required"><sup>*</sup> {l s='required fields'}</p>	
			</section>
			<!--end Customization -->
			{/if}
		{/if}
		</div>
		{if isset($packItems) && $packItems|@count > 0}
		<section id="blockpack" class="tab-pane page-product-box">
			<h3 class="page-product-heading">{l s='Pack content'}</h3>
			{include file="$tpl_dir./product-list.tpl" products=$packItems}
		</section>
		{/if}
                {if isset($HOOK_PRODUCT_FOOTER) && $HOOK_PRODUCT_FOOTER}{$HOOK_PRODUCT_FOOTER}{/if}