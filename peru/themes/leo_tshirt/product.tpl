{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{include file="$tpl_dir./errors.tpl"}
{if $errors|@count == 0}
	{if !isset($priceDisplayPrecision)}
		{assign var='priceDisplayPrecision' value=2}
	{/if}
	{if !$priceDisplay || $priceDisplay == 2}
		{assign var='productPrice' value=$product->getPrice(true, $smarty.const.NULL, $priceDisplayPrecision)}
		{assign var='productPriceWithoutReduction' value=$product->getPriceWithoutReduct(false, $smarty.const.NULL)}
	{elseif $priceDisplay == 1}
		{assign var='productPrice' value=$product->getPrice(false, $smarty.const.NULL, $priceDisplayPrecision)}
		{assign var='productPriceWithoutReduction' value=$product->getPriceWithoutReduct(true, $smarty.const.NULL)}
	{/if}
	<script type="text/javascript">
	$( document ).ready(function() {
  		$('#center_column').attr('style','width:80%');
	});
	</script>
	<div class="primary_block row" itemscope itemtype="http://schema.org/Product">
		{if !$content_only}
			<div class="container">
				<div class="top-hr"></div>
			</div>
		{/if}
		{if isset($adminActionDisplay) && $adminActionDisplay}
			<div id="admin-action">
				<p>{l s='This product is not visible to your customers.'}
					<input type="hidden" id="admin-action-product-id" value="{$product->id}" />
					<input type="submit" value="{l s='Publish'}" name="publish_button" class="exclusive" />
					<input type="submit" value="{l s='Back'}" name="lnk_view" class="exclusive" />
				</p>
				<p id="admin-action-result"></p>
			</div>
		{/if}
		{if isset($confirmation) && $confirmation}
			<p class="confirmation">
				{$confirmation}
			</p>
		{/if}
		<!-- left infos-->  
		<div class="pb-left-column col-xs-12 col-sm-12 col-md-5">
			<!-- product img-->        
			<div id="image-block" class="clearfix">
				{if $product->new}
					<span class="new-box">
						<span class="new-label product-label">{l s='New'}</span>
					</span>
				{/if}
				{if $product->on_sale}
					<span class="sale-box no-print">
						<span class="sale-label product-label">{l s='Sale!'}</span>
					</span>
				{elseif $product->specificPrice && $product->specificPrice.reduction && $productPriceWithoutReduction > $productPrice}
					<span class="discount">{l s='Reduced price!'}</span>
				{/if}
				{if $have_image}
					<span id="view_full_size">
						{if $jqZoomEnabled && $have_image && !$content_only}
							<a class="jqzoom" title="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" rel="gal1" href="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'thickbox_default')|escape:'html':'UTF-8'}" itemprop="url">
								<img itemprop="image" src="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')|escape:'html':'UTF-8'}" title="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" alt="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}"/>
							</a>
						{else}
							<img id="bigpic" itemprop="image" src="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')|escape:'html':'UTF-8'}" title="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" alt="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}"/>
							{if !$content_only}
								<span class="span_link no-print status-enable btn btn-outline-inverse" style="display:none"></span>
							{/if}
						{/if}
					</span>
				{else}
					<span id="view_full_size">
						<img itemprop="image" src="{$img_prod_dir}{$lang_iso}-default-large_default.jpg" id="bigpic" alt="" title="{$product->name|escape:'html':'UTF-8'}"/>
						{if !$content_only}
							<span class="span_link">
								{l s='View larger'}
							</span>
						{/if}
					</span>
				{/if}
			</div> <!-- end image-block -->
			{if isset($images) && count($images) > 0}
				<!-- thumbnails -->
				<div id="views_block" class="clearfix {if isset($images) && count($images) < 2}hidden{/if}">
					{if isset($images) && count($images) > 2}
						<span class="view_scroll_spacer">
							<a id="view_scroll_left" class="" title="{l s='Other views'}" href="javascript:{ldelim}{rdelim}">
								{l s='Previous'}
							</a>
						</span>
					{/if}
					<div id="thumbs_list">
						<ul id="thumbs_list_frame">
						{if isset($images)}
							{foreach from=$images item=image name=thumbnails}
								{assign var=imageIds value="`$product->id`-`$image.id_image`"}
								{if !empty($image.legend)}
									{assign var=imageTitle value=$image.legend|escape:'html':'UTF-8'}
								{else}
									{assign var=imageTitle value=$product->name|escape:'html':'UTF-8'}
								{/if}
								<li id="thumbnail_{$image.id_image}"{if $smarty.foreach.thumbnails.last} class="last"{/if}>
									<a {if $jqZoomEnabled && $have_image && !$content_only} href="javascript:void(0);" rel="{literal}{{/literal}gallery: 'gal1', smallimage: '{$link->getImageLink($product->link_rewrite, $imageIds, 'large_default')|escape:'html':'UTF-8'}',largeimage: '{$link->getImageLink($product->link_rewrite, $imageIds, 'thickbox_default')|escape:'html':'UTF-8'}'{literal}}{/literal}"{else} href="{$link->getImageLink($product->link_rewrite, $imageIds, 'thickbox_default')|escape:'html':'UTF-8'}"	data-fancybox-group="other-views" class="fancybox{if $image.id_image == $cover.id_image} shown{/if}"{/if} title="{$imageTitle}">
										<img class="img-responsive" id="thumb_{$image.id_image}" src="{$link->getImageLink($product->link_rewrite, $imageIds, 'cart_default')|escape:'html':'UTF-8'}" alt="{$imageTitle}" title="{$imageTitle}" itemprop="image" />
									</a>
								</li>
							{/foreach}
						{/if}
						</ul>
					</div> <!-- end thumbs_list -->
					{if isset($images) && count($images) > 2}
						<a id="view_scroll_right" title="{l s='Other views'}" href="javascript:{ldelim}{rdelim}">
							{l s='Next'}
						</a>
					{/if}
				</div> <!-- end views-block -->
				<!-- end thumbnails -->
			{/if}
			{if isset($images) && count($images) > 1}
				<p class="resetimg clear no-print">
					<span id="wrapResetImages" style="display: none;">
						<a href="{$link->getProductLink($product)|escape:'html':'UTF-8'}" name="resetImages">
							<i class="fa fa-repeat"></i>
							{l s='Display all pictures'}
						</a>
					</span>
				</p>
			{/if}
		</div> <!-- end pb-left-column -->
		<!-- end left infos--> 
		<!-- center infos -->
		<div class="pb-center-column col-xs-12 col-sm-7 col-md-7">

	
            {if isset($USE_PTABS) && $USE_PTABS}
                {include file="$tpl_dir./sub/product_info/tab.tpl"}
            {else}
                {include file="$tpl_dir./sub/product_info/default.tpl"}
            {/if}
	



			
		</div>
		<!-- end center infos-->
		
	</div> <!-- end primary_block -->



	<div  id="myModalTallas" style="display:none">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" style="font-weight:bold">SIZE CHART</h4>
            </div>
            <div class="modal-body">
                <span style="font-size: 12px;font-weight: bold;">EN CM:</span>
                <br>
                <br>
                <div style="text-align: center;font-weight: bold;">Hombre</div>
                <table  class='table table-striped tablemen' style='font-size: 12px;text-align: center' >
                    <thead>
                        <tr  style='background-color: rgb(89,89,89);color: white;font-weight: bold;'>
                            <th style='text-align: center;'>Size</th>
                            <th style='text-align: center;'>S</th>
                            <th style='text-align: center;'>M</th>
                            <th style='text-align: center;'>L</th>
                            <th style='text-align: center;'>XL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>CHEST</td>
                            <td>96</td>
                            <td>104</td>
                            <td>106</td>
                            <td>110</td>

                        </tr>
                        <tr>
                            <td>WAIST</td>
                            <td>74 - 76</td>
                            <td>79 - 81</td>
                            <td>84 - 86</td>
                            <td>86 - 91</td>

                        </tr>
                        <tr>
                            <td>HIP</td>
                            <td>94 - 96</td>
                            <td>99 - 104</td>
                            <td>104 - 106</td>
                            <td>109 - 110</td>

                        </tr>
                        <tr>
                            <td>LENGTH</td>
                            <td>70</td>
                            <td>72</td>
                            <td>76</td>
                            <td>78</td>

                        </tr>
                    </tbody>
                </table>

                <div style="text-align: center;font-weight: bold;">Mujer</div>
                <table  class='table table-striped tablewoman' style='font-size: 12px;text-align: center' >
                    <thead>
                        <tr  style='background-color: rgb(89,89,89);color: white;font-weight: bold;'>
                            <th style='text-align: center;'>Size</th>
                            <th style='text-align: center;'>XS</th>
                            <th style='text-align: center;'>S</th>
                            <th style='text-align: center;'>M</th>
                            <th style='text-align: center;'>L</th>
                            <th style='text-align: center;'>XL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>BUST</td>
                            <td>76 - 82</td>
                            <td>81 - 86</td>
                            <td>86 - 90</td>
                            <td>91 - 94</td>
                            <td>97 - 98</td>

                        </tr>
                        <tr>
                            <td>WAIST</td>
                            <td>61 - 64</td>
                            <td>66 - 69</td>
                            <td>71 - 74</td>
                            <td>72 - 76</td>
                            <td>74 - 79</td>

                        </tr>
                        <tr>
                            <td>HIP</td>
                            <td>79 - 84</td>
                            <td>84 - 88</td>
                            <td>89 - 92</td>
                            <td>94 - 98</td>
                            <td>102 - 102</td>

                        </tr>
                        <tr>
                            <td>LENGTH</td>
                            <td>58</td>
                            <td>60</td>
                            <td>62</td>
                            <td>66</td>
                            <td>68</td>

                        </tr>
                    </tbody>
                </table>



                <span style="font-size: 12px;font-weight: bold;">EN INCH:</span>
                   <br><br>
                   <div style="text-align: center;font-weight: bold;">Hombre</div>
                <table  class='table table-striped tablemen' style='font-size: 12px;text-align: center' >
                    <thead>
                        <tr  style='background-color: rgb(89,89,89);color: white;font-weight: bold;'>
                            <th style='text-align: center;'>Size</th>
                            <th style='text-align: center;'>S</th>
                            <th style='text-align: center;'>M</th>
                            <th style='text-align: center;'>L</th>
                            <th style='text-align: center;'>XL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>CHEST</td>
                            <td>38</td>
                            <td>41</td>
                            <td>42</td>
                            <td>43</td>

                        </tr>
                        <tr>
                            <td>WAIST</td>
                            <td>29 - 30</td>
                            <td>31 - 32</td>
                            <td>33 - 34</td>
                            <td>34 - 37</td>

                        </tr>
                        <tr>
                            <td>HIP</td>
                            <td>37 - 38</td>
                            <td>39 - 41</td>
                            <td>41 - 42</td>
                            <td>43 - 43</td>

                        </tr>
                        <tr>
                            <td>LENGTH</td>
                            <td>28</td>
                            <td>28</td>
                            <td>30</td>
                            <td>31</td>

                        </tr>
                    </tbody>
                </table>

<div style="text-align: center;font-weight: bold;">Mujer</div>
                <table  class='table table-striped tablewoman' style='font-size: 12px;text-align: center' >
                    <thead>
                        <tr  style='background-color: rgb(89,89,89);color: white;font-weight: bold;'>
                            <th style='text-align: center;'>Size</th>
                            <th style='text-align: center;'>XS</th>
                            <th style='text-align: center;'>S</th>
                            <th style='text-align: center;'>M</th>
                            <th style='text-align: center;'>L</th>
                            <th style='text-align: center;'>XL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>BUST</td>
                            <td>30 - 32</td>
                            <td>32 - 34</td>
                            <td>34 - 35</td>
                            <td>36 - 37</td>
                            <td>38 - 39</td>

                        </tr>
                        <tr>
                            <td>WAIST</td>
                            <td>24 - 25</td>
                            <td>26 - 27</td>
                            <td>28 - 29</td>
                            <td>29 - 30</td>
                            <td>30 - 31</td>

                        </tr>
                        <tr>
                            <td>HIP</td>
                            <td>31-33</td>
                            <td>33-35</td>
                            <td>35-36</td>
                            <td>37-39</td>
                            <td>40-41</td>

                        </tr>
                        <tr>
                            <td>LENGTH</td>
                            <td>23</td>
                            <td>23</td>
                            <td>24</td>
                            <td>26</td>
                            <td>27</td>

                        </tr>
                    </tbody>
                </table>



            </div>
            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script type="text/javascript">
	$(document).ready(function(){
		if ($('h1').html().substring(0, 4)=='Home')
		{
			$('#add_to_cart').attr('style','display:none');
			$('#Customizedit').attr('style','display:none');
			$('#wishlist_button').attr('style','display:none');
			$('#quantity_wanted_p').attr('style','display:none');
			$('#idcanttitile').attr('style','display:none');
			$('h1').html('Prenda Perzonalizada');
		};
		$('.color_pick').click(function(){
			$(this).parent().parent().parent().parent().find('label').html('Color: ' + $(this).attr('title'));
		})

		$('#Sizecharcart').fancybox({
			autoDimensions: false,
			autoSize: false,
			width: 600,
			height: 'auto',
			helpers: {
				overlay: {
					locked: false
				}
			}
		});
	});
</script>





{strip}
{if isset($smarty.get.ad) && $smarty.get.ad}
{addJsDefL name=ad}{$base_dir|cat:$smarty.get.ad|escape:'html':'UTF-8'}{/addJsDefL}
{/if}
{if isset($smarty.get.adtoken) && $smarty.get.adtoken}
{addJsDefL name=adtoken}{$smarty.get.adtoken|escape:'html':'UTF-8'}{/addJsDefL}
{/if}
{addJsDef allowBuyWhenOutOfStock=$allow_oosp|boolval}
{addJsDef availableNowValue=$product->available_now|escape:'quotes':'UTF-8'}
{addJsDef availableLaterValue=$product->available_later|escape:'quotes':'UTF-8'}
{addJsDef attribute_anchor_separator=$attribute_anchor_separator|escape:'quotes':'UTF-8'}
{addJsDef attributesCombinations=$attributesCombinations}
{addJsDef currencySign=$currencySign|html_entity_decode:2:"UTF-8"}
{addJsDef currencyRate=$currencyRate|floatval}
{addJsDef currencyFormat=$currencyFormat|intval}
{addJsDef currencyBlank=$currencyBlank|intval}
{addJsDef currentDate=$smarty.now|date_format:'%Y-%m-%d %H:%M:%S'}
{if isset($combinations) && $combinations}
	{addJsDef combinations=$combinations}
	{addJsDef combinationsFromController=$combinations}
	{addJsDef displayDiscountPrice=$display_discount_price}
	{addJsDefL name='upToTxt'}{l s='Up to' js=1}{/addJsDefL}
{/if}
{if isset($combinationImages) && $combinationImages}
	{addJsDef combinationImages=$combinationImages}
{/if}
{addJsDef customizationFields=$customizationFields}
{addJsDef default_eco_tax=$product->ecotax|floatval}
{addJsDef displayPrice=$priceDisplay|intval}
{addJsDef ecotaxTax_rate=$ecotaxTax_rate|floatval}
{addJsDef group_reduction=$group_reduction}
{if isset($cover.id_image_only)}
	{addJsDef idDefaultImage=$cover.id_image_only|intval}
{else}
	{addJsDef idDefaultImage=0}
{/if}
{addJsDef img_ps_dir=$img_ps_dir}
{addJsDef img_prod_dir=$img_prod_dir}
{addJsDef id_product=$product->id|intval}
{addJsDef jqZoomEnabled=$jqZoomEnabled|boolval}
{addJsDef maxQuantityToAllowDisplayOfLastQuantityMessage=$last_qties|intval}
{addJsDef minimalQuantity=$product->minimal_quantity|intval}
{addJsDef noTaxForThisProduct=$no_tax|boolval}
{addJsDef customerGroupWithoutTax=$customer_group_without_tax|boolval}
{addJsDef oosHookJsCodeFunctions=Array()}
{addJsDef productHasAttributes=isset($groups)|boolval}
{addJsDef productPriceTaxExcluded=($product->getPriceWithoutReduct(true)|default:'null' - $product->ecotax)|floatval}
{addJsDef productBasePriceTaxExcluded=($product->base_price - $product->ecotax)|floatval}
{addJsDef productBasePriceTaxExcl=($product->base_price|floatval)}
{addJsDef productReference=$product->reference|escape:'html':'UTF-8'}
{addJsDef productAvailableForOrder=$product->available_for_order|boolval}
{addJsDef productPriceWithoutReduction=$productPriceWithoutReduction|floatval}
{addJsDef productPrice=$productPrice|floatval}
{addJsDef productUnitPriceRatio=$product->unit_price_ratio|floatval}
{addJsDef productShowPrice=(!$PS_CATALOG_MODE && $product->show_price)|boolval}
{addJsDef PS_CATALOG_MODE=$PS_CATALOG_MODE}
{if $product->specificPrice && $product->specificPrice|@count}
	{addJsDef product_specific_price=$product->specificPrice}
{else}
	{addJsDef product_specific_price=array()}
{/if}
{if $display_qties == 1 && $product->quantity}
	{addJsDef quantityAvailable=$product->quantity}
{else}
	{addJsDef quantityAvailable=0}
{/if}
{addJsDef quantitiesDisplayAllowed=$display_qties|boolval}
{if $product->specificPrice && $product->specificPrice.reduction && $product->specificPrice.reduction_type == 'percentage'}
	{addJsDef reduction_percent=$product->specificPrice.reduction*100|floatval}
{else}
	{addJsDef reduction_percent=0}
{/if}
{if $product->specificPrice && $product->specificPrice.reduction && $product->specificPrice.reduction_type == 'amount'}
	{addJsDef reduction_price=$product->specificPrice.reduction|floatval}
{else}
	{addJsDef reduction_price=0}
{/if}
{if $product->specificPrice && $product->specificPrice.price}
	{addJsDef specific_price=$product->specificPrice.price|floatval}
{else}
	{addJsDef specific_price=0}
{/if}
{addJsDef specific_currency=($product->specificPrice && $product->specificPrice.id_currency)|boolval} {* TODO: remove if always false *}
{addJsDef stock_management=$stock_management|intval}
{addJsDef taxRate=$tax_rate|floatval}
{addJsDefL name=doesntExist}{l s='This combination does not exist for this product. Please select another combination.' js=1}{/addJsDefL}
{addJsDefL name=doesntExistNoMore}{l s='This product is no longer in stock' js=1}{/addJsDefL}
{addJsDefL name=doesntExistNoMoreBut}{l s='with those attributes but is available with others.' js=1}{/addJsDefL}
{addJsDefL name=fieldRequired}{l s='Please fill in all the required fields before saving your customization.' js=1}{/addJsDefL}
{addJsDefL name=uploading_in_progress}{l s='Uploading in progress, please be patient.' js=1}{/addJsDefL}
{addJsDefL name='product_fileDefaultHtml'}{l s='No file selected' js=1}{/addJsDefL}
{addJsDefL name='product_fileButtonHtml'}{l s='Choose File' js=1}{/addJsDefL}
{/strip}
{/if}
