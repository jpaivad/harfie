<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>
<script type="text/javascript" src="/peru/home/Scripts/jquery.reveal.js"></script>
<script type="text/javascript" src="/peru/home/Scripts/jquery.js"></script>



  <link rel="stylesheet" type="text/css" href="/peru/home/css/base.css">

  <link rel="stylesheet" type="text/css" href="/peru/home/css/skeleton.css">

  <link rel="stylesheet" type="text/css" href="/peru/home/css/layout.css">

  <link rel="stylesheet" type="text/css" href="/peru/home/css/font-awesome.css">

  <link rel="stylesheet" type="text/css" href="/peru/home/css/retina.css">

  <link rel="stylesheet" type="text/css" href="/peru/home/css/settings.css">

 

  <link rel="stylesheet" type="text/css" href="/peru/home/css/colors/color-blue.css">

  

  <link rel="stylesheet" type="text/css" href="/peru/home/css/reveal.css">

  

  <script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.reveal.js"></script>
<script type="text/javascript" src="/peru/home/Scripts/jquery.js"></script>




<?php
require_once dirname(__FILE__) . '/../config/config.inc.php' ;
require_once(dirname(__FILE__) . '/../config/settings.inc.php');
require_once(dirname(__FILE__).  '/../header.php');
require_once dirname(__FILE__) . '/../init.php' ;
?>

<!-- 
<button class="homenav-button homebtn-flat homenav-button" id="homeleft-drawer-btn" style="background: transparent;width:160px;top: 22px;">

            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="21%" height="69%" viewBox="0 0 31 19" enable-background="new 0 0 31 19" xml:space="preserve" class="icon">

            <rect x="0" y="8" fill="#ffffff" width="31" height="3"></rect>

            <rect x="0" y="0" fill="#ffffff" width="31" height="3"></rect>

            <rect x="0" y="16" fill="#ffffff" width="31" height="3"></rect>

                        </svg>

            <span class="hometext" style="font-size: 20px;color: white; position: absolute;display:inline;font-size: 12px;transform:;text-transform: uppercase;">Shop</span>

    </button>

-->


<div id="homehome" style="height: 2096px; max-height: 715px;display: inline;">
    <br />
    <br />
    <div class="homesixteen homecolumns">
        <h1><span>TERMINOS Y CONDICIONES</span></h1>


    </div>

    

    <div style="
    width: 60%;
    margin: auto;
    font-size: 14px;
    margin-top: 38px;
    margin-bottom: 76px;

    text-align: justify;
    font-family: Century Gothic;
" id="contehelptest">


<p class=MsoListParagraphCxSpLast style='margin-left:72.0pt;mso-add-space:auto;
text-align:justify;text-justify:inter-ideograph;line-height:115%'><b
style='mso-bidi-font-weight:normal'><span lang=ES-TRAD style='font-size:11.0pt;
line-height:115%;font-family:"Century Gothic","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'>1. ACEPTACIÓN DEL USUARIO<o:p></o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'>Usted (en adelante el
“Usuario”) al navegar, utilizar y/o visitar esta página web, Harfie.com, acepta
los términos y condiciones de los Términos de Servicio; las Políticas de
Privacidad, Políticas de Tratamiento y Transferencia de sus Datos Personales y Políticas
de Propiedad Intelectual de Harfie.com. En caso, el Usuario no aceptará ninguno
de estos términos, el Usuario no está autorizado a utilizar la página web.
Además, cabe señalar que Harfie.com posee la facultad, bajo su total
discrecionalidad, de modificar o revisar los Términos de Servicio y las
políticas de Harfie.com en cualquier momento, lo cual será debidamente
informado a todos los usuarios mediante un aviso visible dentro de la página
web, otorgándose así al Usuario la posibilidad de revisar las nuevas
modificaciones y/o revisiones, a efectos de que las acepte o las rechace
mediante el envío de un correo electrónico a la dirección que será consignada
en el aviso. <o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'>2. CUENTAS EN HARFIE.COM<o:p></o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'>Existen algunas
características y funciones de Harfie.com que para poder acceder a ellas, el
Usuario deberá crearse una cuenta en la página web. Al momento de completar su
información, el Usuario tiene la obligación de brindar datos (incluidos datos
personales) verdaderos y completos, los cuales autoriza expresa e <span
class=GramE>inequívocamente</span> que podrán ser utilizados para fines publicitarios
de los productos de Harfie.com, así como también podrán ser transferidos,
nacional e internacionalmente, por Harfie.com. Para poder mantener un buen uso
de su cuenta, el Usuario se obliga a ser el único responsable de cualquier
actividad que ocurra en su cuenta, así como a mantener su contraseña de acceso bajo
extrema seguridad. Si el Usuario percibe algún acto ilícito, no autorizado o
violación de seguridad de su cuenta deberá comunicarse de forma inmediata con
Harfie.com,. Cabe resaltar, que al ocurrir un hecho de esta magnitud, bajo
ninguna circunstancia Harfie.com será el responsable por las pérdida causadas
al Usuario; sin embargo, si se producen pérdidas o daños a Harfie.com, el
Usuario sí será el responsable por dicho uso no autorizado, debiendo resarcir
económicamente a Harfie.com por cualquier perjuicio Finalmente, el Usuario no
está autorizado a usar la cuenta de otra persona sin su permiso expreso.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'>3. CONDUCTA Y CONTENIDO DEL USUARIO<o:p></o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'>El Usuario al ser
dueño de una cuenta en Harfie.com tiene permitido adjuntar fotos, contenido
textual (“comentarios del Usuario”), diseños e ilustraciones que podrán ser
puestos en distintas prendas. Todo archivo, contenido gráfico o textual
proporcionado por el Usuario está sujeto a los Términos de propiedad
intelectual de Harfie.com. El único responsable de todo el contenido brindado o
proporcionado a Harfie.com es el Usuario. Asimismo, el Usuario afirma, declara y
garantiza que dicho contenido ha sido obtenido y/o elaborado sin vulnerar
ningún tipo de derechos de terceros, ni ilícitamente, sino por el contrario, el
Usuario garantiza expresamente que cuenta con los derechos, licencias,
consentimientos y permisos necesarios para utilizar o ceder el uso de tal
contenido a terceros. En atención a ello, el Usuario autoriza expresamente a
Harfie.com a usar todas las patentes, marcas registradas, secretos de comercio,
derechos de autor, u otros derecho de propiedad en, con y sobre todo contenido
proporcionado por el Usuario. Asimismo, el Usuario permite la inclusión y uso
del contenido proporcionado de todas las maneras contempladas por la página web
Harfie.com y los Términos de Servicio<span class=GramE>..</span><o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'>Sin embargo, al proporcionar
contenido a Harfie.com, el Usuario le otorga a Harfie.com licencia mundial, no
exclusiva, exenta de canon y transferible para usar, reproducir, distribuir,
preparar trabajos entregables, divulgar y desarrollar dicho contenido del
Usuario en conexión con la página Harfie.com y los negocios de Harfie.com,
incluido, sin limitación, para promocionar y redistribuir parte o todo lo
contenido en Harfie.com en cualquier formato de comunicación y a cualquier
canal, actual, o por descubrirse en el futuro.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'>El Usuario acepta que
no proporcionará material que tenga derechos de autor, que esté protegido por
secreto comercial, que pertenezca a marcas debidamente registradas o
notoriamente conocidas, o de otra forma, directa o indirecta, esté sujeto a los
derechos de propiedad de terceras partes, incluido derechos de privacidad y de
publicidad, a menos que el Usuario sea el propietario de dichos derechos o
cuente con completo permiso del propietario para subir material y otorgar las
licencias antes descritas aquí. El Usuario acepta que Harfie.com tiene el derecho
de rechazar la aceptación, de limitar el acceso, de remover, y/o rechazar la
publicación de cualquier contenido, el cual sea considerado por Harfie.com como
inapropiado, amoral, escandaloso, ofensivo, o de mal gusto. Todas las
determinaciones en cuanto a la calificación del contenido del Usuario, están
sujetas a única y final discreción de Harfie.com. El Usuario está obligado a
aceptar cualquier decisión de Harfie.com.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'>El Usuario se obliga expresamente
a no utilizar, copiar o distribuir cualquier contenido de la página web de
Harfie.com, o que haya sido obtenida a través de la misma, para cualquier
propósito comercial. salvo de la manera expresa permitida aquí, El Usuario
entiende que puede estar expuesto a contenidos de diversas fuentes, y que
Harfie.com no es responsable por la precisión, utilidad, seguridad, o derechos
de propiedad intelectual o derechos relacionados a dicho contenido Asimismo, el
Usuario entiende y conoce que puede estar expuesto a contenido impreciso,
ofensivo, indecente, o censurable; por lo que mediante la aceptación de este
documento, el Usuario renuncia expresamente a cualquier derecho legal,
contractual y/o recursos legales que tenga o pueda interponer en contra de
Harfie.com con respecto a lo antes mencionado, aceptando que Harfie.com, sus
propietarios, operadores, afiliados y/o licenciadores, quedan liberados de toda
responsabilidad permitida por ley de todos los asuntos relacionados al uso de
este sitio y serán indemnizados por el Usuario, en caso éste decida adoptar
medidas legales en contrato de cualquiera de las partes mencionadas. <o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'>4. PROPIEDAD<o:p></o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'>Todos los contenidos
gráficos o textuales (imágenes, ilustraciones, fotografías, dibujos, diseños,
íconos, logos, entre otros materiales) son de derecho de propiedad y marca
registrada controlada por Harfie.com. En caso el Usuario modifique o altere
alguno de estos materiales reconoce que es un trabajo derivado; del cual,
Harfie.com es propietario de todos los derechos ante cualquier cambio o
alteración. <o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'>5. RESTRICCIÓN DE LA GARANTÍA<o:p></o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'>El Usuario acepta que
el uso que haga de la página web de Harfie.com correrá por cuenta única y
exclusiva del Usuario permitida por ley. Harfie.com, sus funcionarios,
directores, empleados, y agentes restringen expresamente todas las garantías,
expresas o implícitas, en conexión con la página web de Harfie.com y el uso que
los Usuarios hagan de ella. Harfie.com no da garantías o hace representaciones
acerca de la precisión o integridad del contenido de este sitio o el contenido
de cualquier sitio ligado al mismo y no asume responsabilidad por cualquier (i)
error, equivocaciones, o imprecisiones de contenido, (ii) agravios personales o
daños de propiedad, de cualquier naturaleza, que resulten del acceso del Usuario
y uso de la página Harfie.com, (iii) acceso no autorizado o uso de nuestros
servidores seguros y/o cualquier y toda información personal y/o información
financiera almacenada aquí, (iv) interrupción o cese de transmisión a desde la
página web de Harfie.com, (v) errores del sistema, virus informáticos, Troyanos
o aquellos que podrían ser transmitidos a o hacia nuestro sitio web por
terceras partes, y/o (vi) errores y omisiones de cualquier contenido o por
cualquier pérdida o daño incurrido como resultado del uso de cualquier
contenido proporcionado, enviado por correo electrónico, transmitido, o de otra
manera puesto disponible a través de la página web Harfie.com. Harfie.com no
garantiza, respalda, garantiza, o asume responsabilidad alguna por cualquier
producto o servicio publicado u ofrecido por terceras partes a través de la
página de Harfie.com o cualquier página con hipervínculo o página web destacada
en cualquier banner u otra publicidad, y Harfie.com no será parte de o será
responsable, de alguna manera, de monitorear cualquier transacción entre el
Usuario y las terceras partes proveedoras de productos o servicios. Al igual
que con la compra de cualquier producto o servicio a través de cualquier medio
o en cualquier ambiente, usted deberá usar su mejor juicio y ejercitar la
precaución cuando fuese apropiado.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'>6. LIMITACIÓN DE LA RESPONSABILIDAD<o:p></o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'>Bajo ningún evento
Harfie.com, sus funcionarios, directores, empleados, o agentes, serán responsables
ante el Usuario por cualquier daño directo, indirecto, incidental, especial,
punitivo o consecuente que resulte de cualquier (i) errores, equivocaciones, o
imprecisiones de contenido, (ii) agravios personales o daños de la propiedad de
cualquier naturaleza, resultado de su acceso a y uso de la página web de Harfie.com,
(iii) acceso no autorizado o uso de nuestros servidores seguros y/o cualquier y
toda información personal y/o información financiera almacenada aquí, (iv)
interrupción o cese de transmisión a o desde la página web de <span
class=SpellE>Harfie</span>, (v) errores del sistema, virus informático,
Troyanos o aquellos que podrían ser transmitidos a o hacia nuestro sitio web
por terceras partes, y/o (vi) errores y omisiones de cualquier contenido o por
cualquier pérdida o daño incurrido como resultado del uso de cualquier
contenido proporcionado, enviado por correo electrónico, transmitido, o de otra
manera puesto disponible a través de la página web Harfie.com, si está basado
sobre una garantía, contrato, acto ilegal, o cualquier otra teoría legal, y si
se le informa o no a la compañía sobre la posibilidad de dichos daños. Las
limitaciones precedentes de responsabilidad se deberá aplicar a todo aquello
exento permitido por ley en la jurisdicción aplicable. El Usuario específicamente
conoce que Harfie.com no deberá ser responsable de <span
style='mso-spacerun:yes'> </span>aquel contenido proporcionado por el Usuario
que sea difamatorio, ofensivo, o califique como una conducta ilegal de parte de
terceras partes. Asimismo, Harfie.com no será responsable del riesgo de agravio
o daño causado por todo lo precedente y que descansa enteramente sobre el Usuario.
<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'>La página web de
Harfie.com es controlada y ofrecida por Harfie.com desde sus instalaciones en Perú.
Harfie.com es una página web que puede ser utilizada en otras locaciones; sin
embargo, aquellas personas que accedan o usen Harfie.com en otras
jurisdicciones por su voluntad propia, son exclusivamente responsables del
cumplimiento de sus leyes locales y de este documento.<span
style='mso-spacerun:yes'>  </span>En caso exista cualquier conflicto entre
harfie.com y algún usuario extranjero o de otro país, éste deberá ser resuelto
ante las autoridades peruanas correspondientes con la exclusiva aplicación de
las leyes peruana, dejándose expresamente de lado las demás jurisdicciones y
normativa extranjera. <o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'>7. DERECHOS DEL VENDEDOR<o:p></o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'>El desembolso de las
regalías o comisiones acumuladas se procederá solo en los casos que los
“Vendedores” superen los S/. XXX (XXX 00/100 NUEVOS SOLES) al cierre del mes.
El pago de dichas comisiones se realizará mediante un depósito de efectivo en
el banco de su preferencia. Dentro del acuerdo entre Harfie.com y el Usuario
Vendedor queda estipulado que la comisión a recibir corresponde a S/. 5 por
cada venta de algún artículo que posea su diseño (contenidos gráficos o
textuales, imágenes, ilustraciones, fotografías, dibujos, diseños, íconos)<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'>8. INDEMNIDAD<o:p></o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'>El Usuario acepta
defender, indemnizar y mantener a Harfie.com, sus funcionarios, directores,
empleados y agentes, libres de toda responsabilidad de y contra cualquier y
todo reclamo, daño, obligación, pérdida, responsabilidad, costo o deuda, y
gasto (incluido pero no limitado las tarifas del abogado) provenientes de (i)
el uso y acceso del Usuario a la página web de Harfie.com; (ii) violación del
Usuario de cualquier término de los Términos de Servicio, Política de
Privacidad de Harfie.com o Política de Propiedad Intelectual de Harfie.com;
(iii) violación del Usuario de cualquier derecho de terceras partes, sin
incluir limitación alguna de derechos de propiedad, propiedad, o derechos de
privacidad; (iv) cualquier reclamo que alguna de las contribuciones o Artes de
sus usuarios que cause daño a terceras partes. Esta defensa y obligación de
indemnización sobrevivirá en los Términos de Servicio y en cualquier y todo uso
de la página web de Harfie.com.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'>9. CAPACIDAD DE ACEPTACIÓN DE LOS TÉRMINOS DE
SERVICIO<o:p></o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'>El Usuario declara ser
mayor de edad, un menor emancipado y/o posee consentimiento legal parental o de
su apoderado, y puede y es competente para aceptar los términos, condiciones,
obligaciones, afirmaciones, representaciones, y garantías especificadas en los
Términos de Servicio, y para acatar y cumplir con los Términos de Servicio. Solo
se considerará como Usuario a cualquier persona natural que afirme que es mayor
de 13 años de edad, dado que, la página web de Harfie.com no está dirigida a
niños menores de 13 años. <o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'>10. DATOS PERSONALES<o:p></o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'>De conformidad a lo
dispuesto en la Ley Nº 29733, Ley de Protección de Datos Personales (en
adelante, la “LPDP”), el Usuario autoriza de manera previa, expresa e
inequívoca a Harfie.com a tratar y transferir, nacional e internacionalmente,
cualquier de sus datos personales. Cabe señalar que, el Usuario reconoce que su
información de contacto, así como sus demás datos personales serán almacenados
en el Banco de Datos de Clientes o Proveedores de Harfie.com, según
corresponda, y utilizados por éste para remitirle publicidad y/o información
relevante y relacionada con el negocio de Harfie.com <o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=ES-TRAD style='font-size:11.0pt;font-family:"Century Gothic","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:Arial;
mso-fareast-language:ES'>Al otorgar su autorización, Harfie.com cumplirá el
marco legal sobre protección de datos personales en el Perú; razón por la cual
le garantizamos que su información será preservada para los fines anteriormente
descritos, bajo estricta seguridad y por el plazo que usted crea conveniente.
Asimismo, usted podrá dirigirse al siguiente domicilio ubicado en (…<span
class=GramE>) ,</span> Distrito de (…), Provincia y Departamento de Lima, o a
los siguientes correos electrónico (…) con el asunto “<u>Protección de Datos
Personales</u>”, a efectos de plantear cualquier solicitud destinada a que Harfie.com
deje de tener y/o cese de tratar su información de acuerdo a la LPDP, o le
permita acceder a ella, actualizarla, rectificarla o suprimirla de nuestra base
de datos.<o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'>11. LEY APLICABLE RESOLUCIÓN DE CONTROVERSIAS<o:p></o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'>Cualquier
discrepancia y/o reclamo que pueda surgir entre la relación del Usuario,
nacional o extranjero, y Harfie.com deberá resolverse por de manera exclusiva
ante las autoridades peruanas respectivas, bajo la aplicación de las leyes
peruanas correspondientes. En ese sentido, las partes excluyen expresamente la
competencia de cualquier autoridad de otras jurisdicciones, sometiéndose a la
actuación de los órganos peruanos competentes. <o:p></o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'>12. ASIGNACIÓN<o:p></o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><b style='mso-bidi-font-weight:normal'><span lang=ES-TRAD
style='font-size:11.0pt;line-height:115%;font-family:"Century Gothic","sans-serif";
mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%;mso-pagination:none;mso-layout-grid-align:none;text-autospace:
none'><span lang=ES-TRAD style='font-size:11.0pt;line-height:115%;font-family:
"Century Gothic","sans-serif";mso-bidi-font-family:Arial'>Los Términos de
Servicio, y cualquier derecho y licencia otorgada en el presente, no puede ser
transferido o asignado por el Usuario, pero puede ser asignado por Harfie.com
sin ninguna restricción.<b style='mso-bidi-font-weight:normal'><o:p></o:p></b></span></p>



    </div>

</div>






  <!-- JAVASCRIPT

    ================================================== -->

    <script type="text/javascript" src="/peru/home/Scripts/jquery.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/modernizr.custom.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/royal_preloader.min.js"></script>

    <script type="text/javascript">

        Royal_Preloader.config({

            mode: 'text', // 'number', "text" or "logo"

            text: 'HARFIE.COM',

            timeout: 0,

            showInfo: true,

            opacity: 1,

            background: ['#FFFFFF']

        });

    </script>

    <script type="text/javascript" src="/peru/home/Scripts/classie.js"></script>



    <script type="text/javascript" src="/peru/home/Scripts/cbpAnimatedHeader.min.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/styleswitcher.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/retina-1.1.0.min.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.themepunch.plugins.min.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.themepunch.revolution.min.js"></script>

    <script type="text/javascript">



        var revapi;



        jQuery(document).ready(function () {

          $('.columns-container').attr('style','');
            revapi = jQuery('.hometp-banner').revolution(

             {

                 delay: 7000,

                 startwidth: 1170,

                 startheight: 700,

                 hideThumbs: 10,

                 fullWidth: "on",

                 forceFullWidth: "on"

             });



        });	//ready



    </script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.easing.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/svganimations.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.parallax-1.1.3.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.bxslider.min.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/contact.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.fitvids.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/plugins.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/template.js"></script>

    <script type="text/javascript" src="/peru/themes/leo_tshirt/js/modules/blocktopmenu/js/blocktopmenu.js"></script>
    <script type="text/javascript" src="/peru/themes/leo_tshirt/js/modules/blocktopmenu/js/superfish-modified.js"></script>


<?php
require_once(dirname(__FILE__).  '/../footer.php');

?>