<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>
<script type="text/javascript" src="/peru/home/Scripts/jquery.reveal.js"></script>
<script type="text/javascript" src="/peru/home/Scripts/jquery.js"></script>



  <link rel="stylesheet" type="text/css" href="/peru/home/css/base.css">

  <link rel="stylesheet" type="text/css" href="/peru/home/css/skeleton.css">

  <link rel="stylesheet" type="text/css" href="/peru/home/css/layout.css">

  <link rel="stylesheet" type="text/css" href="/peru/home/css/font-awesome.css">

  <link rel="stylesheet" type="text/css" href="/peru/home/css/retina.css">

  <link rel="stylesheet" type="text/css" href="/peru/home/css/settings.css">

 

  <link rel="stylesheet" type="text/css" href="/peru/home/css/colors/color-blue.css">

  

  <link rel="stylesheet" type="text/css" href="/peru/home/css/reveal.css">

  

  <script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>

	<script type="text/javascript" src="/peru/home/Scripts/jquery.reveal.js"></script>
<script type="text/javascript" src="/peru/home/Scripts/jquery.js"></script>


<?php
require_once dirname(__FILE__) . '/../config/config.inc.php' ;
require_once(dirname(__FILE__) . '/../config/settings.inc.php');
require_once(dirname(__FILE__).  '/../header.php');
require_once dirname(__FILE__) . '/../init.php' ;
?>
<div id="home" style="height: 2096px; max-height: 715px;display: inline;">
    <div class="sixteen columns">
        <h1><span>NOSOTROS</span></h1>


          <div id="homelogos">
              <div class="homecontainer z-index">
                <br><br>
                Harfie es la única marca en el mundo que te permite diseñar tu ropa; mezclando texturas, patrones y colores. El proceso de confección comienza en el momento que se realiza la compra. Así, cada producto es hecho de manera personalizada y especial para cada cliente.
                <br><br>
              </div>
          </div>

          <h1><span>EL EQUIPO</span></h1>

    </div>

    <img src="/peru/home/images/parallax/angelorodrigoPS.png" style="
    margin: auto;
    left: 0;
    right: 0;
    text-align: center;
    display: block;
    width: 80%;" />
<div id="homelogos">
  <div class="homecontainer z-index">
    <div class="homesixteen homecolumns jsLeft"  data-scrollreveal="enter bottom and move 150px over 1s">
      <div class="homelogos-wrapNosotros2">
        <h4>ANGELO ANDRADE</h4>
        Co-Fundador

      </div>
      <div class="homelogos-wrapNosotros2">
                          <h4>RODRIGO BEJARANO</h4>
        Co-Fundador
      </div>

      
        
    </div>
    
  </div>
</div>



<div style="width:100%;text-align: center;margin-top: 70px;">
  <div  style="
    
    
    margin: auto;
    left: 0;
    right: 0;
    text-align: center;
    display: inline-block;
    vertical-align: top;


">
 <img src="/peru/home/images/parallax/jesusPS.png" style="width:80%"  />
<h4>JESUS PAIVA</h4>
Programación Web
  </div>
 <div  style="
    
    
    margin: auto;
    left: 0;
    right: 0;
    text-align: center;
    display: inline-block;
    
    vertical-align: top;


">
<img src="/peru/home/images/parallax/yadiraPS.png" style="
    
    
    
    margin: auto;
    left: 0;
    right: 0;
    text-align: center;
    display: inline-block;
    vertical-align: top;
    width: 80%;



" />
<h4>YADIRA AGURTO</h4>
Confección

</div>
</div>


    <div style="
    width: 60%;
    margin: auto;
    font-size: 14px;
    margin-top: 38px;
    margin-bottom: 76px;

    text-align: justify;
    font-family: Century Gothic;



">

        


    </div>
</div>







  <!-- JAVASCRIPT

    ================================================== -->

    <script type="text/javascript" src="/peru/home/Scripts/jquery.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/modernizr.custom.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/royal_preloader.min.js"></script>

    <script type="text/javascript">

        Royal_Preloader.config({

            mode: 'text', // 'number', "text" or "logo"

            text: 'HARFIE.COM',

            timeout: 0,

            showInfo: true,

            opacity: 1,

            background: ['#FFFFFF']

        });

    </script>

    <script type="text/javascript" src="/peru/home/Scripts/classie.js"></script>



    <script type="text/javascript" src="/peru/home/Scripts/cbpAnimatedHeader.min.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/styleswitcher.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/retina-1.1.0.min.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.themepunch.plugins.min.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.themepunch.revolution.min.js"></script>

    <script type="text/javascript">



        var revapi;



        jQuery(document).ready(function () {

        $('.columns-container').attr('style','');
        //jQuery('.container').addClass('homecontainer')
        //jQuery('.homecontainer').removeClass('container')

        
            revapi = jQuery('.hometp-banner').revolution(

             {

                 delay: 7000,

                 startwidth: 1170,

                 startheight: 700,

                 hideThumbs: 10,

                 fullWidth: "on",

                 forceFullWidth: "on"

             });



        });	//ready



    </script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.easing.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/svganimations.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.parallax-1.1.3.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.bxslider.min.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/contact.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.fitvids.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/plugins.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/template.js"></script>

  <script type="text/javascript" src="/peru/themes/leo_tshirt/js/modules/blocktopmenu/js/blocktopmenu.js"></script>
    <script type="text/javascript" src="/peru/themes/leo_tshirt/js/modules/blocktopmenu/js/superfish-modified.js"></script>




    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <style type="text/css">
    #homelogos{
      display: block!important;
    }
    </style>

<?php
require_once(dirname(__FILE__).  '/../footer.php');

?>