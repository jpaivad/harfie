<?php
return array(
  'mnuCART'=>
    'BOLSA'
  ,
  'mnuLANGUAGE'=>
    'IDIOMA'
  ,
  'mnuLogin'=>
    'INGRESAR'
  ,
  'mnusubCREATEANACCOUNT'=>
    'CREAR UNA CUENTA'
  ,
  'mnusubENGLISH'=>
    'ENGLISH'
  ,
  'mnusubESPAÑOL'=>
    'ESPAÑOL'
  ,
  'mnusubSIGNIN'=>
    'INGRESAR'
  ,
  'mnuWISHLIST'=>
    'LISTA DE DESEOS'
  ,
  'bnbtnCOMBINESTYLESINANYWAY'=>
    'COMBINA COMO QUIERAS'
  ,
  'bnbtnCREATE'=>
    'CREA'
  ,
  'bnbtnCUSTOMIZEYOURSHIRT'=>
    'PERZONALIZA TU PRODUCTO'
  ,
  'bnbtnDESIGN'=>
    'DISE&#205;A'
  ,
  'bnbtnDESIGNNOW'=>
    'DISE&#209;A AHORA'
  ,
  'bnbtnNEWA'=>
    'NOVED'
  ,
  'bnbtnQUALITYANDSERVICE'=>
    'CALIDAD Y SERVICIO'
  ,
  'bnbtnRRIVALS'=>
    'ADES'
  ,
  'bnbtnSHOPNOW'=>
    'COMPRA AHORA'
  ,
  'bnbtnSHOPONLINE'=>
    'COMPRA EN LINEA'
  ,
  'bnbtnWITHTOTALCONTROL'=>
    'CON TOTAL CONTROL'
  ,
  'menuleftBESTSELLERS'=>
    'LO MAS VENDIDO'
  ,
  'menuleftCROP'=>
    'CROP'
  ,
  'menuleftCUSTOMIZENOW'=>
    'DISEÑA AHORA'
  ,
  'menuleftHOODIES'=>
    'SUDADERAS'
  ,
  'menuleftMAN'=>
    'HOMBRE'
  ,
  'menuleftTANKS'=>
    'BVD'
  ,
  'menuleftTEES'=>
    'POLOS'
  ,
  'menuleftWOMAN'=>
    'MUJER'
  ,
  'seccion1COMPRA'=>
    'COMPRA'
  ,
  'seccion1CREA'=>
    'CREA'
  ,
  'seccion1VENDE'=>
    'VENDE'
  ,
  'seccion1VISTETUCREATIVIDAD'=>
    '#VISTETUCREATIVIDAD'
  ,
  'bestsellerComprar'=>
    'COMPRAR'
  ,
  'modal1text'=>
    'El deseo de ser único y queremos mostrar nuestra esencia es la razón por la Harfie ha desarrollado una herramienta que contribuye a este deseo.'
  ,
  'modal2text'=>
    'Expresa tu creatividad; Harfie incentiva a sus usuarios a exponer sus obras y poder ganar una comisión por cada diseño que sea vendido. De esta forma, podrás mostrar tus creaciones a todo el mundo y empezar a ganar dinero con algo que te apasiona.'
  ,
  'modal2text1'=>
    '*La comisión que recibirán representa el 8% del precio de cada producto vendido (polos, bvd y crop).'
  ,
  'modal3ENTREGAS'=>
    'ENTREGAS'
  ,
  'modal3text1'=>
    'El delivery depende de la ubicación geográfica y el método de envío. Al mismo tiempo, se debe de tener en consideración que los productos demoran 4 días en salir del almacén. Para poder comprobar el tiempo de envío deberá realizar los siguientes pasos:'
  ,
  'modal3text3'=>
    '1)	Agrega el producto a tu carrito de compras dando clic en "Comprar Ahora".'
  ,
  'modal3text4'=>
    '2)	Abajo del listado de tus productos seleccionados, introduce los siguientes datos en la sección de dirección de entrega: Selecciona el país, departamento, la provincia y el distrito (Código postal).'
  ,
  'modal3text5'=>
    '3)	Automáticamente aparecerá el tiempo de envío en la columna "TIEMPO ESTIMADO DE ENTREGA"'
  ,
  'modal3text6'=>
    'Recuerde que el tiempo de entrega se cuenta en días hábiles ( Sábado , domingo y días festivos ( calendario) están excluidos) y comienza a funcionar después de confirmado su pago, no necesariamente en el mismo día de la compra .'
  ,
  'secctioninfoContinuarleyendo'=>
    'Continuar leyendo..'
  ,
  'secctioninfoORDERINGSHIPPING'=>
    'PEDIDOS &amp; ENV&#205;OS'
  ,
  'secctioninfoREADABOUTIT'=>
    'SIGA LEYENDO'
  ,
  'secctioninfoText1'=>
    'Entrega gratuita a toda Lima Metropolitiana
Y env&#237;os internacionales provincias.'
  ,
  'secctioninfotext2'=>
    'Queremos que te vistas realmente como eres'
  ,
  'secctionWHYCUSTOM'=>
    'POR QU&#201; CUSTOMIZAR'
  ,
  'sepClients'=>
    'Clientes'
  ,
  'sepCustomtShirt'=>
    'Polos Perzonalizados'
  ,
  'sepSellers'=>
    'Ventas'
  ,
  'sepTshirt'=>
    'Polos'
  ,
  'sepWHYSELL'=>
    'WHY SELL'
  ,
  'servicescustomize'=>
    'PERSONALIZAR'
  ,
  'servicesSHOPMEN'=>
    'TIENDA DE HOMBRE'
  ,
  'servicesSHOPWOMAN'=>
    'TIENDA DE MUJER'
  ,
  'mnufooterabouttext'=>
    'NOSOTROS'

  ,
  'mnufooteraboutlink'=>
    'Harfie es una tienda virtual de prendas básicas; donde podrás encontrar la ropa adecuada para tu estilo. Ofrecemos todas nuestras prendas en el mejor algodón peruano y también, existe la posibilidad de utilizar algodón 100% orgánico. Toda nuestra ropa es hecha mano por confeccionistas peruanos. Todas nuestras ordenes son elaborados una vez que ingresa el pedido y es hecha de forma especial para ti. Nos gusta que nuestros clientes sientan el control sobre su forma de vestir y así, poder mostrar su esencia.'

  ,
  'mnufootercontactustext'=>
    'CONTÁCTANOS'

  ,
  'mnufootercontactuslink'=>
    'Déjanos conocer tu opinión o inquietud, que con mucho gusto la resolveremos. Estamos para ayudarte: Lunes a viernes, 8am – 5pm Sábado, 9am – 2pm info@harfie.com'

 ,
  'mnufooterneedhelptext'=>
    'AYUDA'

 ,
  'mnufooterneedhelplink'=>
    'Comúnicate con nosotros, que nuestro equipo de servicio al cliente resolverá tus dudas. Llámanos a: 51 1 563 3644 Correo: info@harfie.com'

 ,
  'mnufooterprivacyandtermstext'=>
    'TÉRMINOS & CONDICIONES'

 ,
  'mnufooterfollowustext'=>
    'SÍGUENOS EN');
