<?php
return array(
  'mnuCART' =>
    'CART'
  ,
  'mnuLANGUAGE' =>
    'LANGUAGE'
  ,
  'mnuLogin' =>
    'LOG IN'
  ,
  'mnusubCREATEANACCOUNT' =>
    'CREATE AN ACCOUNT'
  ,
  'mnusubENGLISH' =>
    'ENGLISH'
  ,
  'mnusubESPAÑOL' =>
    'ESPAÑOL'
  ,
  'mnusubSIGNIN' =>
    'SIGN IN'
  ,
  'mnuWISHLIST' =>
    'WISH LIST'
  ,
  'bnbtnCOMBINESTYLESINANYWAY' =>
    'COMBINE STYLES IN ANY WAY'
  ,
  'bnbtnCREATE' =>
    'CREATE'
  ,
  'bnbtnCUSTOMIZEYOURSHIRT' =>
    'CUSTOMIZE YOUR SHIRT'
  ,
  'bnbtnDESIGN' =>
    'DESIGN'
  ,
  'bnbtnDESIGNNOW' =>
    'DESIGN NOW'
  ,
  'bnbtnNEWA' =>
    'NEW A'
  ,
  'bnbtnQUALITYANDSERVICE' =>
    'QUALITY AND SERVICE'
  ,
  'bnbtnRRIVALS' =>
    'RRIVALS'
  ,
  'bnbtnSHOPNOW' =>
    'SHOP NOW'
  ,
  'bnbtnSHOPONLINE' =>
    'SHOP ONLINE'
  ,
  'bnbtnWITHTOTALCONTROL' =>
    'WITH TOTAL CONTROL'
  ,
  'menuleftBESTSELLERS' =>
    'BEST SELLERS'
  ,
  'menuleftCROP' =>
    'CROP'
  ,
  'menuleftCUSTOMIZENOW' =>
    'CUSTOMIZE NOW'
  ,
  'menuleftHOODIES' =>
    'HOODIES'
  ,
  'menuleftMAN' =>
    'MAN'
  ,
  'menuleftTANKS' =>
    'TANKS'
  ,
  'menuleftTEES' =>
    'TEES'
  ,
  'menuleftWOMAN' =>
    'WOMAN'
  ,
  'seccion1COMPRA' =>
    'SHOP'
  ,
  'seccion1CREA' =>
    'CREATE'
  ,
  'seccion1VENDE' =>
    'SELL'
  ,
  'seccion1VISTETUCREATIVIDAD' =>
    '#WEARYOURCREATIVITY'
  ,
  'bestsellerComprar' =>
    'BUY'
  ,
  'modal1text' =>
    'The desire to be unique and want to show our essence is the reason why Harfie has developed a tool that contributes to this desire.'
  ,
  'modal2text' =>
    'Express your creativity; Harfie encourages its users to exhibit their work and earn a commission for each design that is sold. In this way, you can show your creations to the world and start making money with something you love.'
  ,
  'modal2text1' =>
    '* The commission received it represents 8 % of each sold product ( poles , BVD and crop ) .'
  ,
  'modal3ENTREGAS' =>
    'ORDERING & SHIPPING'
  ,
  'modal3text1' =>
    'The delivery depends on the geographical location and shipping method. At the same time , it must take into consideration that products delayed four days to leave the warehouse . To check the delivery time must perform the following steps:'
  ,
  'modal3text2' =>
    ''
  ,
  'modal3text3' =>
    '1) Add the product to your shopping cart by clicking on " Buy Now " .'
  ,
  'modal3text4' =>
    '2) Below the list of your selected product , enter the following data in the delivery address section : Select the country , department, province and district ( ZIP ) .'
  ,
  'modal3text5' =>
    '3) shipping time automatically appear in the column " DELIVERY TIME " .'
  ,
  'modal3text6' =>
    'Remember that the delivery time is counted in business days ( Saturday, Sunday and public holidays ( calendar ) are excluded) and starts running after your payment is confirmed, not necessarily on the same day of purchase.'
  ,
  'secctioninfoContinuarleyendo' =>
    'Continue reading.'
  ,
  'secctioninfoORDERINGSHIPPING' =>
    'ORDERING &amp; SHIPPING'
  ,
  'secctioninfoREADABOUTIT' =>
    'READ ABOUT IT'
  ,
  'secctioninfoText1' =>
    'Free delivery to all Lima Metropolitiana
And international shipments provinces .'
  ,
  'secctioninfotext2' =>
    'We want to make you look just the way you are'
  ,
  'secctionWHYCUSTOM' =>
    'WHY CUSTOM'
  ,
  'sepClients' =>
    'Clients'
  ,
  'sepCustomtShirt' =>
    'Custom T-Shirt'
  ,
  'sepSellers' =>
    'Sellers'
  ,
  'sepTshirt' =>
    'T-shirt'
  ,
  'sepWHYSELL' =>
    'WHY SELL'
  ,
  'servicescustomize' =>
    'customize'
  ,
  'servicesSHOPMEN' =>
    'SHOP MEN'
  ,
  'servicesSHOPWOMAN' =>
    'SHOP WOMAN'
  ,
  'mnufooterabouttext'=>
    'ABOUT US'

  ,
  'mnufooteraboutlink'=>
    'Harfie es una tienda virtual de prendas básicas; donde podrás encontrar la ropa adecuada para tu estilo. Ofrecemos todas nuestras prendas en el mejor algodón peruano y también, existe la posibilidad de utilizar algodón 100% orgánico. Toda nuestra ropa es hecha mano por confeccionistas peruanos. Todas nuestras ordenes son elaborados una vez que ingresa el pedido y es hecha de forma especial para ti. Nos gusta que nuestros clientes sientan el control sobre su forma de vestir y así, poder mostrar su esencia.'

  ,
  'mnufootercontactustext'=>
    'CONTACT US'

  ,
  'mnufootercontactuslink'=>
    'Déjanos conocer tu opinión o inquietud, que con mucho gusto la resolveremos. Estamos para ayudarte: Lunes a viernes, 8am – 5pm Sábado, 9am – 2pm info@harfie.com'

 ,
  'mnufooterneedhelptext'=>
    'NEED HELP'

 ,
  'mnufooterneedhelplink'=>
    'Comúnicate con nosotros, que nuestro equipo de servicio al cliente resolverá tus dudas. Llámanos a: 51 1 563 3644 Correo: info@harfie.com'

 ,
  'mnufooterprivacyandtermstext'=>
    'PRIVACY & TERMS'

 ,
  'mnufooterfollowustext'=>
    'FOLLOW US'  );