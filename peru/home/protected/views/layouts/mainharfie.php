
<!DOCTYPE html>

<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->

<!--[if gt IE 8]>

<!-->

<html class="no-js" lang="en">

<!--<![endif]-->

<!-- Mirrored from ivang-design.com/chronos/revolutionslider/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 08 May 2015 06:12:15 GMT -->



<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 

    <!-- Basic Page Needs

    ================================================== -->

    
     





    <meta name="description" content="">

    <meta name="author" content="">



    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">



    <title>.:HARFIE:.</title>

    <link rel="shortcut icon" href="favicon.png">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png">

    <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png">







<!-- CSS

  ================================================== -->

  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/base.css">

  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/skeleton.css">

  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/layout.css">

  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.css">

  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/retina.css">

  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/settings.css">

 

  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/colors/color-blue.css">

  

  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/reveal.css">

  

  <script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>

	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.reveal.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.js"></script>

  
</head>

<body class="royal_loader">

	<button class="nav-button btn-flat nav-button" id="left-drawer-btn" style="background: transparent;width:160px;top: 22px;">

            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="21%" height="69%" viewBox="0 0 31 19" enable-background="new 0 0 31 19" xml:space="preserve" class="icon">

            <rect x="0" y="8" fill="#ffffff" width="31" height="3"></rect>

            <rect x="0" y="0" fill="#ffffff" width="31" height="3"></rect>

            <rect x="0" y="16" fill="#ffffff" width="31" height="3"></rect>

                        </svg>

            <span class="text" style="font-size: 20px;color: white; position: absolute;display:inline;font-size: 12px;transform:;text-transform: uppercase;">Shop</span>

    </button>

	<div id="menu-wrap" class="menu-back cbp-af-header">



        <div class="container" style="width: 100%">



            <div class="sixteen columns" style="width: 100%">

                <a href="/peru/home">

                <div class="logo" style="left: 0;right:0;margin:auto">

                </div>

                </a>

                <ul class="slimmenu">

                    <li>

                        <a class="scroll" href="#home"><?php echo Yii::t('app','mnuLANGUAGE');?> </a>

                        <ul>

                            <li>

<a href="http://harfie.com/peru/home/?lg=en">

                                <?php echo Yii::t('app','mnusubENGLISH');?></a>

                                &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;

						    </li>

						    <li>

						    	<a href="http://harfie.com/peru/home/?lg=es">

						        <?php echo Yii::t('app','mnusubESPAÑOL');?></a>

						    </li>



						</ul>



					</li>

					<li>

					<a class="scroll" href="#about">

					    <?php echo Yii::t('app','mnuLogin');?>



					</a>

					<ul>

					    <li>

					        <a href="#"><?php echo Yii::t('app','mnusubSIGNIN');?> </a>

					    </li>

					    <li>

					        <a href="#"><?php echo Yii::t('app','mnusubCREATEANACCOUNT');?> </a>

					    </li>



					</ul>



					</li>

					<li>

					<a class="scroll" href="#work"><?php echo Yii::t('app','mnuWISHLIST');?>  </a>

					<ul style="width: 300px;height:300px;left:-140px">

					    <li>

					        <div style="width: 300px;height:300px;">

					            Lsta de Interes Vacio

					        </div>



					    </li>



					</ul>



					</li>

					<li>

					<a class="scroll" href="#services"><?php echo Yii::t('app','mnuCART');?></a>

					<ul style="width: 300px;height:300px;left:-220px">

					    <li style="width: 300px;">



					        Shopping Bag

					        <hr />



					        <span style="text-align: center;width:300px">

					            Carrito de Compras Vacio

					        </span>

					    </li>



					</ul>

					</li>

				</ul>

			</div>

		</div>

	</div>



	<div id="body">

    

    <section class="content-wrapper main-content clear-fix">

       	<?php echo $content; ?>

    </section>

	</div>



<div id="footer" style="font-family: Century Gothic;font-weight: bold;font-weight: 700;">

    <a class="scroll" href="#home"><div class="back-top">&#xf102;</div></a>

    <div class="container" style="color: white; width: 100%; font-size: 14px; font-weight: bold; height: 148px;">



        <div style="

    width: 19%;

    display: inline-block;    vertical-align: top;

    ">

    <a href="/peru/home/about">

<?php echo Yii::t('app','mnufooterabouttext');?>

</a>



                </div>

                <div style="

    width: 19%;

    display: inline-block;    vertical-align: top;text-align:left

    ">

    <a href="/peru/home/contact">

                    <?php echo Yii::t('app','mnufootercontactustext');?>

                </a>



                </div>

                <div style="

    width: 19%;

    display: inline-block;    vertical-align: top;

    ">



<a href="/peru/home/help">
                    <?php echo Yii::t('app','mnufooterneedhelptext');?>

</a>
<br>
<a href="#">
                    SIZING

</a>
<br>
<a href="#">
                    SHIPPING

</a>
<br>
<a href="#">
                    ORDER STATUS

</a>
<br>
<a href="#">
                    RETURNS

</a>


                </div>

                <div style="

    width: 19%;

    display: inline-block;    vertical-align: top;

    ">

<a href="/peru/home/terms">
                    <?php echo Yii::t('app','mnufooterprivacyandtermstext');?>


</a>

                </div>

                <div style="

    display: inline-block;    vertical-align: top;

    width: 19%;

    ">


                    <?php echo Yii::t('app','mnufooterfollowustext');?>


    <ul class="list-social" style="top: 600px;">

        <li class="icon-soc tipped">

            <a href="#">&#xf09a;</a>

        </li>

        <li class="icon-soc tipped">

            <a href="#">&#xf099;</a>

        </li>



        <li class="icon-soc tipped">

<a href="#">&#xf231;</a>

        </li>

        <li class="icon-soc tipped">

            <a href="#">&#xf16d;</a>

        </li>

    </ul>



</div>





<div style="width: 100%; margin-top: 20px;">

    <p>&#169; 2015 Harfie</p>

    <p><small>Todo los derechos reservados</small></p>

</div>



</div>

</div>

    











</body>

</html>