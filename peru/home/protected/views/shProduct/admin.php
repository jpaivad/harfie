<?php
/* @var $this ShProductController */
/* @var $model ShProduct */

$this->breadcrumbs=array(
	'Sh Products'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ShProduct', 'url'=>array('index')),
	array('label'=>'Create ShProduct', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#sh-product-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Sh Products</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'sh-product-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_product',
		'id_supplier',
		'id_manufacturer',
		'id_category_default',
		'id_shop_default',
		'id_tax_rules_group',
		/*
		'on_sale',
		'online_only',
		'ean13',
		'upc',
		'ecotax',
		'quantity',
		'minimal_quantity',
		'price',
		'wholesale_price',
		'unity',
		'unit_price_ratio',
		'additional_shipping_cost',
		'reference',
		'supplier_reference',
		'location',
		'width',
		'height',
		'depth',
		'weight',
		'out_of_stock',
		'quantity_discount',
		'customizable',
		'uploadable_files',
		'text_fields',
		'active',
		'redirect_type',
		'id_product_redirected',
		'available_for_order',
		'available_date',
		'condition',
		'show_price',
		'indexed',
		'visibility',
		'cache_is_pack',
		'cache_has_attachments',
		'is_virtual',
		'cache_default_attribute',
		'date_add',
		'date_upd',
		'advanced_stock_management',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
