<?php
/* @var $this ShProductController */
/* @var $model ShProduct */

$this->breadcrumbs=array(
	'Sh Products'=>array('index'),
	$model->id_product,
);

$this->menu=array(
	array('label'=>'List ShProduct', 'url'=>array('index')),
	array('label'=>'Create ShProduct', 'url'=>array('create')),
	array('label'=>'Update ShProduct', 'url'=>array('update', 'id'=>$model->id_product)),
	array('label'=>'Delete ShProduct', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_product),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ShProduct', 'url'=>array('admin')),
);
?>

<h1>View ShProduct #<?php echo $model->id_product; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_product',
		'id_supplier',
		'id_manufacturer',
		'id_category_default',
		'id_shop_default',
		'id_tax_rules_group',
		'on_sale',
		'online_only',
		'ean13',
		'upc',
		'ecotax',
		'quantity',
		'minimal_quantity',
		'price',
		'wholesale_price',
		'unity',
		'unit_price_ratio',
		'additional_shipping_cost',
		'reference',
		'supplier_reference',
		'location',
		'width',
		'height',
		'depth',
		'weight',
		'out_of_stock',
		'quantity_discount',
		'customizable',
		'uploadable_files',
		'text_fields',
		'active',
		'redirect_type',
		'id_product_redirected',
		'available_for_order',
		'available_date',
		'condition',
		'show_price',
		'indexed',
		'visibility',
		'cache_is_pack',
		'cache_has_attachments',
		'is_virtual',
		'cache_default_attribute',
		'date_add',
		'date_upd',
		'advanced_stock_management',
	),
)); ?>
