<?php
/* @var $this ShProductController */
/* @var $model ShProduct */

$this->breadcrumbs=array(
	'Sh Products'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ShProduct', 'url'=>array('index')),
	array('label'=>'Manage ShProduct', 'url'=>array('admin')),
);
?>

<h1>Create ShProduct</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>