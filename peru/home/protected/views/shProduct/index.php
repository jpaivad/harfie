<?php
/* @var $this ShProductController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Sh Products',
);

$this->menu=array(
	array('label'=>'Create ShProduct', 'url'=>array('create')),
	array('label'=>'Manage ShProduct', 'url'=>array('admin')),
);
?>

<h1>Sh Products</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
