<?php
/* @var $this ShProductController */
/* @var $model ShProduct */

$this->breadcrumbs=array(
	'Sh Products'=>array('index'),
	$model->id_product=>array('view','id'=>$model->id_product),
	'Update',
);

$this->menu=array(
	array('label'=>'List ShProduct', 'url'=>array('index')),
	array('label'=>'Create ShProduct', 'url'=>array('create')),
	array('label'=>'View ShProduct', 'url'=>array('view', 'id'=>$model->id_product)),
	array('label'=>'Manage ShProduct', 'url'=>array('admin')),
);
?>

<h1>Update ShProduct <?php echo $model->id_product; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>