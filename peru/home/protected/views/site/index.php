<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<div id="home">
    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>
                <!-- SLIDE  -->
                <li data-transition="zoomout" data-slotamount="7" data-masterspeed="1000">
                    <div class="just_pattern"></div>
                    <!-- MAIN IMAGE -->
                    <img src="images/1.jpg" alt="" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption customin"
                         data-x="474"
                         data-y="269"
                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="500"
                         data-start="800"
                         data-easing="Power3.easeInOut"
                         data-endspeed="300"
                         style="z-index: 2">
                        <a href="http://harfie.com/peru/home/Customizador/">
                            <div class="services-link">
                                <?php echo Yii::t('app','bnbtnDESIGNNOW');?>
                            </div>
                        </a>
                        <a href="http://www.harfie.com/peru/index.php?id_category=3&controller=category&id_lang=6">
                            <div class="services-link">
                                <?php echo Yii::t('app','bnbtnSHOPNOW');?>
                            </div>
                        </a>


                    </div>

                    <!-- LAYER NR. 
                    <div  id="layer1" class="tp-caption customin"
                         data-x="245"
                         data-y="142"
                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="500"
                         data-start="500"
                         data-easing="Power3.easeInOut"
                         data-endspeed="300"
                         style="z-index: 3">
                        <img src="images/imac1.png" alt="">

                    </div>
					2 -->
                    <!-- LAYER NR. 
                    <div id="layer2"  class="tp-caption customin"
                         data-x="800"
                         data-y="369"
                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="500"
                         data-start="1300"
                         data-easing="Power3.easeInOut"
                         data-endspeed="300"
                         style="z-index: 4">
                        <img src="images/lupe_macbook.png" alt="">

                    </div>
					3 -->
                    <!-- LAYER NR. 
                    <div id="layer3" class="tp-caption customin"
                         data-x="100"
                         data-y="321"
                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="500"
                         data-start="1400"
                         data-easing="Power3.easeInOut"
                         data-endspeed="300"
                         style="z-index: 5">
                        <img src="images/lupe_imac.png" alt="">
                    </div>
					4 -->
                    <!-- LAYER NR.
                    <div id="layer4" class="tp-caption medium_bg_asbestos skewfromleft customout"
                         data-x="47"
                         data-y="233"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="800"
                         data-start="1500"
                         data-easing="Power4.easeOut"
                         data-endspeed="300"
                         data-endeasing="Power1.easeIn"
                         data-captionhidden="on"
                         style="z-index: 6">
                        <?php echo Yii::t('app','bnbtnQUALITYANDSERVICE');?>
                    </div>
					 5 -->
                    <!-- LAYER NR. 6
                    <div id="layer5"  class="tp-caption medium_bg_asbestos skewfromright customout"
                         data-x="820"
                         data-y="324"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="800"
                         data-start="1700"
                         data-easing="Power4.easeOut"
                         data-endspeed="300"
                         data-endeasing="Power1.easeIn"
                         data-captionhidden="on"
                         style="z-index: 7">
                        <?php echo Yii::t('app','bnbtnCOMBINESTYLESINANYWAY');?>
                    </div>
					 -->
                    <!-- LAYER NR. 7 
                    <div id="layer6"  class="tp-caption medium_bg_asbestos skewfromright customout"
                         data-x="820"
                         data-y="369"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="800"
                         data-start="1800"
                         data-easing="Power4.easeOut"
                         data-endspeed="300"
                         data-endeasing="Power1.easeIn"
                         data-captionhidden="on"
                         style="z-index: 8">
                        <?php echo Yii::t('app','bnbtnCUSTOMIZEYOURSHIRT');?>
                    </div>
					-->
                    <!-- LAYER NR. 8 
                    <div id="layer7" class="tp-caption medium_bg_asbestos skewfromleft customout"
                         data-x="218"
                         data-y="278"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="800"
                         data-start="1600"
                         data-easing="Power4.easeOut"
                         data-endspeed="300"
                         data-endeasing="Power1.easeIn"
                         data-captionhidden="on"
                         style="z-index: 9">
                        <?php echo Yii::t('app','bnbtnSHOPONLINE');?>
                    </div>
					-->
                    <!-- LAYER NR. 9 
                    <div class="tp-caption large_bold_white customin customout"
                         data-x="328"
                         data-y="54"
                         data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="600"
                         data-start="1100"
                         data-easing="Back.easeOut"
                         data-endspeed="300"
                         data-endeasing="Power1.easeIn"
                         style="z-index: 10">
                        <?php echo Yii::t('app','bnbtnNEWA');?>
                    </div>
					-->
                    <!-- LAYER NR. 10 
                    <div class="tp-caption medium_light_white customin customout"
                         data-x="536"
                         data-y="71"
                         data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="600"
                         data-start="1200"
                         data-easing="Back.easeOut"
                         data-endspeed="300"
                         data-endeasing="Power1.easeIn"
                         style="z-index: 11">
                        <?php echo Yii::t('app','bnbtnRRIVALS');?>
                    </div>
					-->
                </li>
                <!-- SLIDE  -->
                <li  data-transition="slidedown" data-slotamount="7" data-masterspeed="1000">
                    <div class="just_pattern"></div>
                    <!-- MAIN IMAGE -->
                    <img src="images/2.jpg" alt="" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->
                    <!-- LAYER NR. 7 
                    <div id="layer8" class="tp-caption sfb ltl"
                         data-x="20"
                         data-y="370"
                         data-speed="600"
                         data-start="100"
                         data-easing="Back.easeOut"
                         data-endspeed="400"
                         data-endeasing="Back.easeIn"
                         data-captionhidden="on"
                         style="z-index: 8">
                        <img src="images/iphone1.png" alt="">
                    </div>
					-->
                    <!-- LAYER NR. 7.1 -->
                    <div class="tp-caption customin"
                         data-x="474"
                         data-y="269"
                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="500"
                         data-start="800"
                         data-easing="Power3.easeInOut"
                         data-endspeed="300"
                         style="z-index: 2">
                        <a href="http://harfie.com/peru/home/Customizador/"><div class="services-link"><?php echo Yii::t('app','bnbtnDESIGNNOW');?></div></a>
                        <a href="http://www.harfie.com/peru/index.php?id_category=3&controller=category&id_lang=6">
                            <div class="services-link">
                                <?php echo Yii::t('app','bnbtnSHOPNOW');?>
                            </div>
                        </a>


                    </div>
                    <!-- LAYER NR. 8 
                    <div id="layer9"  class="tp-caption sfb ltl"
                         data-x="800"
                         data-y="150"
                         data-speed="600"
                         data-start="700"
                         data-easing="Back.easeOut"
                         data-endspeed="400"
                         data-endeasing="Back.easeIn"
                         data-captionhidden="on"
                         style="z-index: 10">
                        <img src="images/ipad.png" alt="">
                    </div>
					-->
                    <!-- LAYER NR. 9
                    <div id="layer10"  class="tp-caption sfb ltl"
                         data-x="180"
                         data-y="300"
                         data-speed="600"
                         data-start="400"
                         data-easing="Back.easeOut"
                         data-endspeed="400"
                         data-endeasing="Back.easeIn"
                         data-captionhidden="on"
                         style="z-index: 9">
                        <img src="images/iphone2.png" alt="">
                    </div>

					 -->



                    <!-- LAYER NR. 13 
                    <div class="tp-caption large_bold_white customin ltl"
                         data-x="66"
                         data-y="41"
                         data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="1000"
                         data-start="850"
                         data-easing="Back.easeInOut"
                         data-endspeed="400"
                         data-endeasing="Back.easeIn"
                         style="z-index: 14">
                        <?php echo Yii::t('app','bnbtnDESIGN');?>
                    </div>
					-->
                    <!-- LAYER NR. 14 
                    <div class="tp-caption medium_light_white customin ltl"
                         data-x="291"
                         data-y="64"
                         data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="1000"
                         data-start="900"
                         data-easing="Back.easeInOut"
                         data-endspeed="400"
                         data-endeasing="Back.easeIn"
                         style="z-index: 15">
                        &
                    </div>
					-->
                    <!-- LAYER NR. 15 
                    <div class="tp-caption large_bold_white customin ltl"
                         data-x="315"
                         data-y="41"
                         data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="1000"
                         data-start="950"
                         data-easing="Back.easeInOut"
                         data-endspeed="400"
                         data-endeasing="Back.easeIn"
                         style="z-index: 16">
                        <?php echo Yii::t('app','bnbtnCREATE');?>
                    </div>
					-->
                    <!-- LAYER NR. 16 
                    <div class="tp-caption mediumlarge_light_white customin ltl"
                         data-x="128"
                         data-y="107"
                         data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="1000"
                         data-start="1050"
                         data-easing="Back.easeInOut"
                         data-endspeed="400"
                         data-endeasing="Back.easeIn"
                         style="z-index: 17">
                        <?php echo Yii::t('app','bnbtnWITHTOTALCONTROL');?>
                    </div>
					-->
                </li>
            </ul>
            <div class="tp-bannertimer"></div>
            <a class="scroll" href="#about-scroll" id="ScrollMain"><div class="scroll-btn tipped" data-title="Seguir bajando" data-tipper-options='{"direction":"top","follow":"true"}'></div></a>
        </div>
    </div>
</div>
<!-- NAV -->

<nav id="nav-desktop" class="nav-slide">

    <div class="container">
        <span style="position: fixed;left: 0;top: 70px;color: white;font-weight: bold;"><?php echo Yii::t('app','menuleftWOMAN');?></span>
        <ul>

            <li><a href="#team"><span><?php echo Yii::t('app','menuleftTEES');?></span></a></li>
            <li><a href="#services"><span><?php echo Yii::t('app','menuleftTANKS');?></span></a></li>
            <li><a href="#portfolio"><span><?php echo Yii::t('app','menuleftHOODIES');?></span></a></li>
            <li><a href="#contact"><span><?php echo Yii::t('app','menuleftCROP');?></span></a></li>

        </ul>


        <span style="position: fixed;left: 0;top: 229px;color: white;font-weight: bold;"><?php echo Yii::t('app','menuleftMAN');?></span>
        <ul style="top: 267px">

            <li><a href="#team"><span><?php echo Yii::t('app','menuleftTEES');?></span></a></li>
            <li><a href="#services"><span><?php echo Yii::t('app','menuleftTANKS');?></span></a></li>
            <li><a href="#portfolio"><span><?php echo Yii::t('app','menuleftHOODIES');?></span></a></li>
            <li><a href="#contact"><span><?php echo Yii::t('app','menuleftCROP');?></span></a></li>

        </ul>
        <a href="#">
            <span style="position: fixed;left: 0;top: 389px;color: white;font-weight: bold;">
                <?php echo Yii::t('app','menuleftBESTSELLERS');?>

            </span>
        </a>
        <img src="images/gallery/3.jpg" alt="" style="position: fixed;left: -6px;top: 422px;color: white;font-weight: bold" />
        <ul class="list-social" style="top: 600px;">
            <li class="icon-soc tipped" data-title="facebook" data-tipper-options='{"direction":"top","follow":"true"}'>
                <a href="#">&#xf09a;</a>
            </li>
            <li class="icon-soc tipped" data-title="twitter" data-tipper-options='{"direction":"top","follow":"true"}'>
                <a href="#">&#xf099;</a>
            </li>

            <li class="icon-soc tipped" data-title="github" data-tipper-options='{"direction":"top","follow":"true"}'>
                <a href="#">&#xf231;</a>
            </li>
            <li class="icon-soc tipped" data-title="google +" data-tipper-options='{"direction":"top","follow":"true"}'>
                <a href="#">&#xf16d;</a>
            </li>
        </ul>
        <a href="http://harfie.com/peru/home/Customizador/">
            <span style="position: fixed;left: 0;top: 564px;color: rgb(83, 186, 208);font-weight: bold;">

                <?php echo Yii::t('app','menuleftCUSTOMIZENOW');?>
            </span>
        </a>
    </div>
</nav>

<!-- /NAV -->
<div id="logos">
    <div class="container z-index">
        <div class="sixteen columns jsLeft"  data-scrollreveal="enter bottom and move 150px over 1s">
            <div class="logos-wrap">
                <?php echo Yii::t('app','seccion1COMPRA');?>

            </div>
            <div class="logos-wrap">
                <?php echo Yii::t('app','seccion1CREA');?>
            </div>
            <div class="logos-wrap">
                <?php echo Yii::t('app','seccion1VENDE');?>
            </div>
            <div class="logos-wrap">
                <?php echo Yii::t('app','seccion1VISTETUCREATIVIDAD');?>
            </div>
        </div>
    </div>
</div>
<div id="sep">
    <div class="just_pattern"></div>
    <div class="just_pattern1"></div>
    <div class="parallax"></div>
    <div class="container z-index" id="conetenedornumeros">
        <div class="four columns">
            <div class="facts-wrap">
                <div class="facts-line"></div>
                <div class="facts-wrap-num"><span class="counter">87</span></div>
                <h6><?php echo Yii::t('app','sepClients');?></h6>
            </div>
        </div>
        <div class="four columns">
            <div class="facts-wrap">
                <div class="facts-line"></div>
                <div class="facts-wrap-num"><span class="counter">25</span></div>
                <h6><?php echo Yii::t('app','sepSellers');?></h6>
            </div>
        </div>
        <div class="four columns">
            <div class="facts-wrap">
                <div class="facts-line"></div>
                <div class="facts-wrap-num"><span class="counter">68</span></div>
                <h6><?php echo Yii::t('app','sepTshirt');?></h6>
            </div>
        </div>
        <div class="four columns">
            <div class="facts-wrap">
                <div class="facts-wrap-num"><span class="counter">46</span></div>
                <h6><?php echo Yii::t('app','sepCustomtShirt');?></h6>
            </div>
        </div>

    </div>
    <a href="#" class="big-link" data-reveal-id="myModal"><div class="services-link" style="width: 250px">
                                                               <?php echo Yii::t('app','secctionWHYCUSTOM');?>
        
        </div></a>
</div>





<div id="services">
    <div class="container" style="width:100%">

        <div class="clear"></div>
        <div style="margin: 0 auto;text-align: center">
            <div class="Div2 DivWhomman" data-scrollreveal="enter left and move 150px over 1s">
                <a href="http://www.grupoissa.org/shop/index.php?id_category=8&controller=category"><div class="services-link"><?php echo Yii::t('app','servicesSHOPWOMAN');?></div></a>
            </div>
            <div class="Div2 Divcuto" data-scrollreveal="enter left and move 150px over 1s">

                <a href="#"><div class="services-link"><?php echo Yii::t('app','servicescustomize');?></div></a>
            </div>
            <div class="Div2 Divmen" data-scrollreveal="enter right and move 150px over 1s">
                <a href="http://www.grupoissa.org/shop/index.php?id_category=8&controller=category"><div class="services-link"><?php echo Yii::t('app','servicesSHOPMEN');?></div></a>
            </div>
        </div>


        <div class="clear"></div>
    </div>
</div>




<div id="about">
    <div class="sixteen columns">
        <h1><span><?php echo Yii::t('app','menuleftBESTSELLERS');?></span></h1>
    </div>
    <div class="clear"></div>
    <a href="#"><div class="services-link">BEST SELLERS</div></a>
    <ul class="portfolio-wrap" style="margin-top: 34px;">
        <li class="portfolio-box photography web-design">
            <a class="expander" href="project2.html" title="">
                <img src="images/gallery/1.jpg" alt="" />
                <div class="mask">
                    <h4><?php echo Yii::t('app','bestsellerComprar');?></h4>
                </div>

            </a>
        </li>
        <li class="portfolio-box illustration1 motion-graphics">
            <a class="expander" href="project1.html" title="">
                <img src="images/gallery/2.jpg" alt="" />
                <div class="mask">
                    <h4><?php echo Yii::t('app','bestsellerComprar');?></h4>
                </div>

            </a>
        </li>
        <li class="portfolio-box motion-graphics photography">
            <a class="expander" href="project2.html" title="">
                <img src="images/gallery/3.jpg" alt="" />
                <div class="mask">
                    <h4><?php echo Yii::t('app','bestsellerComprar');?></h4>
                </div>

            </a>
        </li>
        <li class="portfolio-box web-design illustration1">
            <a class="expander" href="project1.html" title="">
                <img src="images/gallery/4.jpg" alt="" />
                <div class="mask">
                    <h4><?php echo Yii::t('app','bestsellerComprar');?></h4>
                </div>

            </a>
        </li>
    </ul>




</div>


<div style="margin: 0 auto;text-align: center;width:100%;height: 235px;" id="seccionInfo">
    <div class="Div3" data-scrollreveal="enter left and move 150px over 1s">
        <div class="head-subtext" style="font-weight: bold;font-family: Century Gothic;">
            <?php echo Yii::t('app','secctioninfoORDERINGSHIPPING');?>
            <br>
            <br>
            <span style="
    text-transform: none;
    font-size: 15px;
    font-weight: normal;">
                <?php echo Yii::t('app','secctioninfoText1');?><a href="#" class="big-link" data-reveal-id="myModal3"> <?php echo Yii::t('app','secctioninfoContinuarleyendo');?></a>.
            </span>
        </div>

    </div>
    <div class="Div3" data-scrollreveal="enter right and move 150px over 1s">
        <div class="head-subtext" style="
    font-weight: bold;">


            <?php echo Yii::t('app','sepWHYSELL');?> <br><br>

            
            <span style="
    text-transform: none;
    font-size: 15px;
    font-weight: normal;
">
                <?php echo Yii::t('app','secctioninfotext2');?>
            </span>
        </div>
        <a href="#" class="big-link" data-reveal-id="myModal2"><div class="services-link" style="width: 180px"><?php echo Yii::t('app','secctioninfoREADABOUTIT');?></div></a>

    </div>
</div>


<div id="myModal" class="reveal-modal">
    <h1><?php echo Yii::t('app','secctionWHYCUSTOM');?></h1>
    <p>
    <?php echo Yii::t('app','modal1text');?>
    </p>
    <a class="close-reveal-modal">&#215;</a>
</div>
<div id="myModal2" class="reveal-modal">
    <h1><?php echo Yii::t('app','sepWHYSELL');?></h1>
    <p>
        <?php echo Yii::t('app','modal2text');?>
        <br>
        <br>
        <?php echo Yii::t('app','modal2text1');?>
        
    </p>
    <a class="close-reveal-modal">&#215;</a>
</div>
<div id="myModal3" class="reveal-modal">
    <h1><?php echo Yii::t('app','modal3ENTREGAS');?></h1>
    <p>
        
        <?php echo Yii::t('app','modal3text1');?>
        <br>

        <br>
        <?php echo Yii::t('app','modal3text3');?>
        <br>
        <?php echo Yii::t('app','modal3text4');?>
        <br>
        <br>
        <?php echo Yii::t('app','modal3text5');?>

    </p>
    <a class="close-reveal-modal">&#215;</a>
</div>





  <!-- JAVASCRIPT

    ================================================== -->

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/modernizr.custom.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/royal_preloader.min.js"></script>

    <script type="text/javascript">

        Royal_Preloader.config({

            mode: 'text', // 'number', "text" or "logo"

            text: 'HARFIE.COM',

            timeout: 0,

            showInfo: true,

            opacity: 1,

            background: ['#FFFFFF']

        });

    </script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/classie.js"></script>



    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/cbpAnimatedHeader.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/styleswitcher.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/retina-1.1.0.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.themepunch.plugins.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.themepunch.revolution.min.js"></script>

    <script type="text/javascript">



        var revapi;



        jQuery(document).ready(function () {



            revapi = jQuery('.tp-banner').revolution(

             {

                 delay: 7000,

                 startwidth: 1170,

                 startheight: 700,

                 hideThumbs: 10,

                 fullWidth: "on",

                 forceFullWidth: "on"

             });



        });	//ready



    </script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.easing.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/svganimations.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.parallax-1.1.3.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.bxslider.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/contact.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.fitvids.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/plugins.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/template.js"></script>





    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>