<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - About';
$this->breadcrumbs=array(
	'About',
);
?>

<div id="home" style="height: 2096px; max-height: 715px;display: inline;">
    <div class="sixteen columns">
        <h1><span>ABOUT US</span></h1>


    </div>

    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/parallax/99.jpg" style="
    margin: auto;
    left: 0;
    right: 0;
    text-align: center;
    display: block;
    width: 80%;
" />
    
    <div style="
    width: 60%;
    margin: auto;
    font-size: 14px;
    margin-top: 38px;
    margin-bottom: 76px;

    text-align: justify;
    font-family: Century Gothic;



">
        Harfie es una tienda virtual de prendas básicas; donde podrás encontrar la ropa adecuada para tu estilo. Ofrecemos todas nuestras prendas en el mejor algodón peruano y también, existe la posibilidad de utilizar algodón 100% orgánico. Toda nuestra ropa es hecha mano por confeccionistas peruanos.

        Todas nuestras ordenes son elaborados una vez que ingresa el pedido y es hecha de forma especial para ti. Nos gusta que nuestros clientes sientan el control sobre su forma de vestir y así, poder mostrar su esencia.
    </div>




</div>




  <!-- JAVASCRIPT

    ================================================== -->

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/modernizr.custom.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/royal_preloader.min.js"></script>

    <script type="text/javascript">

        Royal_Preloader.config({

            mode: 'text', // 'number', "text" or "logo"

            text: 'HARFIE.COM',

            timeout: 0,

            showInfo: true,

            opacity: 1,

            background: ['#FFFFFF']

        });

    </script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/classie.js"></script>



    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/cbpAnimatedHeader.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/styleswitcher.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/retina-1.1.0.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.themepunch.plugins.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.themepunch.revolution.min.js"></script>

    <script type="text/javascript">



        var revapi;



        jQuery(document).ready(function () {



            revapi = jQuery('.tp-banner').revolution(

             {

                 delay: 7000,

                 startwidth: 1170,

                 startheight: 700,

                 hideThumbs: 10,

                 fullWidth: "on",

                 forceFullWidth: "on"

             });



        }); //ready



    </script>
    
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.easing.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/svganimations.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.parallax-1.1.3.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.bxslider.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/contact.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.fitvids.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/plugins.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/template.js"></script>





    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>