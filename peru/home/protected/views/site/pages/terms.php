<div id="home" style="height: 2096px; max-height: 715px;display: inline;">
    <br />
    <br />
    <div class="sixteen columns">
        <h1><span>PRIVACY & TERMS</span></h1>


    </div>

    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/parallax/9.jpg" style="
    margin: auto;
    left: 0;
    right: 0;
    text-align: center;
    display: block;
    width: 80%;
" />

    <div style="
    width: 60%;
    margin: auto;
    font-size: 14px;
    margin-top: 38px;
    margin-bottom: 76px;

    text-align: justify;
    font-family: Century Gothic;



">



        1.	Políticas de entrega<br />
        <br />

        a.	Plazos de entrega: El delivery depende de la ubicación geográfica y el método de envío. Al mismo tiempo, se debe de tener en consideración que los productos demoran 4 días en salir del almacén. Para poder comprobar el tiempo de envío deberá realizar los siguientes pasos:
        <br />
        a)	Agrega el producto a tu carrito de compras dando clic en 'Comprar Ahora'.
        <br />
        b)	Abajo del listado de tus productos seleccionados, introduce los siguientes datos en la sección de dirección de entrega: Selecciona el país, departamento, la provincia y el distrito (Código postal).
        <br />
        c)	Automáticamente aparecerá el tiempo de envío en la columna 'TIEMPO ESTIMADO DE ENTREGA'.
        <br />
        Recuerda que el tiempo de entrega se cuenta en días hábiles (se excluyen sábados, domingos y días festivos (Calendario) ) y empieza a correr a partir de que se confirme tu pago, no necesariamente el mismo día de la compra.
        <br />
        b.	Horarios de entrega: Los pedidos podrán ser  entregados entre las 9am y 5pm. Sin embargo, nuestra empresa no puede establecer cuál será la hora exacta de la entrega del pedido. Cabe indicar que al momento de que el producto sale de nuestro almacén, se notificará que está  rumbo a su destino. De acuerdo al destino las siguientes empresas ofrecen servicio de Tracking internacional.
        <br />
        c.	Cobertura de entrega: La cobertura de envíos está sujeta a los destinos que ofrecen las compañías de mensajería asociadas a Harfie.
        <br />
        d.	Medios de entrega: En las condiciones ofrecidas en la página web, en el lugar y tiempo pactados, y en la paquetería brindada por la empresa de servicio de mensajería según el destino.
        <br />
        e.	Modo de confirmación de la entrega: Según lo establecido con las empresas de mensajería, las personas que reciban el paquete deberán tienen la obligación de firmar el comprobante respectivo. Cabe indicar que, nuestra empresa enviará al cliente un mail a la dirección de correo electrónico proporcionada al momento de la compra, indicando que el producto ha sido correctamente entregado.
        <br />
        f.	Costos relacionados a la entrega: Los costos de envío se acuerdan al momento de la compra, conforme al contrato con las empresas de mensajería.
        <br />
        <br /><br />
        2.	Políticas de devolución y cancelación de productos:
        <br /><br />

        a.	Política de cambios: Los cambios aplican única y exclusivamente en caso el producto materia de compra presente un defecto de fabricación y/o no sea entregado acorde a las características ofrecidas al momento de la compra,  lo cual deberá ser sustentado en parámetros meramente objetivos y no sobre la base de hechos subjetivos (gustos o impresiones del cliente). 	En estos casos, el cliente únicamente podrá realizar la devolución dentro de un plazo máximo de siete (07) días hábiles contados desde la recepción de la mercadería por parte del cliente; caso contrario, se entenderá que se encuentra conforme con el producto. Cabe señalar que, el cambio o devolución del producto también se encuentra condicionado a que el cliente entregue el artículo en su empaque original, lo cual incluye la caja del producto, el empaque y papel burbuja.
        <br />
        En el presente documento, nuestra empresa deja expresa constancia de que bajo ninguna circunstancia  se recibirán artículos que muestren daños por mal uso o negligencia por parte del cliente y/o hechos determinantes de terceros. Las prendas deben estar con sus etiquetas, hang tag y accesorios respectivos. Los artículos comprados como paquete ( 3 o 5 packs de polos), podrán ser devueltos de manera individual. Cabe indicar que, para facilitar el proceso de análisis de devolución del producto, nuestra empresa tiene el derecho de solicitar fotografías de la mercadería y/o empaques vía mail, las cuales deberán ser proporcionadas por el cliente antes del envío de la mercancía.
        <br />
        b.	Política de cancelación: Debido a que los productos se elaboran en base a los requerimientos particulares de cada cliente, lamentablemente la orden no podrá ser cancelada.
        <br />

        3.	Políticas de recepción de reclamos
        <br />

        a.	Descripción del procedimiento: El cliente tiene la opción de comunicarse con el departamento de atención al cliente o acercarse a las oficinas. Este evaluará la solicitud de reclamo y tratará de ofrecerle la mejor solución a su inconveniente.
        <br />

        El cliente también tiene el derecho de interponer su queja o reclamo en el libro de reclamaciones virtual implementado por nuestra empresa, de conformidad con las disposiciones de la Ley N° 29571, Código de Protección y Defensa del Consumidor.
        <br /><br /><br />

        4.	Políticas de privacidad de la información del consumidor
        <br /><br />

        Para Harfie la confianza de sus clientes es el activo más importante. Es por esta razón que se ha desarrollado un sistema que protege íntegramente su privacidad. Asimismo, se manejan políticas estrictas en el manejo de nuestra base de datos, con el objetivo de asegurar la protección de toda la información almacenada.
        <br />

        En atención a ello, y de conformidad con la Ley N° 29733, Ley de Protección de Datos Personales, y sus normas reglamentarias y modificatorias, el cliente manifiesta su consentimiento previo, informado, expreso e inequívoco para autorizar a Harfie a llevar a cabo el tratamiento y transferencia, nacional e internacional, de sus datos personales para los fines comerciales que se encuentren relacionados con los derechos y obligaciones de la compra de productos.
        <br /><br /><br />

        5.	Restricciones
        <br /><br />

        a.	Restricciones legales: No existen impedimentos legales para el desarrollo del servicio.
        <br />
        b.	Restricciones determinadas por el comercio y aplicables al servicio: No existen impedimentos  comerciales para el desarrollo del negocio.
        <br /><br /><br />

        6.	Datos de contacto: Incluidos en la sección “Contáctenos”
        <br /><br />

        c.	Revisar la sección de “Contáctenos” en la página web.


    </div>

</div>




  <!-- JAVASCRIPT

    ================================================== -->

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/modernizr.custom.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/royal_preloader.min.js"></script>

    <script type="text/javascript">

        Royal_Preloader.config({

            mode: 'text', // 'number', "text" or "logo"

            text: 'HARFIE.COM',

            timeout: 0,

            showInfo: true,

            opacity: 1,

            background: ['#FFFFFF']

        });

    </script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/classie.js"></script>



    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/cbpAnimatedHeader.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/styleswitcher.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/retina-1.1.0.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.themepunch.plugins.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.themepunch.revolution.min.js"></script>

    <script type="text/javascript">



        var revapi;



        jQuery(document).ready(function () {



            revapi = jQuery('.tp-banner').revolution(

             {

                 delay: 7000,

                 startwidth: 1170,

                 startheight: 700,

                 hideThumbs: 10,

                 fullWidth: "on",

                 forceFullWidth: "on"

             });



        }); //ready



    </script>
    
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.easing.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/svganimations.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.parallax-1.1.3.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.bxslider.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/contact.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/jquery.fitvids.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/plugins.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/Scripts/template.js"></script>





    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>