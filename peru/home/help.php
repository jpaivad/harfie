<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>
<script type="text/javascript" src="/peru/home/Scripts/jquery.reveal.js"></script>
<script type="text/javascript" src="/peru/home/Scripts/jquery.js"></script>



  <link rel="stylesheet" type="text/css" href="/peru/home/css/base.css">

  <link rel="stylesheet" type="text/css" href="/peru/home/css/skeleton.css">

  <link rel="stylesheet" type="text/css" href="/peru/home/css/layout.css">

  <link rel="stylesheet" type="text/css" href="/peru/home/css/font-awesome.css">

  <link rel="stylesheet" type="text/css" href="/peru/home/css/retina.css">

  <link rel="stylesheet" type="text/css" href="/peru/home/css/settings.css">

 

  <link rel="stylesheet" type="text/css" href="/peru/home/css/colors/color-blue.css">

  

  <link rel="stylesheet" type="text/css" href="/peru/home/css/reveal.css">

  

  <script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>

  <script type="text/javascript" src="/peru/home/Scripts/jquery.reveal.js"></script>
<script type="text/javascript" src="/peru/home/Scripts/jquery.js"></script>



<?php
require_once dirname(__FILE__) . '/../config/config.inc.php' ;
require_once(dirname(__FILE__) . '/../config/settings.inc.php');
require_once(dirname(__FILE__).  '/../header.php');
require_once dirname(__FILE__) . '/../init.php' ;
?>

<div id="home" style="height: 2096px; max-height: 715px;display: inline;">
    <div class="sixteen columns">
        <h1><span>AYUDA</span></h1>


    </div>

    

    <div style="
    width: 60%;
    margin: auto;
    font-size: 14px;
    margin-top: 38px;
    margin-bottom: 76px;

    text-align: justify;
    font-family: Century Gothic;
" id="contehelptest">


        

        <div style="text-align:center;display:block">
            <p>
        Comúnicate con nosotros, que nuestro equipo de servicio al cliente resolverá tus dudas.
        <br><br>
        
            Contáctanos a 
        <A NAME="SIZING"></A>
        <br>
        Correo: info@harfie.com
        <br>
        Teléfono: 51 1 563 3644
        </p>
        </div>
        

<br>
<br>

        
        <p>
            <h1>CUADRO DE TALLAS</h1>
<span style="font-size: 12px;font-weight: bold;">EN CM:</span>
                <br>
                <br>
<div style='font-size: 12px;text-align: center;width: 49%;display: inline-block;font-weight: bold;'>
    Hombre
</div>
<div class="divmaxmujertitulo" style='font-size: 12px;text-align: center;width: 49%;display: inline-block;font-weight: bold;'>
    Mujer
</div>
                <table  class='table table-striped tablemen' style='font-size: 12px;text-align: center;width: 49%;display: inline-block;' >
                    <thead>
                        <tr  style='background-color: rgb(89,89,89);color: white;font-weight: bold;'>
                            <th style='text-align: center;'>Size</th>
                            <th style='text-align: center;'>S</th>
                            <th style='text-align: center;'>M</th>
                            <th style='text-align: center;'>L</th>
                            <th style='text-align: center;'>XL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>PECHO</td>
                            <td>96</td>
                            <td>104</td>
                            <td>106</td>
                            <td>110</td>

                        </tr>
                        <tr>
                            <td>CINTURA</td>
                            <td>74 - 76</td>
                            <td>79 - 81</td>
                            <td>84 - 86</td>
                            <td>86 - 91</td>

                        </tr>
                        <tr>
                            <td>CADERA</td>
                            <td>94 - 96</td>
                            <td>99 - 104</td>
                            <td>104 - 106</td>
                            <td>109 - 110</td>

                        </tr>
                        <tr>
                            <td>LARGO</td>
                            <td>70</td>
                            <td>72</td>
                            <td>76</td>
                            <td>78</td>

                        </tr>
                    </tbody>
                </table>

<div class="divminimujertitulo" style='font-size: 12px;text-align: center;width: 49%;font-weight: bold;'>
    Mujer
</div>
                <table  class='table table-striped tablewoman' style='font-size: 12px;text-align: center;width: 49%;display: inline-block;' >
                    <thead>
                        <tr  style='background-color: rgb(89,89,89);color: white;font-weight: bold;'>
                            <th style='text-align: center;'>Size</th>
                            <th style='text-align: center;'>XS</th>
                            <th style='text-align: center;'>S</th>
                            <th style='text-align: center;'>M</th>
                            <th style='text-align: center;'>L</th>
                            <th style='text-align: center;'>XL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>BUSTO</td>
                            <td>76 - 82</td>
                            <td>81 - 86</td>
                            <td>86 - 90</td>
                            <td>91 - 94</td>
                            <td>97 - 98</td>

                        </tr>
                        <tr>
                            <td>CINTURA</td>
                            <td>61 - 64</td>
                            <td>66 - 69</td>
                            <td>71 - 74</td>
                            <td>72 - 76</td>
                            <td>74 - 79</td>

                        </tr>
                        <tr>
                            <td>CADERA</td>
                            <td>79 - 84</td>
                            <td>84 - 88</td>
                            <td>89 - 92</td>
                            <td>94 - 98</td>
                            <td>102 - 102</td>

                        </tr>
                        <tr>
                            <td>LARGO</td>
                            <td>58</td>
                            <td>60</td>
                            <td>62</td>
                            <td>66</td>
                            <td>68</td>

                        </tr>
                    </tbody>
                </table>



                <span style="font-size: 12px;font-weight: bold;">EN PÚLGADAS:</span>
                   <br><br>

                   <div style='font-size: 12px;text-align: center;width: 49%;display: inline-block;font-weight: bold;'>
    Hombre
</div>
<div class="divmaxmujertitulo" style='font-size: 12px;text-align: center;width: 49%;display: inline-block;font-weight: bold;'>
    Mujer
</div>
                <table  class='table table-striped tablemen' style='font-size: 12px;text-align: center;width: 49%;display: inline-block;' >
                    <thead>
                        <tr  style='background-color: rgb(89,89,89);color: white;font-weight: bold;'>
                            <th style='text-align: center;'>Size</th>
                            <th style='text-align: center;'>S</th>
                            <th style='text-align: center;'>M</th>
                            <th style='text-align: center;'>L</th>
                            <th style='text-align: center;'>XL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>PECHO</td>
                            <td>38</td>
                            <td>41</td>
                            <td>42</td>
                            <td>43</td>

                        </tr>
                        <tr>
                            <td>CINTURA</td>
                            <td>29 - 30</td>
                            <td>31 - 32</td>
                            <td>33 - 34</td>
                            <td>34 - 37</td>

                        </tr>
                        <tr>
                            <td>CADERA</td>
                            <td>37 - 38</td>
                            <td>39 - 41</td>
                            <td>41 - 42</td>
                            <td>43 - 43</td>

                        </tr>
                        <tr>
                            <td>LARGO</td>
                            <td>28</td>
                            <td>28</td>
                            <td>30</td>
                            <td>31</td>

                        </tr>
                    </tbody>
                </table>

<div class="divminimujertitulo" style='font-size: 12px;text-align: center;width: 49%;font-weight: bold;'>
    Mujer
</div>
                <table  class='table table-striped tablewoman' style='font-size: 12px;text-align: center;width: 49%;display: inline-block;' >
                    <thead>
                        <tr  style='background-color: rgb(89,89,89);color: white;font-weight: bold;'>
                            <th style='text-align: center;'>Size</th>
                            <th style='text-align: center;'>XS</th>
                            <th style='text-align: center;'>S</th>
                            <th style='text-align: center;'>M</th>
                            <th style='text-align: center;'>L</th>
                            <th style='text-align: center;'>XL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>BUSTO</td>
                            <td>30 - 32</td>
                            <td>32 - 34</td>
                            <td>34 - 35</td>
                            <td>36 - 37</td>
                            <td>38 - 39</td>

                        </tr>
                        <tr>
                            <td>CINTURA</td>
                            <td>24 - 25</td>
                            <td>26 - 27</td>
                            <td>28 - 29</td>
                            <td>29 - 30</td>
                            <td>30 - 31</td>

                        </tr>
                        <tr>
                            <td>CADERA</td>
                            <td>31-33</td>
                            <td>33-35</td>
                            <td>35-36</td>
                            <td>37-39</td>
                            <td>40-41</td>

                        </tr>
                        <tr>
                            <td>LARGO</td>
                            <td>23</td>
                            <td>23</td>
                            <td>24</td>
                            <td>26</td>
                            <td>27</td>

                        </tr>
                    </tbody>
                </table>
                <A NAME="SCADERAPING"></A>
        </p>

          <br>
          <br>
          <br>
        <p>
            <br>

            <h1>ENVÍOS</h1> 
            Disfruta de un envío gratuito en harfie.com a cualquiera de nuestros destinos. 
            <br><br>

            Tomar en concideracion lo siguiente:
            Nuestros envíos se realizan solo en dias habiles. Se considera días hábiles de lunes a viernes excepto los feriados o festividades en Perú.

            Recibirás una notificación apenas tu orden haya sido enviada. 

            <br>
            
            <br> Por favor esperar al menos 24 horas para poder realizar el seguimiento de su pedido.
            <br><br>
            Una vez que se haya realizado la orden de tu pedido, tardará entre   <b> 2-4 días laborables </b> en confeccionarse y salir de nuestro almacén.
            <br>
            Por favor, tomar en cuenta que para poder realizar el tracking de su pedido deberá esperar <b>al menos 1 día.</b> 
            <br><br>
            No olvidar, que <b> ningún envío </b> se realizará en <b>  días feriados o festividades</b> de acuerdo al calendario de <b> Perú.</b>

            <span> TIEMPO ESTIMADO DE DELIVERY</span>
            <br><br>
                <table  class='table table-striped tablewoman' style='font-size: 12px;text-align: center;' >
                    <thead>
                        <tr  style='background-color: rgb(89,89,89);color: white;font-weight: bold;'>
                            <th style='text-align: center;'>Destino</th>
                            <th style='text-align: center;'>Tiempo Estimado</th>
                            <th style='text-align: center;'>Servicio</th>
                            <th style='text-align: center;'>Costo</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Perú</td>
                            <td>5 - 7 días laborables</td>
                            <td>Olva Courier</td>
                            <td>¡Gratis!</td>
                            

                        </tr>
                        <tr>
                            <td>América</td>
                            <td>5 - 10 días laborables</td>
                            <td>UPS</td>
                            <td>¡Gratis!</td>
                        </tr>
                        <tr>
                            <td>Europa</td>
                            <td>5 - 10 días laborables</td>
                            <td>UPS</td>
                            <td>¡Gratis!</td>
                        </tr>
                        <tr>
                            <td>Asia</td>
                            <td>5 - 10 días laborables</td>
                            <td>UPS</td>
                            <td>¡Gratis!</td>
                            

                        </tr>
                    </tbody>
                </table>
<br><br>
            <A NAME="ORDERSTATUS"></A>
<br>
<b> Por favor tomar en cuenta:</b> Los envíos internacionales pueden estar sujetos a impuestos por importación, derechos y tasas de aduana que se aplican una vez que su paquete llega al país de destino y <b> dichos cargos serán responsabilidad del cliente.</b>


        </p>
        
<br>
<br>
<A NAME="RETURNS"></A>
<br>
        
        
        
        
        
        
<br>
<br>
<br>
        
        <p><H1>DEVOLUCIONES</H1>
            Las órdenes de compra realizadas en Harfie.com pueden ser devueltas por correo a nuestras oficinas con su empaque original y con los acompañamientos del producto (Boleta, hangtag, bolsa de protección, tarjeta de gracias y stickers).

            Recuerda que cuentas con 7 días calendario a partir de la fecha que recibiste tu producto para realizar la devolución.
<br><br>
            Como realizar las devoluciones vía correo
            <br>

            - Acercarse a las oficinas de un servicio postal <br>
            - Todas las devoluciones deberán ser pagadas por el cliente. <br>
            - Los paquetes que sean devueltos se recomienda enviarlos con un método rastreable. Por favor, guardar el número de tracking para la seguridad del envío. <br>
            - Las devoluciones deberán ser enviadas a nuestras oficinas cuya dirección es: <br>

            Calle 1, Manzana F, Lote 14, Urbanización Residencial Aeropuerto, Callao, Callao, Perú.  <br> <br>

            <b> * Si la compra fue realizada a través del medio de pago: PAGOEFECTIVO. Dicha orden no podrá ser devuelta.</b>

            <br> <br>            

            Harfie.com acepta devoluciones las cuales otorgarán a nuestros clientes un vale de consumo por el mismo valor de dicha orden de compra. La cual podrá ser utilizada en su siguiente visita. <br> <br>

            Harfie.com no realiza reembolsos de efectivo.            
        </p>
<br>
<br>

<br>
        
        
<A NAME="reclamaciones"></A>        
        
        
        
<br>
<br>
<br>
        
        <p><H1>LIBRO DE RECLAMACIONES</H1>
            <br><br>

De acuerdo al Código de Protección y Defensa del Consumidor, contamos con un Libro de Reclamaciones a disposición de quién requiera registrar una queja o reclamo. Para cualquier queja o reclamo sobre las compras que se realizan por internet , sigue los siguientes pasos: 

<br><br>

1. Descarga aquí el archivo Excel  <a href="FormatoReclamacionVirtual.xls" target="#">  "Libro de Reclamaciones".</a> <br>


2. Guarda el archivo en tu computadora. <br>


3. Llena el archivo con tu respectivo reclamo. <br>


4. Guarda el archivo y envíanoslo adjunto en un correo electrónico a: info@harfie.com <br>


5. De conformidad y en cumplimiento del D.S. 006-2014 PCM, el plazo de atención del reclamo es de 30 días calendario desde su presentación, el cual podrá extenderse excepcionalmente de acuerdo a la complejidad del requerimiento.<br>


6. Nos comunicaremos contigo para confirmar la recepción de tu reclamo. <br>


7. Debes tener en cuenta que el Libro de Reclamaciones es exclusivo para reclamos o consultas que sean relacionados con las compras que se realizan por internet. <br>

        </p>


    </div>

</div>


<style type="text/css">
td{
    text-align: center;
}
</style>
  <!-- JAVASCRIPT

    ================================================== -->

    <script type="text/javascript" src="/peru/home/Scripts/jquery.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/modernizr.custom.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/royal_preloader.min.js"></script>

    <script type="text/javascript">

        Royal_Preloader.config({

            mode: 'text', // 'number', "text" or "logo"

            text: 'HARFIE.COM',

            timeout: 0,

            showInfo: true,

            opacity: 1,

            background: ['#FFFFFF']

        });

    </script>

    <script type="text/javascript" src="/peru/home/Scripts/classie.js"></script>



    <script type="text/javascript" src="/peru/home/Scripts/cbpAnimatedHeader.min.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/styleswitcher.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/retina-1.1.0.min.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.themepunch.plugins.min.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.themepunch.revolution.min.js"></script>

    <script type="text/javascript">



        var revapi;



        jQuery(document).ready(function () {

        //jQuery('.container').addClass('homecontainer')
        //jQuery('.homecontainer').removeClass('container')

        $('.columns-container').attr('style','');
            revapi = jQuery('.hometp-banner').revolution(

             {

                 delay: 7000,

                 startwidth: 1170,

                 startheight: 700,

                 hideThumbs: 10,

                 fullWidth: "on",

                 forceFullWidth: "on"

             });



        });	//ready



    </script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.easing.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/svganimations.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.parallax-1.1.3.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.bxslider.min.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/contact.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/jquery.fitvids.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/plugins.js"></script>

    <script type="text/javascript" src="/peru/home/Scripts/template.js"></script>

    <script type="text/javascript" src="/peru/themes/leo_tshirt/js/modules/blocktopmenu/js/blocktopmenu.js"></script>
    <script type="text/javascript" src="/peru/themes/leo_tshirt/js/modules/blocktopmenu/js/superfish-modified.js"></script>



    

<?php
require_once(dirname(__FILE__).  '/../footer.php');

?>
